<?php
	error_reporting(E_ERROR);
	session_start();
	header("Content-Type:text/html; charset=utf-8");
	require_once("config.php");

	if(include("modules/core_class.php")) $coreObj = new core;

	if(!isset($_SESSION['logHash'])){
		$_SESSION['logHash'] = md5(date('d.m.Y').' - '.date( "H:i:s" ).rand()); //
		$coreObj->logMe('Создан хэш для логирования: "'.$_SESSION['logHash'].'"','info','log');
	}
	$coreObj->logMe('Прошли через точку входа','info','log');
	$class = "authorization";
	if($_GET['page']){
		$class = trim(strip_tags($_GET['page']));
	}

	if($_SESSION['logined']['rights'] == 'noAccess' && $class != 'logIn'){
		$class = 'noAccess';
	}

	if(file_exists("modules/client/".$class."_class.php")){
		include("modules/client/".$class."_class.php");
		if(class_exists($class)){
			$class_obj = new $class;
			if($_POST){
				$class_obj->form_obr();
			}else{
				$class_obj->get_page();
			}
		}
		else{
			$coreObj->logMe('Не найден класс: '.$class.' в файле: '."modules/client/".$class."_class.php",'error','log');
			header("Location: http://".$_SERVER['HTTP_HOST'].'/authorization/');
		}
	}elseif(file_exists("modules/work/".$class."_class.php")){
		include("modules/work/".$class."_class.php");
		if(class_exists($class)){
			$class_obj = new $class;
			if($_POST){
				$class_obj->form_obr();
			}else{
				$class_obj->get_page();
			}
		}
		else{
			$coreObj->logMe('Не найден класс: '.$class.' в файле: '."modules/work/".$class."_class.php",'error','log');
			header("Location: http://".$_SERVER['HTTP_HOST'].'/authorization/');
		}
	}
	else{
		$coreObj->logMe('Запрашиваемый файил не существует : '.$class."_class.php",'error','log');
		header("Location: http://".$_SERVER['HTTP_HOST'].'/authorization/');
	}
	unset($coreObj);
	unset($class_obj);
?>