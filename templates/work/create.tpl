<script>
    $(document).ready(function(){
        $("#clientFinder").chosen();
        $(".chosen-select").chosen();
    });
</script>
<div class="container" style="width: 99% !important;">
    <h2>Создание ништяков делается туточки!</h2>

    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#client">Клиенты</a></li>
        <li class="active"><a data-toggle="tab" href="#tickets">Новый акт</a></li>
    </ul>

    <div class="tab-content">
        <div id="client" class="tab-pane fade">
            <div class="col-sm-6" style=" margin: 15px;">
                {$clntsCnfg = $config.clients}
                {include 'work/newClientForm.tpl'}
            </div>
            {if in_array($logined.rights, array('admin','manager'))}
                <div class="rov">
                    <div class="col-md-12">
                    {include 'work/allClientsTable.tpl'}
                    </div>
                </div>
            {/if}
        </div>
            
        <div id="tickets" class="tab-pane fade in active">
            <div class="panel-group" id="accordion">
                {$tktsCnfg = $config.tikets.columns}
                <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
                    <div id="newTiketPanel">
                        <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                        {include 'work/newTiketForm.tpl'}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include 'work/modals/allClientInfoModal.tpl'}

</div>