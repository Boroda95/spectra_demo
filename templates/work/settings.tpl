<div class="container" style="width: 99% !important;">
  <p><br></p>
	<h2>Тут можно редактировать права пользователей.</h2>
	<p>На данный момент существуют следующие типы аккаунтов:</p> 
	<p>
		<strong>admin</strong> - безграничные возможности.<br>
		<strong>manager</strong> - все те же возможности, кроме удаления записей из базы и редактирования этих прав.<br>
		<strong>moderator</strong> - чувак, который может создавать новые заказы, видеть текущие,готовые и менять их рабочий статус, а также редактировать шаблоны.<br>
        <strong>remontnik</strong> - чувак, который может то же что и "moderator", но не может создавать новых заказов.<br>
		<strong>noAccess</strong> - чувак, который может сходить в окно.<br>
	</p>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#active">Действующие</a></li>
        <li><a data-toggle="tab" href="#blocked">Заблокированые</a></li>
    </ul>

    <div class="tab-content">
        <div id="active" class="tab-pane fade in active">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Логин</th>
                        <th>Права</th>
                        <th>Новый пароль</th>
                    </tr>
                </thead>
                <tbody>
                    {$rights = array('admin','manager','moderator','remontnik','noAccess')}
                    {foreach item=ItemsData key=Itemskey from=$managers}
                    {if $logined.login != 'NLO' && $ItemsData.synonym == 'NLO'}
                    {elseif $ItemsData.rights != "noAccess"}
                    <tr>
                        <td dataID="{$ItemsData.id}" dataName="fName" dataParent="managers" contenteditable="true" class="editableTD">
                            {$ItemsData.fName}
                        </td>
                        <td dataID="{$ItemsData.id}" dataName="sName" dataParent="managers" contenteditable="true" class="editableTD">
                            {$ItemsData.sName}
                        </td>
                        <td dataID="{$ItemsData.id}" dataName="synonym" dataParent="managers" contenteditable="true" class="editableTD">
                            {$ItemsData.synonym}
                        </td>
                        <td>
                            <select class="editableContent form-control" dataID="{$ItemsData.id}" dataName="rights" dataParent="managers">
                                {foreach item=refData from=$rights}
                                {if $ItemsData.rights == $refData}
                                <option selected value="{$refData}">{$refData}</option>
                                {else}
                                <option value="{$refData}">{$refData}</option>
                                {/if} 
                                {/foreach}
                            </select>
                        </td>
                        <td dataID="{$ItemsData.id}" dataName="password" dataParent="managers" contenteditable="true" class="editableTD"></td>
                    </tr>
                    {/if}
                    {/foreach}
                </tbody>  
            </table>
            <table class="table table-striped table-condensed">
                <tbody>
                    <tr>
                        <form action="/settings/" method="POST" id="newMasterForm">
                            <td>
                                <input type="text" name="masterName" class="form-control" placeholder="Имя" required>
                            </td>
                            <td>
                                <input type="text" name="masterSecName" class="form-control" placeholder="Фамилия" required>
                            </td>
                            <td>
                                <input type="text" name="masterSynonym" class="form-control" placeholder="Логин" required>
                            </td>
                            <td>
                                <select class="form-control" name="masterRights" required>
                                    <option selected value="">Не выбрано</option>
                                    {foreach item=refData from=$rights}
                                    <option value="{$refData}">{$refData}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td>
                                <input type="password" name="masterPassword" class="form-control" placeholder="Пароль" required>
                            </td>
                            <td>
                                <button type="submit" name="action" value="addNewMaster" class="form-control">Добавить</button>
                            </td>
                        </form>
                    </tr>
                </tbody>
            </table>
        </div>
            
        <div id="blocked" class="tab-pane fade">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Логин</th>
                        <th>Права</th>
                        <th>Новый пароль</th>
                    </tr>
                </thead>
                <tbody>
                    {$rights = array('admin','manager','moderator','remontnik','noAccess')}
                    {foreach item=ItemsData key=Itemskey from=$managers}
                    {if $logined.login != 'NLO' && $ItemsData.synonym == 'NLO'}
                    {elseif $ItemsData.rights == "noAccess"}
                    <tr>
                        <td dataID="{$ItemsData.id}" dataName="fName" dataParent="managers" contenteditable="true" class="editableTD">
                            {$ItemsData.fName}
                        </td>
                        <td dataID="{$ItemsData.id}" dataName="sName" dataParent="managers" contenteditable="true" class="editableTD">
                            {$ItemsData.sName}
                        </td>
                        <td dataID="{$ItemsData.id}" dataName="synonym" dataParent="managers" contenteditable="true" class="editableTD">
                            {$ItemsData.synonym}
                        </td>
                        <td>
                            <select class="editableContent form-control" dataID="{$ItemsData.id}" dataName="rights" dataParent="managers">
                                {foreach item=refData from=$rights}
                                {if $ItemsData.rights == $refData}
                                <option selected value="{$refData}">{$refData}</option>
                                {else}
                                <option value="{$refData}">{$refData}</option>
                                {/if} 
                                {/foreach}
                            </select>
                        </td>
                        <td dataID="{$ItemsData.id}" dataName="password" dataParent="managers" contenteditable="true" class="editableTD"></td>
                    </tr>
                    {/if}
                    {/foreach}
                </tbody>  
            </table>
        </div>
    </div>
</div>

