<thead>
  <tr>
      <th>Заголовок</th>
      <th>Описание этапа</th>
      <th>Скрытое описание</th>
      <th>Цена</th>
      <th>Дата создания</th>
      <th>Master</th>
      <th>Картинки</th>
      {if in_array($logined.rights, array('admin'))}
      <th>DEL</th>
      {/if}
  </tr>
</thead>
{$price = 0}
<tbody>
  {foreach item=stepData key=stepKey from=$steps}
  {$price = $price+ $stepData.stagePrice}
  {$tktImagesArr = unserialize($stepData.stageimages)}
  <tr stepID="{$stepData.id}">
  {if in_array($logined.rights, array('admin','manager','remontnik','moderator'))}
    <td>
      {if $logined.login == $stepData.stageauthor || $logined.rights == 'admin'}
      <div contenteditable="true" class="editableTD" dataID="{$stepData.id}" dataName="stagename" dataParent="stages" style="word-wrap: break-word;">
        {$stepData.stagename}
      </div>
      {else}
        <div style="word-wrap: break-word;">
        {$stepData.stagename}
        </div>
      {/if}
    </td>
    <td>
      {if $logined.login == $stepData.stageauthor || $logined.rights == 'admin'}
      <div contenteditable="true" class="editableTD" dataID="{$stepData.id}" dataName="stagedescription" dataParent="stages" style="word-wrap: break-word;">
        {$stepData.stagedescription}
      </div>
      {else}
        <div style="word-wrap: break-word;">
        {$stepData.stagedescription}
        </div>
      {/if}
    </td>
    <td>
      {if $logined.login == $stepData.stageauthor || $logined.rights == 'admin'}
      <div contenteditable="true" class="editableTD" dataID="{$stepData.id}" dataName="hidstagedescription" dataParent="stages" style="word-wrap: break-word;">
        {$stepData.hidstagedescription}
      </div>
      {else}
        <div style="word-wrap: break-word;">
        {$stepData.hidstagedescription}
        </div>
      {/if}
    </td>
    <td>
      {if $logined.rights == 'admin' || $logined.rights == 'manager'}
      <div contenteditable="true" class="editableTD" dataID="{$stepData.id}" dataName="stagePrice" dataParent="stages">
        {$stepData.stagePrice}
      </div>
      {else}
        <div style="word-wrap: break-word;">
        {$stepData.hidstagedescription}
        </div>
      {/if}
    </td>
    <td>{$stepData.stagedate}</td>
    <td>{$stepData.stageauthor}</td>
    <td>
    <div class="row editableIMGContent" dataID="{$stepData.id}" dataName="stageimages" dataParent="stages">
      {foreach key=imgKey item=imgItem from=$tktImagesArr}
      <div class="col-md-6 imgContainer">
        <img class="img-responsive" src="/img/{$imgItem}" style="width: 70px; height: 60px;">
        <a href="#" class="delThisImg">Удалить</a>
      </div>
      {/foreach} 
    </div>
    </td>
    {if in_array($logined.rights, array('admin'))}
    <td><span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$stepData.id}" dataParent="stages"></span></td>
    {/if}
  {else}
    <td>{$stepData.stagename}</td>
    <td>{$stepData.stagedescription}</td>
    <td>{$stepData.hidstagedescription}</td>
    <td>{$stepData.stagePrice}</td>
    <td>{$stepData.stagedate}</td>
    <td>{$stepData.stageauthor}</td>
    <td>
    <div class="row">
      {foreach key=imgKey item=imgItem from=$tktImagesArr}
      <div class="col-md-6"><img class="img-responsive" src="/img/{$imgItem}" style="width: 70px; height: 60px;"></div>
      {/foreach} 
    </div>
    </td>
  {/if}
  </tr>
  {/foreach}
</tbody>
<thead>
<tr>
    <th colspan="3" style="text-align:right">Итого:</th>
    <th>{$price}</th>
    <th colspan="{if in_array($logined.rights, array('admin'))}4{else}3{/if}"></th>
</tr>
</thead>
