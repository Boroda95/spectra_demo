<table class="table table-bordered table-condensed" id="theTaskListTable">
  <thead>
    <tr>
      <th width="75%">Текст заметки</th>
      <th width="10%">Автор</th>
      <th width="10%">Время</th>
      <th width="5%" style="text-align: center; font-weight: bold;">X</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$toDoListArr item=tdlItem key=tdlKey}
    <tr>
      {if $logined.login == $tdlItem.taskAuthor || $logined.rights == 'admin'}
        <td dataID="{$tdlItem.id}" dataName="taskText" dataParent="toDoList" contenteditable="true" class="editableTD" style="white-space: pre-wrap;">{$tdlItem.taskText}</td>
      {else}
        <td style="white-space: pre-wrap;">{$tdlItem.taskText}</td>
      {/if}
      <td>{$tdlItem.taskAuthor}</td>
      <td>{$tdlItem.taskDate}</td>
      {if $logined.login == $tdlItem.taskAuthor || $logined.rights == 'admin'}
        <td style="text-align: center; color: red;"><span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$tdlItem.id}" dataParent="toDoList"></span></td>
       {else}
        <td style="text-align: center; font-weight: bold;">X</td>
      {/if}
    </tr>
    {/foreach}
  </tbody>
</table>