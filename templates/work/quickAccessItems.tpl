<div class="container" style="width: 99% !important;">
  <p><br></p>
  <h2>Тут можно редактировать плюшки из выпадающих списков</h2>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#titleItems">Заголовок этапа</a></li>
    <li><a data-toggle="tab" href="#descrItems">Описание этапа</a></li>
    <li><a data-toggle="tab" href="#completeness">Комплектность</a></li>
    <li><a data-toggle="tab" href="#tktExtView">Внешний вид</a></li>
    <li><a data-toggle="tab" href="#tktCmmnt">Комментарий</a></li>
    <li><a data-toggle="tab" href="#miniPrice">Минипрайс</a></li>
    {if in_array($logined.rights, array('admin','manager'))}
    <li><a data-toggle="tab" href="#smsTemplates">Шаблоны СМС</a></li>
    {/if}
  </ul>

  <div class="tab-content">
    <div id="titleItems" class="tab-pane fade in active">
    	<table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Элемент</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {foreach item=titleItemsData key=titleItemskey from=$stepTitleItemsTable}
          <tr>
            <td dataID="{$titleItemsData.id}" dataName="items" dataParent="quickAccessItems" contenteditable="true" class="editableTD">
              {$titleItemsData.items}
            </td>
            <td>
              <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$titleItemsData.id}" dataParent="quickAccessItems"></span>
            </td>
          </tr>
          {/foreach}
           <tr>
          	<td>
          		<form action="/quickAccessItems/" method="POST">
          			<input type="text" name="items" placeholder="Название">
          			<input type="hidden" name="target" value="stepTitle">
          			<button type="submit" name="addNewItemTo" value="quickAccessItems">Добавить</button>
          		</form>
          	</td>
          	<td></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div id="descrItems" class="tab-pane fade">
    	<table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Элемент</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {foreach item=descrItemsData key=descrItemskey from=$stepDescrItemsTable}
          <tr>
            <td dataID="{$descrItemsData.id}" dataName="items" dataParent="quickAccessItems" contenteditable="true" class="editableTD">
              {$descrItemsData.items}
            </td>
            <td>
              <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$descrItemsData.id}" dataParent="quickAccessItems"></span>
            </td>
          </tr>
          {/foreach}
          <tr>
          	<td>
          		<form action="/quickAccessItems/" method="POST">
          			<input type="text" name="items" placeholder="Название">
          			<input type="hidden" name="target" value="stepDescr">
          			<button type="submit" name="addNewItemTo" value="quickAccessItems">Добавить</button>
          		</form>
          	</td>
          	<td></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div id="completeness" class="tab-pane fade">
      <table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Элемент</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {foreach item=cmpltnItemsData key=cmpltnItemskey from=$tktCmpltnItemsTable}
          <tr>
            <td dataID="{$cmpltnItemsData.id}" dataName="items" dataParent="quickAccessItems" contenteditable="true" class="editableTD">
              {$cmpltnItemsData.items}
            </td>
            <td>
              <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$cmpltnItemsData.id}" dataParent="quickAccessItems"></span>
            </td>
          </tr>
          {/foreach}
          <tr>
          	<td>
          		<form action="/quickAccessItems/" method="POST">
          			<input type="text" name="items" placeholder="Название">
          			<input type="hidden" name="target" value="tktCmpltn">
          			<button type="submit" name="addNewItemTo" value="quickAccessItems">Добавить</button>
          		</form>
          	</td>
          	<td></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div id="tktExtView" class="tab-pane fade">
      <table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Элемент</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {foreach item=tktExtViewItemsData key=tktExtViewItemskey from=$tktExtViewItemsTable}
          <tr>
            <td dataID="{$tktExtViewItemsData.id}" dataName="items" dataParent="quickAccessItems" contenteditable="true" class="editableTD">
              {$tktExtViewItemsData.items}
            </td>
            <td>
              <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$tktExtViewItemsData.id}" dataParent="quickAccessItems"></span>
            </td>
          </tr>
          {/foreach}
          <tr>
          	<td>
          		<form action="/quickAccessItems/" method="POST">
          			<input type="text" name="items" placeholder="Название">
          			<input type="hidden" name="target" value="tktExtView">
          			<button type="submit" name="addNewItemTo" value="quickAccessItems">Добавить</button>
          		</form>
          	</td>
          	<td></td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <div id="miniPrice" class="tab-pane fade">
        {include 'work/dynamicBlocks/quickAccessMinipriceTab.tpl'}
    </div>

    <div id="tktCmmnt" class="tab-pane fade">
      <table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Элемент</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {foreach item=tktCmmntItemsData key=tktCmmntItemskey from=$tktCmmntItemsTable}
          <tr>
            <td dataID="{$tktCmmntItemsData.id}" dataName="items" dataParent="quickAccessItems" contenteditable="true" class="editableTD">
              {$tktCmmntItemsData.items}
            </td>
            <td>
              <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$tktCmmntItemsData.id}" dataParent="quickAccessItems"></span>
            </td>
          </tr>
          {/foreach}
          <tr>
          	<td>
          		<form action="/quickAccessItems/" method="POST">
          			<input type="text" name="items" placeholder="Название">
          			<input type="hidden" name="target" value="tktCmmnt">
          			<button type="submit" name="addNewItemTo" value="quickAccessItems">Добавить</button>
          		</form>
          	</td>
          	<td></td>
          </tr>
        </tbody>
      </table>
    </div>
    {if in_array($logined.rights, array('admin','manager'))}
    <div id="smsTemplates" class="tab-pane fade">
      <table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Элемент</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {foreach item=ItemsData key=Itemskey from=$smsTemplates}
          <tr>
            <td dataID="{$ItemsData.id}" dataName="templatetext" dataParent="smsTemplates" contenteditable="true" class="editableTD">
              {$ItemsData.templatetext}
            </td>
            <td>
              <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$ItemsData.id}" dataParent="smsTemplates"></span>
            </td>
          </tr>
          {/foreach}
          <tr>
            <td>
              <form action="/quickAccessItems/" method="POST">
                <textarea name="templatetext" placeholder="Текст"></textarea>
                <button type="submit" name="addNewTemplate" value="smsTemplates">Добавить</button>
              </form>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
    {/if}
  </div>
</div>

<script>
    $(document).ready(function () {
        $.each($(document).find(".customVerticalMenu"), function (i, btGroup) {
            $(btGroup).find('ul.dropdown-menu').css({ 'left': $(btGroup).width()+25, 'top': '-3px' });
        });
        $(document).on("mouseenter", "button.groupItem", function (e) {
            $.each($(document).find("button.groupItem"), function (i, groupItem) {
                $(this).attr('aria-expanded', false);
                $(this).parent().removeClass('open');
            });
            $(this).attr('aria-expanded', true);
            $(this).parent().addClass('open');
        });
    });
    $(document).on("submit", "#addNewTplGroup, #addNewTiketTpl", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var data = new FormData();
            var thisForm = $(this);
            var formData = thisForm.serializeArray();
            var formButton = $(document).find(thisForm.find("button[type='submit']")[0]);
            formData.push({ 'name': formButton.attr('name'), 'value': formButton.val() });
            $.each(formData, function (i, datas) {
                if(datas.name == "tplDescription"){
                    var t = datas.value.replace(/\n/g, "|||");
                     t = t.replace(/\r/g, "");
                    data.append(datas.name, t+";");
                }else{
                    data.append(datas.name, datas.value);
                }
            });
            $.ajax({
                url: '/quickAccessItems/',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'html',
                processData: false,
                contentType: false,
                beforeSend: function(){ $('#loader').fadeIn(); },
                success: function (data) {
                    if (data != 'Не получилосъ') {
                        $("#miniPrice").empty().append(data);
                        
                    }else{
                        console.log('Хз, что могло пойти не так. Но что-то пошло не так. ');
                    }
                },
                error: function (data){ console.log(data); },
                complete: function (){ $('#loader').delay(500).fadeOut();}
            });
            return false;
        });
</script>
<style>
    .tplItemData {
        /* display: none; */
        font-size: 11px;
    }

</style>