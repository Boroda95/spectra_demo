<form action="/create/" method="POST" id="addNewClient">
{foreach key=clmnsKey item=clmnsdArr from=$clntsCnfg.columns}
{if $clmnsdArr.colDisplay == 'yes' && $clmnsdArr.colType == 'edit'}
  {if $clmnsKey == 'phone'}
  <input type="tel" class="form-control" placeholder="9308511452 БЕЗ 8" name="{$clmnsKey}"  id="{$clmnsKey}_id" size="10" minlength="10" maxlength="10" style="margin-bottom: 10px;" required>
  {else}
  <input type="text" class="form-control" placeholder="{$clmnsdArr.colCaption}" name="{$clmnsKey}"  id="{$clmnsKey}" style="margin-bottom: 10px;">
  {/if}
{elseif $clmnsdArr.colDisplay == 'yes' && $clmnsdArr.colType == 'method'}
  <input type="text" class="form-control" placeholder="{$clmnsdArr.colCaption}" name="{$clmnsKey}" readonly="readonly" value="{${$clmnsdArr.action}}" style="margin-bottom: 10px;">
{elseif $clmnsdArr.colDisplay == 'yes' && $clmnsdArr.colType == 'radioTandem'}
  <div class="labeledRadio">
    <p style="margin-top: 3px; margin-bottom: 3px; width: 500px;">
    {$clmnsdArr.colCaption}: 
    <label class="label label-warning radioLabel">
      <input type="radio" checked="checked" name="{$clmnsKey}" value="0" class="status">{$clmnsdArr.tandemItems[0]}
    </label>
    
    <label class="label label-success radioLabel">
      <input type="radio" name="{$clmnsKey}" value="1" class="status">{$clmnsdArr.tandemItems[1]}
    </label>
    </p>
  </div>
{elseif $clmnsdArr.colDisplay == 'yes' && $clmnsdArr.colType == 'radioTrio'}
  <div class="labeledRadio">
    <p style="margin-top: 3px; margin-bottom: 3px; width: 500px;">
    {$clmnsdArr.colCaption}: 
    <label class="label label-warning radioLabel">
      <input type="radio" name="{$clmnsKey}" value="0" class="status">{$clmnsdArr.tandemItems[0]}
    </label>
    <label class="label label-primary radioLabel">
      <input type="radio" checked="checked" name="{$clmnsKey}" value="2" class="status">{$clmnsdArr.tandemItems[2]}
    </label>
    <label class="label label-success radioLabel">
      <input type="radio" name="{$clmnsKey}" value="1" class="status">{$clmnsdArr.tandemItems[1]}
    </label>
    </p>
  </div>
{elseif $clmnsdArr.colDisplay == 'yes' && $clmnsdArr.colType == 'date'}
  <p> {$clmnsdArr.colCaption} 
  <input type="date" class="form-control"  name="{$clmnsKey}" style="margin-bottom: 10px;"></p>
{/if}
{/foreach}
  <button type="submit" name="{$clntsCnfg.tableName}" value="add" class="btn btn-success">Добавить</button>
</form>

<script type="text/javascript">
  $(document).ready(function(){
    $("#addNewClient").submit(function(e){
      
      var thisForm = $(this);
      var formData = thisForm.serializeArray();
      var thePhone = $('#phone_id').val();
      console.log(thePhone);
      if(thePhone.length != 10 || thePhone[0] != 9){
        alert("Номер '"+thePhone+"'' не соответствует обговоренному формату. Приведите номер телефона к формату: 9004003020. 10 цифр. Первая цифра 9.");
        e.preventDefault();
        e.stopPropagation();
      }
      $.ajax({
        url: '/contentEditor/',
        type: 'POST',
        async:false,
        data: {
          checkPhone: 'true',
          clientPhone: thePhone
        },
        success: function(data){
          console.log(data);
          res = $.parseJSON(data);
          if(res['checkResult'] != 'ok'){
            e.preventDefault();
            e.stopPropagation();
            alert("Клиент с номером "+thePhone+" в базе уже есть.");
            $('#phone').focus();
          }
        }
      });
    });
  });
</script>