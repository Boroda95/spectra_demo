<html lang="ru" xml:lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{$title}</title>
  <script type='text/javascript' src="/components/jquery/jquery-3.2.1.min.js"></script>
  <script type='text/javascript' src="/components/bootstrap/js/bootstrap.min.js"></script>
  <link href="/components/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div id="loader"><span></span></div>
<!-- --------------------------------------------------------------------------------------- -->
<p><br></p>
      <div id="print_act" style="width: 900px; padding: 0px 10px 0px 17px !important;">
        <div class="row" style="padding-top: 25px;">
          <div class="col-md-12" id="logo"><img class="logo" src="../img/logo.png" width="230px;">
          <!-- <div class="col-md-6" id="contact" style="font-size: 20px; font-weight: bold; width: 438px; margin-bottom: 20px;"> -->
            <div style="float: right; text-align: right; font-size: 21px; font-weight: bold" id="contact1">
              <p>ООО «Спектра»</p><p>+7 930 844 44 10</p><p>www.thespectra.ru</p>
            </div></div>
          <!-- </div> -->
        </div>
      <table class="table table-bordered table-condensed table-hover" id="ssanaya_tbl" style="margin-bottom: 0 !important; margin-top: 20px !important;">
        <tr>
          <td colspan="4"><h6 style="">249032, Калужская область, город Обнинск, улица Курчатова, д.41в, оф.15 <span style="float: right;">ИНН 4025441043, ОГРН 1144025003792, КПП 402501001</span></h6></td>
        </tr>
        <tr style="font-size: 17px; font-weight: bold;">
          <td colspan="2">Акт приема оборудования в ремонт №:</td>
          <td class="success actnum" style="text-align: center;"></td>
          <td class="danger admissiondate"></td>
        </tr>
        <tr style="font-size: 11px; font-weight: bold;">
          <td style="font-size: 12px;">Заказчик (Ф.И.О.):</td>
          <td class="danger">
            <span class="surname"></span>
            <span class="firstname"></span> 
            <span class="middlename"></span> 
          </td>
          <td>Моб. Тел.:</td>
          <td class="danger phone"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold;">
          <td>Название оборудования:</td>
          <td class="danger productname"></td>
          <td>Комплектность:</td>
          <td class="danger completeness"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold;">
          <td colspan="2">Неисправность оборудования со слов клиента:</td>
          <td rowspan="2" colspan="2" class="danger faultdescription"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold;">
          <td>Серийный номер:</td>
          <td class="danger productseries"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold; height: 45px;">
          <td style="vertical-align: inherit;">Внешний вид:</td>
          <td class="danger exteriorview" colspan="3" style="vertical-align: inherit;"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold; height: 45px;">
          <td style="vertical-align: inherit;">Требования клиента <br> при обращении:</td>
          <td class="danger tktcomment" colspan="3" style="vertical-align: inherit;"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold; height: 45px;">
          <td style="vertical-align: inherit;">Примерные рамки стоимости:</td>
          <td class="danger priceRange" colspan="3" style="vertical-align: inherit;"></td>
        </tr>
        <tr style="font-size: 10px; font-weight: bold; height: 45px;">
          <td style="vertical-align: inherit;">Выдержка из прайс листа:</td>
          <td class="danger miniPrice" colspan="3" style="vertical-align: inherit; white-space: pre;"></td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 11px;">
            <ul style="padding-left: 15px; margin-bottom: 0px !important;">
              <li>При приеме оборудования , вскрытие и детальный осмотр не производится.</li>
              <li>При отсутствии деталей ремонт продлевается до срока их получения. </li>
              <li>При периодически появляющейся неисправности, ремонт может продлеваться на неопределенный срок до полного устранения дефектов.</li>
              <li>В случае отказа заказчика в одностороннем порядке от ремонта, после проведения диагностики, заказчиком оплачивается диагностика (экспертиза аппаратуры в соответствии с Прейскурантом Сервисного центра, а так же возмещаются все расходы, понесенные сервисным центром в целях надлежащего выполнения услуги (ст.32 и 33 Закона РФ ОЗПП)   </li>
              <li>Заказчик подтверждает, что с условиями обслуживания, прейскурантом и условиями оплаты ознакомлен до начала работ</li>
              <li>При утере акта приема оборудования в ремонт получить оборудование клиент может только при предъявлении паспорта</li>
              <li>Заказчик обязуется забрать и оплатить отремонтированное оборудование в течении 14 дней с момента оповещения о готовности. По истечении срока бесплатного хранения (14 дней) взымается плата за хранение в размере 10руб в день. По истечении 2-х месяцев от указанного срока оборудование может быть реализовано в счет возмещения убытков Сервисного центра (согласно правилам бытового обслуживания населения РФ).</li>
              <li>Стоимость услуг (работ) определяется сервисным центром только после проведения диагностики в соответствии с прейскурантом сервисного центра.</li>
              <li>Клиент согласен, что все неисправности и внутренние повреждения, которые могут быть обнаружены при сервисном обслуживании возникли до приема по данному сервисному листу.</li>
              <li><b>При приеме оборудования, не указывается точная комплектация устройства, так как вскрытие и детальный осмотр при приеме оборудования не производится. Комплектацию можно определить только в случае если принятое устройство было фирменным и на корпусе имеется неповрежденная наклейка или гравировка с Part Number и cерийным номером, но если есть следы вскрытия, конфигурация могла быть изменена до приема.</b></li>
              <li><b>Если устройство переданное в ремонт имеет аппаратную неисправность, то за место диагностики производится углубленный поиск неисправности аппаратной части, в ходе которого может быть произведен ремонт даже если это не требовал заказчик.</b></li>
              <li><b>Клиент согласен, что при наличии следов вскрытия которые выявлены при приеме оборудования, конфигурация устройства могла быть изменена до приема оборудования</b></li>
            </ul>
          </td>
        </tr>

        <tr style="font-size: 11px;">
          <td style="vertical-align: inherit;" colspan="2">
            <span>Доступ к личному кабинету:</span>
          </td>
          <td class="danger" style="vertical-align: inherit;" colspan="2">
            <span>Адрес:</span> <span>http://login.thespectra.ru/</span> <br>
            <span>Логин</span> <span class="info_td phone"></span><br>
            <span>Пароль</span> <span class="info_td password"></span>
          </td>
        </tr>

        <tr style="font-size: 12px;">
          <td colspan="3">Подтверждаю, что акт заполнен правильно, с условиями ремонта согласен:<br>
            <span class="surname"></span> 
            <span class="firstname"></span> 
            <span class="middlename"></span> <br> <br>  Подпись: ____________________ </td>

          <td>Оборудование принял:<br><br> <br>Подпись: ____________________ </td>
        </tr>
        {* ------------ *}
      </table>

<!--       <table style="margin-top: 10px;">
        <tr style="font-size: 12px; border-left: hidden; border-right: hidden; border-bottom: hidden; height: 33px; padding: 5px;">
          <td style="border-left: hidden; border-right: hidden; border-bottom: hidden;">Доступ к личному кабинету:</td>
        </tr>
        <tr style="font-size: 12px; border-left: hidden; border-right: hidden; border-bottom: hidden; height: 33px; padding: 5px;">
          <td class="title_td">Адрес:</td>
          <td class="info_td" style="border: hidden;">http://login.thespectra.ru/</td>
        </tr>
        <tr style="font-size: 12px; border: hidden; height: 33px; padding: 5px;">
          <td class="title_td" style="border: hidden;">Логин:</td>
          <td class="info_td phone" style="border: hidden;"></td>
        </tr>
        <tr style="font-size: 12px; border: hidden; height: 33px; padding: 5px;">
          <td class="title_td" style="border: hidden;">Пароль:</td>
          <td class="info_td password" style="border: hidden;"></td>
        </tr>
      </table> -->

      </div>
</body>

<style>
  @media print{
    #print_act{
      padding-top: 35px !important;
      height: 100%;
    }
    #contact{
      width: 437px;
      float: right;
    }
    #contact1{
      float: right !important; text-align: right !important; font-size: 22px !important; font-weight: bold !important;
    }
    #ssanaya_tbl {
      border: 2px solid #000000;
    }
    #ssanaya_tbl > thead > tr > th,
    #ssanaya_tbl > tbody > tr > th,
    #ssanaya_tbl > tfoot > tr > th,
    #ssanaya_tbl > thead > tr > td,
    #ssanaya_tbl > tbody > tr > td,
    #ssanaya_tbl > tfoot > tr > td {
       border: 2px solid #000000 !important;
    }
    .header-left-top, .a-right-bottom, .time-right-bottom {
      display: none;
    }
  }

  .ssanaya_tbl {
    border: 0.5px solid #000000;
  }
  .table-bordered > thead > tr > th,
  .table-bordered > tbody > tr > th,
  .table-bordered > tfoot > tr > th,
  .table-bordered > thead > tr > td,
  .table-bordered > tbody > tr > td,
  .table-bordered > tfoot > tr > td {
     border: 0.5px solid #000000;
  }
  .footer{
    position: relative;
    bottom: 0px;
    height: 25px;
    width: 100%;
    left: 0px;
    margin-top: 10px;
  }
  #loader{
      position:fixed;
      display:none;
      top:0;
      left:0;
      height:100%;
      width:100%;
      background:#f2f2f2;
      opacity:.9;
      z-index: 100500;
  }
  #loader span{
      display: block;
      background: url(/img/loader.gif) no-repeat;
      width: 50px;
      height: 50px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -7px 0 0 -56px;
  }
</style>

<script type="text/javascript">
  function dateTimeToTiketDate(theDate){
    var timestamp = new Date(Date.parse(theDate));
    var date = new Date();
    var restDate = '';
    var monthNames = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                      "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"
    ];

    date.setTime(timestamp);
    restDate += date.getDate();
    restDate += ' '+monthNames[date.getMonth()];
    restDate += ' '+date.getFullYear()+'г.';
    return restDate;

  }
  $(document).ready(function(){
    $.ajax({
      url: '/tiket_print/',
      type: 'POST',
      data: {
        getTiketInfo: 'Get tiket info',
        tiketID: {$thisID}
      },
      beforeSend: function(){
        $('#loader').fadeIn();
      },
      success: function(data){
        res = $.parseJSON(data);
        console.log(res);
        $.each(res[0],function(key,value){
          if(key == 'admissiondate'){ 
            $('.'+key).empty().html(dateTimeToTiketDate(value));
          }else{
            $('.'+key).empty().html(value);
          }
        });
      },
      error: function(data){
        res = $.parseJSON(data);
        console.log(res['message']);
      },
      complete: function(){
        $('#loader').delay(500).fadeOut();
      }
    });
  });
</script>