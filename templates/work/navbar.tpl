<div id="loader"><span></span></div>
<nav class="navbar navbar-inverse navbar-fixed-top">
	{if isset($logined) && $logined != ''}
	<a class="navbar-brand" href="/workspace/">Work Space</a>
	{else}
	<a class="navbar-brand" href="#">Spectra</a>
	{/if}
	<button  type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
	</button>
	{if isset($logined) && $logined != ''}
    	<div class="navbar-collapse collapse" id="theNavbar">
    		<ul class="nav navbar-nav navbar-right" style="margin-right: 50px;">
                {if $logined.rights != 'noAccess'}
        			<li>
        				<a href="/create/">Создать</a>
        			</li>
        			<li>
        				<a href="/quickAccessItems/">Шаблоны</a>
        			</li>
                    <li>
                        <a href="/workspace/completed/">Готовые</a>
                    </li>
        			<li>
        				<a href="/workspace/bolt/">Болт</a>
        			</li>
        			{if in_array($logined.rights, array('admin','manager'))}
        			<li>
        				<a href="/workspace/archive/">Архив</a>
        			</li>
        			<li>
        				<a href="/smsArchive/">SMS история</a>
        			</li>
        			{/if}
        			{if in_array($logined.rights, array('admin'))}
                    <li>
                        <a href="/settings/">Настройки</a>
                    </li>
        			<li>
        				<a href="/monitoring/">Мониторинг</a>
        			</li>
                    {/if}
    			{/if}
    			<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$logined.login} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                  	{if in_array($logined.rights, array('admin'))}
                    <li><a href="/backupDB/">Бэкап БД</a></li>
                    {/if}
                    <li><a href="/logIn/exit/">Fuck</a></li>
                  </ul>
                </li>
            </ul>
    	</div>
	{/if}

</nav>
