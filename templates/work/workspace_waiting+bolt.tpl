<div class="container" style="width: 99% !important;">
    <p>
    <br/>
    <br/>
    </p>
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#bolt">Болт</a></li>
        <li class="active"><a data-toggle="tab" href="#waiting">Ждун</a></li>
    </ul>

    <div class="tab-content">
        <div id="bolt" class="tab-pane fade">
            <h3>Это есть болтовые заказы.</h3>
            <form action="#" id="tiketsMastersFilterForm">
                <input type="hidden" value="*" name="tiketsMastersFilter">
            </form>
            <table class="table table-bordered table-condensed" id="theFuckingTikets">
                <thead>
                    <tr>
                        <th></th>
                        <th>№ акта</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Тип устройства</th>
                        <th>Наименование техники</th>
                        <th>Cтатус</th>
                        <th>Состояние</th>
                        <th>Оповещен</th>
                        <th>Дата создания</th>
                        <th></th>
                        {if in_array($logined.rights, array('admin'))}
                        <th></th>
                        {/if}
                    </tr>
                </thead>
            </table>
        </div>
    
        <div id="waiting" class="tab-pane fade in active">
            <h3>Это есть Ждун. Он ждет доставку / гарантию / ЗП и/или решения клиентов / и т.д.</h3>
            <table class="table table-bordered table-condensed" id="theWaitingTikets">
                <thead>
                    <tr>
                        <th></th>
                        <th>№ акта</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Тип устройства</th>
                        <th>Наименование техники</th>
                        <th>Cтатус</th>
                        <th>Состояние</th>
                        <th>Оповещен</th>
                        <th>Дата создания</th>
                        <th></th>
                        {if in_array($logined.rights, array('admin'))}
                        <th></th>
                        {/if}
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
//bolt
$(document).ready(function () {
    var helper;
    var dt = $("#theFuckingTikets").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 50,
        "aLengthMenu": [5, 10, 25, 50, 100, 200, 500],
        "order": [[1, "desc"]],
        "ajax": {
            url: "/workspace/",
            data: function (queryData) {
                queryData.dataAction = "processTikets",
                    queryData.statuscode = "bolt",
                    queryData.managersFilterForm = $("#tiketsMastersFilterForm").serializeArray()
            }
        },
        "columns": [
            { data: null, class: "tiketDetails", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            { data: 'actnum' },
            { data: 'surname' },
            { data: 'firstname' },
            { data: 'phone' },
            { data: 'id_typesofequipment' },
            { data: 'productname' },
            { data: 'id_statuses', searchable: false },
            { data: 'id_tiketState', searchable: false },
            { data: 'is_notified', searchable: false },
            { data: 'admissiondate' },
            { data: 'printReceipt', class: "receiptPrint", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            {if in_array($logined.rights, array('admin'))}
            {literal}{ data: 'delItemFromTable', class: "tiketDelete", orderable: false, sortable: false, searchable: false, defaultContent: "" }, {/literal}
            {/if}
        ],
        "columnDefs": [
            {
                "targets": 0,
                "render": function (data, type, rowData, json) {
                    var html = '<span class="glyphicon glyphicon-info-sign tiketDetailsModal" dataID="' + rowData.actnum + '" style="cursor: pointer; font-size: 25px;"></span>';
                    return html;
                }
            },//info
            {
                "targets": 1,
                "render": function (data, type, rowData, json) {
                    return '<a target="_blank" href="/tiketPreview/' + data + '/">' + data + '</a>';
                }
            },//tiketPreview
            {
                "targets": 5,
                "render": function (data, type, rowData, json) {
                    helper = json.settings.json.helper;
                    var html = '';
                    var equipments = helper.equipments;
                    for (eq in equipments) {
                        if (equipments[eq].id == data) {
                            html += equipments[eq].equipmentname;
                        }
                    }
                    return html;
                }
            },//typesofequipment
            {
                "targets": 6,
                "createdCell": function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    {if in_array($logined.rights, array('admin', 'manager'))} {literal}
                            $(renderedTD).attr('contenteditable', 'true');
                            $(renderedTD).attr('class', 'editableTD');
                            $(renderedTD).attr('dataName', 'productname');
                            $(renderedTD).attr('dataParent', 'tikets');
                            $(renderedTD).attr('dataId', rowData.tktID);
                        {/literal}
                    {/if}
                }
            }, //productname
            {
                "targets": 7,
                "render": function (data, type, rowData, json) {
                    var tktStatuses = helper.tktStatuses;
                    {if in_array($logined.rights, array('admin'))}
                    var html = '<select dataID="' + rowData.tktID + '" dataName="id_statuses" dataParent="tikets" class="form-control editableContent">';
                    for (status in tktStatuses) {
                        if (tktStatuses[status].id == data) {
                            html += '<option selected value="' + tktStatuses[status].id + '">' + tktStatuses[status].statusname + '</option>';
                        } else {
                            html += '<option value="' + tktStatuses[status].id + '">' + tktStatuses[status].statusname + '</option>';
                        }
                    }
                    html += "</select>";
                    return html;
                    {else}
                        for (status in tktStatuses) {
                            if (tktStatuses[status].id == data) {
                                return tktStatuses[status].statusname;
                            }
                        }
                    {/if}
                }
            },//id_statuses
            {
                "targets" : 8,
                    "render" : function(data, type, rowData, json) {
                        var html = '<select dataID="' + rowData.tktID + '" dataName="id_tiketState" dataParent="tikets" class="form-control editableContent id_tiketState">';
                        var tktStates = helper.tktStates;
                        for (state in tktStates) {
                            if (tktStates[state].id == data) {
                                html += '<option selected value="' + tktStates[state].id + '">' + tktStates[state].tktState + '</option>';
                            } else {
                                html += '<option value="' + tktStates[state].id + '">' + tktStates[state].tktState + '</option>';
                            }
                        }
                        html += "</select>";
                        return html;
                    },
                "createdCell": function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    $(renderedTD).parent().attr("style", getTableColColor(data));
                }
            },//tiketState
            {
                "targets" : 9,
                    "render" : function(data, type, rowData, json) {
                        var html = '';
                        html += '<div class="row"> <div class=col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">';
                        html += '<label class="label label-danger radioLabel">';
                        html += '<input class="inLabel radioStatus" type="radio" ';
                        html += data == 0 ? 'checked="true" ' : ' ';
                        html += 'value="0" name="is_notified_' + rowData.tktID + '" dataID="' + rowData.tktID + '" dataName="is_notified" dataParent="tikets"> </label>';
                        html += '</div>';
                        html += '<div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;"> <label class="label label-primary radioLabel">';
                        html += '<input class="inLabel radioStatus" type="radio" ';
                        html += data == 3 ? 'checked="true" ' : ' ';
                        html += 'value="3" name="is_notified_' + rowData.tktID + '" dataID="' + rowData.tktID + '" dataName="is_notified" dataParent="tikets">';
                        html += '</label>';
                        html += '</div>';
                        html += '<div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">';
                        html += '<label class="label label-success radioLabel">';
                        html += '<input class="inLabel radioStatus" type="radio" ';
                        html += data == 1 ? 'checked="true" ' : ' ';
                        html += 'value="1" name="is_notified_' + rowData.tktID + '" dataID="' + rowData.tktID + '" dataName="is_notified" dataParent="tikets">';
                        html += '</label>';
                        html += '</div>';
                        html += '</div>';
                        return html;
                    },
                "createdCell": function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    $(renderedTD).attr('data-order', data);
                    $(renderedTD).attr('style', "padding: 8 10 0 31px !important;");
                }
            },//notified
            {
                "targets" : 10,
                    "render" : function(data, type, rowData, json) {
                        return data;
                    }
            },//date
            {
                "targets" : 11,
                "render" : function(data, type, rowData, json) {
                    var html = '';
                    html += '<a target="_blank" href="/receipt_print/' + rowData.tktID + '">';
                    html += '<span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;color: red;"></span></a>';
                    return html;
                }
            },//printTiket
            {if in_array($logined.rights, array('admin'))}
            {literal}
            {
                "targets" : 12,
                "render" : function(data, type, rowData, json) {
                    return '<span class="btn glyphicon glyphicon-trash delItemFromTable" dataID="' + rowData.tktID + '" dataParent="tikets"></span>';
                }
            },//removeTiket
            {/literal}
            {/if}
        ]
    });
//bolt
    var dt1 = $("#theWaitingTikets").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 50,
        "aLengthMenu": [5, 10, 25, 50, 100, 200, 500],
        "order": [[1, "desc"]],
        "ajax": {
            url: "/workspace/",
            data: function (queryData) {
                queryData.dataAction = "processTikets",
                    queryData.statuscode = "waiting",
                    queryData.managersFilterForm = $("#tiketsMastersFilterForm").serializeArray()
            }
        },
        "columns": [
            { data: null, class: "tiketDetails", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            { data: 'actnum' },
            { data: 'surname' },
            { data: 'firstname' },
            { data: 'phone' },
            { data: 'id_typesofequipment' },
            { data: 'productname' },
            { data: 'id_statuses', searchable: false },
            { data: 'id_tiketState', searchable: false },
            { data: 'is_notified', searchable: false },
            { data: 'admissiondate' },
            { data: 'printReceipt', class: "receiptPrint", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            {if in_array($logined.rights, array('admin'))}
            {literal}
            { data: 'delItemFromTable', class: "tiketDelete", orderable: false, sortable: false, searchable: false, defaultContent: "" }, 
            {/literal}
            {/if}
        ],
        "columnDefs": [
            {
                "targets": 0,
                "render": function (data, type, rowData, json) {
                    var html = '<span class="glyphicon glyphicon-info-sign tiketDetailsModal" dataID="' + rowData.actnum + '" style="cursor: pointer; font-size: 25px;"></span>';
                    return html;
                }
            },//info
            {
                "targets": 1,
                "render": function (data, type, rowData, json) {
                    return '<a target="_blank" href="/tiketPreview/' + data + '/">' + data + '</a>';
                }
            },//tiketPreview
            {
                "targets": 5,
                "render": function (data, type, rowData, json) {
                    helper = json.settings.json.helper;
                    var html = '';
                    var equipments = helper.equipments;
                    for (eq in equipments) {
                        if (equipments[eq].id == data) {
                            html += equipments[eq].equipmentname;
                        }
                    }
                    return html;
                }
            },//typesofequipment
            {
                "targets": 6,
                "createdCell": function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    {if in_array($logined.rights, array('admin', 'manager'))}
                    {literal}
                    $(renderedTD).attr('contenteditable', 'true');
                    $(renderedTD).attr('class', 'editableTD');
                    $(renderedTD).attr('dataName', 'productname');
                    $(renderedTD).attr('dataParent', 'tikets');
                    $(renderedTD).attr('dataId', rowData.tktID);
                    {/literal}
                    {/if}
                }
            }, //productname
            {
                "targets": 7,
                "render": function (data, type, rowData, json) {
                    var tktStatuses = helper.tktStatuses;
                    {if in_array($logined.rights, array('admin'))}
                    var html = '<select dataID="' + rowData.tktID + '" dataName="id_statuses" dataParent="tikets" class="form-control editableContent">';
                    for (status in tktStatuses) {
                        if (tktStatuses[status].id == data) {
                            html += '<option selected value="' + tktStatuses[status].id + '">' + tktStatuses[status].statusname + '</option>';
                        } else {
                            html += '<option value="' + tktStatuses[status].id + '">' + tktStatuses[status].statusname + '</option>';
                        }
                    }
                    html += "</select>";
                    return html;
                    {else}
                    for (status in tktStatuses) {
                        if (tktStatuses[status].id == data) {
                            return tktStatuses[status].statusname;
                        }
                    }
                    {/if}
                }
            },//id_statuses
            {
                "targets" : 8,
                    "render" : function(data, type, rowData, json) {
                        var html = '<select dataID="' + rowData.tktID + '" dataName="id_tiketState" dataParent="tikets" class="form-control editableContent id_tiketState">';
                        var tktStates = helper.tktStates;
                        for (state in tktStates) {
                            if (tktStates[state].id == data) {
                                html += '<option selected value="' + tktStates[state].id + '">' + tktStates[state].tktState + '</option>';
                            } else {
                                html += '<option value="' + tktStates[state].id + '">' + tktStates[state].tktState + '</option>';
                            }
                        }
                        html += "</select>";
                        return html;
                    },
                "createdCell": function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    $(renderedTD).parent().attr("style", getTableColColor(data));
                }
            },//tiketState
            {
                "targets" : 9,
                    "render" : function(data, type, rowData, json) {
                        var html = '';
                        html += '<div class="row"> <div class=col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">';
                        html += '<label class="label label-danger radioLabel">';
                        html += '<input class="inLabel radioStatus" type="radio" ';
                        html += data == 0 ? 'checked="true" ' : ' ';
                        html += 'value="0" name="is_notified_' + rowData.tktID + '" dataID="' + rowData.tktID + '" dataName="is_notified" dataParent="tikets"> </label>';
                        html += '</div>';
                        html += '<div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;"> <label class="label label-primary radioLabel">';
                        html += '<input class="inLabel radioStatus" type="radio" ';
                        html += data == 3 ? 'checked="true" ' : ' ';
                        html += 'value="3" name="is_notified_' + rowData.tktID + '" dataID="' + rowData.tktID + '" dataName="is_notified" dataParent="tikets">';
                        html += '</label>';
                        html += '</div>';
                        html += '<div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">';
                        html += '<label class="label label-success radioLabel">';
                        html += '<input class="inLabel radioStatus" type="radio" ';
                        html += data == 1 ? 'checked="true" ' : ' ';
                        html += 'value="1" name="is_notified_' + rowData.tktID + '" dataID="' + rowData.tktID + '" dataName="is_notified" dataParent="tikets">';
                        html += '</label>';
                        html += '</div>';
                        html += '</div>';
                        return html;
                    },
                "createdCell": function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    $(renderedTD).attr('data-order', data);
                    $(renderedTD).attr('style', "padding: 8 10 0 31px !important;");
                }
            },//notified
            {
                "targets" : 10,
                    "render" : function(data, type, rowData, json) {
                        return data;
                    }
            },//date
            {
                "targets" : 11,
                    "render" : function(data, type, rowData, json) {
                        var html = '';
                        html += '<a target="_blank" href="/receipt_print/' + rowData.tktID + '">';
                        html += '<span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;color: red;"></span></a>';
                        return html;
                    }
            },//printTiket
            {if in_array($logined.rights, array('admin'))}
            {literal}
            {
                "targets" : 12,
                "render" : function(data, type, rowData, json) {
                    return '<span class="btn glyphicon glyphicon-trash delItemFromTable" dataID="' + rowData.tktID + '" dataParent="tikets"></span>';
                }
            },//removeTiket
            {/literal}
            {/if}
        ]
    });
});
</script>

{literal}
<style>
    tr.odd td,
    tr.even td {
        vertical-align: middle !important;
    }

    .delItemFromTable {
        padding: 2px 10px;
        font-size: 18px;
    }

    .tiketDetails,
    .remove-control {
        cursor: pointer;
        text-align: center;
        font-size: 25px;
        padding-top: 10px !important;
    }
</style>
{/literal}