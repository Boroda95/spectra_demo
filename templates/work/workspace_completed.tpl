<div class="container" style="width: 99% !important;">
    <h2>Это есть готовые к выдаче заказы.</h2>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#toDoList" style="background-color: white !important; border-color: white !important;">
                <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                    Заметки <span class="badge">{count($toDoListArr)}</span>
                </h4>
            </div>
            <div id="toDoList" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;" >
                <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                    <div id="theTaskListTableContainer">
                    {include 'work/toDoListTable.tpl'}
                    </div>
                    <p><br></p> 
                    <form action="/workspace/" method="POST" class="addNewTask" id="addNewTask">
                    <div class="row">
                        <div class="col-md-10">
                            <textarea placeholder="Текст заметки" class="stepFormArea form-control" rows="4" name="taskText" style="width: 100%; overflow-x: hidden; resize: none;" required></textarea>
                        </div>
                        <div class="col-md-2">
                        <button type="submit" name="addNewTask" value="completed" class="btn btn-success">
                            Добавить заметку
                        </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#tasksList" style="background-color: white !important; border-color: white !important;">
                <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                    Задачи <span class="badge">{count($tasksListArr)}</span>
                </h4>
            </div>
            <div id="tasksList" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;" >
                <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                    <div id="theNewTasksListTableContainer">
                    {include 'work/tasksListTable.tpl'}
                    </div>
                    <p><br></p> 
                    <form action="/workspace/" method="POST" class="addNewTaskToTasks" id="addNewTaskToTasks">
                    <div class="row">
                        <div class="col-md-10">
                            <textarea placeholder="Текст заметки" class="stepFormArea form-control" rows="4" name="taskText" style="width: 100%; overflow-x: hidden; resize: none;" required></textarea>
                        </div>
                        <div class="col-md-2">
                        <button type="submit" name="addNewTaskToTasks" value="completed" class="btn btn-success">
                            Добавить задачу
                        </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-bordered table-condensed" id="theTikets">
        <thead>
        <tr>
            <th></th>
            <th>№ акта</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Телефон</th>
            <th>Тип устройства</th>
            <th>Наименование техники</th>
            <th>Cтатус</th>
            <th>Состояние</th>
            <th>Оповещен</th>
            <th>Дата создания</th>
            <th></th>
            {if in_array($logined.rights, array('admin'))}
            <th></th>
            {/if}
        </tr>
        </thead>
    <tbody>
      {for $foo = 0 to count($theTikets.data)-1}
      {$tiket = $theTikets.data.$foo}
      {$refs = $theTikets.refs}
      <tr>
        <td data-toggle="modal" data-target=".myModal{$tiket.tiket.tktID}" style="cursor: pointer; font-size: 25px;">
          <span class="glyphicon glyphicon-info-sign"></span>
        </td>
        {foreach item=colData key=colKey from=$config.mainCols}
          {$colParent = $colData.parent}
          {$curentColumn = $config.$colParent.columns.$colKey}
          {if $curentColumn.colType == 'select'}
            {if $colKey != "masters"}
            {if in_array($logined.rights, array('remontnik')) && $colKey == 'id_statuses' || in_array($logined.rights, array('remontnik','moderator','manager','admin')) && $colKey == 'id_typesofequipment'}
              {foreach item=refData key=refKey from=$refs.$colKey}
                {$dataKey = $curentColumn.relatedKEYData}
              {if $refData.id == $tiket.tiket.$colKey}
                <td>{$refData.$dataKey}</td>
              {/if} 
              {/foreach}
            {else}
              <td>
                <select class="editableContent {$colKey}" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                {foreach item=refData key=refKey from=$refs.$colKey}
                  {$dataKey = $curentColumn.relatedKEYData}
                {if $refData.id == $tiket.tiket.$colKey}
                  <option selected value="{$refData.id}">{$refData.$dataKey}</option>
                {else}
                  <option value="{$refData.id}">{$refData.$dataKey}</option>
                {/if} 
                {/foreach}
                </select>
              </td>
            {/if}
            {/if}
          {elseif $curentColumn.colType == 'edit' && $curentColumn.expressEditing == 'yes' && $colData.editable == 'true'}
            {if in_array($logined.rights, array('admin','manager'))} 
            <td dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" id="{$colKey}" dataParent="{$colParent}" contenteditable="true" class="editableTD">
                {$tiket.tiket.$colKey}
            </td>
            {else}
              <td id="{$colKey}">
                {$tiket.tiket.$colKey}
              </td>
            {/if}
          {elseif $curentColumn.colType == 'radioTandem' && $curentColumn.expressEditing == 'yes'}
            <td style="padding: 8 10 0 31px !important;">
                <div class="row">
                    <div class=col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                        <label class="label label-danger radioLabel">
                        <input class="inLabel radioStatus" type="radio" {if $tiket.tiket.$colKey == 0}checked="true"{/if} value="0" name="{$colKey}{$tiket.tiket.tktID}" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {$curentColumn.tandemItems[0]}
                        </label>
                    </div>
                    <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                        <label class="label label-primary radioLabel">
                        <input class="inLabel radioStatus" type="radio" {if $tiket.tiket.$colKey == 3}checked="true"{/if} value="3" name="{$colKey}{$tiket.tiket.tktID}" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {$curentColumn.tandemItems[2]}
                        </label>
                    </div>
                    <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                        <label class="label label-success radioLabel">
                        <input class="inLabel radioStatus" type="radio" {if $tiket.tiket.$colKey == 1}checked="true"{/if} value="1" name="{$colKey}{$tiket.tiket.tktID}" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {$curentColumn.tandemItems[1]}
                        </label>
                    </div>
                </div>
            </td>
          {else}
            {if $colKey == 'actnum'}
              <td style="cursor: pointer; font-size: 17px;">
                <a target="_blank" href="/tiketPreview/{$tiket.tiket.actnum}/">{$tiket.tiket.$colKey}</a>
              </td>
            {else}
              <td id="{$colKey}">{$tiket.tiket.$colKey}</td>
            {/if}
          {/if}
        {/foreach}
        <td><a target="_blank" href="/receipt_print/{$tiket.tiket.tktID}"><span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px; color:red;"></span></a></td>
        {if in_array($logined.rights, array('admin'))}
          <td><span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$tiket.tiket.tktID}" dataParent="tikets"></span></td>
        {/if}
      </tr>
      {/for}
    </tbody>
  </table>
</div>
{for $foo = 0 to count($theTikets.data)-1}
    {$tiket = $theTikets.data.$foo.tiket}
    {$steps = $theTikets.data.$foo.steps}
    <div class="modal fade bs-example-modal-lg  myModal{$tiket.tktID}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg megaModal" style="width: 100%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <a target="_blank" href="/tiket_print/{$tiket.tktID}">
              <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px;"></span>
            </a>
            {if in_array($logined.rights, array('admin','manager','moderator'))}
            <a href="/printClientData/{$tiket.clntID}">
              <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; color: green;"></span>
            </a>
            <span class="glyphicon glyphicon-send sendSMSModal glyphiconColls" id="sendSmsModalShow" 
                  activedata="{$tiket.clntID}"  style="cursor: pointer; font-size: 25px;">
            </span>
            {/if}
            <br/>
            <div class="row">
              {foreach item=colData key=colKey from=$config.mainCols}
              {$colParent = $colData.parent}
              {$curentColumn = $config.$colParent.columns.$colKey}
              {if $colKey == 'id_statuses' || $colKey == 'id_typesofequipment' || $colKey == 'id_tiketState'}
                {if in_array($logined.rights, array('remontnik')) && $colKey == 'id_statuses' && $tiket.id_statuses == '3' || 
                    in_array($logined.rights, array('manager')) && ($colKey == 'id_statuses' || $colKey == 'id_tiketState') && $tiket.id_statuses == '3' || 
                    in_array($logined.rights, array('remontnik','moderator','manager')) && $colKey == 'id_typesofequipment'}
                  {foreach item=refData key=refKey from=$refs.$colKey}
                    {$dataKey = $curentColumn.relatedKEYData}
                  {if $refData.id == $tiket.$colKey}
                    <div class="col-md-2 {$colKey}" value="{$refData.id}">{$refData.$dataKey}</div>
                  {/if} 
                  {/foreach}
                {else}
                  <div class="col-md-2">
                    <select class="editableContent {$colKey}" dataID="{$tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                    {foreach item=refData key=refKey from=$refs.$colKey}
                      {$dataKey = $curentColumn.relatedKEYData}
                    {if $refData.id == $tiket.$colKey}
                      <option selected value="{$refData.id}">{$refData.$dataKey}</option>
                    {else}
                      <option value="{$refData.id}">{$refData.$dataKey}</option>
                    {/if} 
                    {/foreach}
                    </select>
                  </div>
                {/if}
              {/if}
              {/foreach}
            </div>
            <br>
            <table class="table table-bordered table-condensed table-hover" id="ssanaya_tbl" style="margin-bottom: 0px !important; width: 60% !important; float: left;">
              <tr style="font-size: 17px; font-weight: bold;">
                <td colspan="2">Акт приема оборудования в ремонт №:</td>
                <td class="success" style="text-align: center;">{$tiket.actnum}</td>
                <td class="danger">{$tiket.admissiondate}</td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold;">
                <td>Заказчик (Ф.И.О.):</td>
                <td class="danger">
                  <span>{$tiket.surname}</span>
                  <span>{$tiket.firstname}</span> 
                  <span>{$tiket.middlename}</span> 
                </td>
                <td>Моб. Тел.:</td>
                <td class="danger"><span id='clientLogin'>{$tiket.phone}</span> {if $tiket.secondphone} или {$tiket.secondphone}{/if}</td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold;">
                <td>Название оборудования:</td>
                <td class="danger">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="productname" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.productname}
                  </div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.productname}
                  </div>
                  {/if}
                </td>
                <td>Комплектность:</td>   <!-- --------------------------------------- -->
                <td class="danger">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-10" dataID="{$tiket.tktID}" dataName="completeness" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.completeness}
                  </div>
                  {else}
                  <div class="col-md-10" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.completeness}
                  </div>
                  {/if}
                </td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold;">
                <td colspan="2">Неисправность оборудования со слов клиента:</td>
                <td rowspan="2" colspan="2" class="danger">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="faultdescription" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.faultdescription}
                  </div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.faultdescription}
                  </div>
                  {/if}
                </td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold;"> 
                <td>Серийный номер:</td>
                <td class="danger">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="productseries" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.productseries}
                  </div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.productseries}
                  </div>
                  {/if}
                </td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td style="vertical-align: inherit;">Внешний вид:</td>
                <td class="danger" colspan="3" style="vertical-align: inherit;">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="exteriorview" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.exteriorview}
                  </div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.exteriorview}
                  </div>
                  {/if}
                </td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td style="vertical-align: inherit;">Комментарии:</td>
                <td class="danger" colspan="3" style="vertical-align: inherit;">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="tktcomment" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.tktcomment} 
                  </div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.tktcomment} 
                  </div>
                  {/if}
                </td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold;">
                <td style="vertical-align: inherit;">Минипрайс:</td>
                <td class="danger" colspan="3" style="vertical-align: inherit;">
                  {if in_array($logined.rights, array('admin'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="miniPrice" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px; white-space: pre;">{$tiket.miniPrice}</div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px; white-space: pre;">{$tiket.miniPrice} </div>
                  {/if}
                </td>
              </tr>
              <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td class="info" style="vertical-align: inherit;">Скрытое поле:</td>
                <td class="info" colspan="3" style="vertical-align: inherit;">
                  {if in_array($logined.rights, array('admin','manager'))}
                  <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="hiddenstatus" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.hiddenstatus} 
                  </div>
                  {else}
                  <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                    {$tiket.hiddenstatus} 
                  </div>
                  {/if}
                </td>
              </tr>
            </table>
            <div style="width: 40% !important; float: left;">
              <h3 style="margin-top: 0px;">История заказов клиента:</h3>
              {$curID = $tiket.tktID}
              {foreach from=$theTikets.tiketHistory.$curID item=historyItem key=historyKey}
                {if $historyItem.statuscode == 'during'}
                  {if $historyItem.tktId == $tiket.tktID}
                  <span class="label label-primary" class="tHistoryLabel">#{$historyItem.actnum}</span>
                  {else}
                  <span class="label label-primary" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                  {/if}
                {elseif $historyItem.statuscode == 'completed'}
                  <span class="label label-success" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                {elseif $historyItem.statuscode == 'archive'}
                  <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                {elseif $historyItem.statuscode == 'bolt'}
                  <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                {/if}
              {/foreach}
            </div>
          </div>
          <div class="modal-body">
            <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
              <div class="panel-heading" style="background-color: white !important; border-color: white !important;"><h4><strong>Этапы:</strong></h4></div>
                <table class="table table-responsive table-hover" id="theStepsTable">
                    {include 'work/stepsTable.tpl'}
                </table>
              <p style="width: 100%; border-bottom: 2px solid gray;"><br></p>
              <form action="/workspace/" method="POST" enctype="multipart/form-data" class="addNewStage" id="addNewStage" stpsTblID="theStepsTable{$tiket.tktID}" tiketId="{$tiket.tktID}">
                <div class="row">
                  <div class="col-md-2">
                    <div class="theCbbxTextarea">
                      <textarea placeholder="Заголовок этапа" class="stepFormArea" rows="5" cols="45" name="stagename" style="width: 100%;" required></textarea>
                      <div class="btn-group">
                        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
                          <span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
                        </button>
                        <ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
                          {foreach item=titleItemsData key=titleItemskey from=$stepTitleItemsTable}
                          <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$titleItemsData.items}</li>
                          {/foreach}
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="theCbbxTextarea">
                      <textarea placeholder="Описание" class="stepFormArea" rows="5" cols="45" name="stagedescription" style="width: 100%;" required></textarea>
                      <div class="btn-group">
                        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
                          <span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
                        </button>
                        <ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
                          {foreach item=descrItemsData key=descrItemskey from=$stepDescrItemsTable}
                          <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$descrItemsData.items}</li>
                          {/foreach}
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <textarea placeholder="Скрытое описание" class="stepFormArea" rows="5" cols="45" name="hidstagedescription" style="width: 100%;"></textarea>
                  </div>
                  <div class="col-md-3" id="stepFileInputContainer">
                    <div class="row" style="padding: 0 !important;">
                      <div class="prev"></div>
                    </div>
                    <p><br></p>
                    <input type="file" name="stageimages[]" class="images" id="stepFormFileInput" multiple>
                  </div>
                </div>
                <p><br></p>  
                <div class="row">
                  <div class="col-md-12">
                    <button type="submit" name="addNewStage" value="{$tiket.tktID}"  class="btn btn-success" style="float: right; margin-right: 30px;">
                      Добавить
                    </button>  
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-right: 28px;">
                Закрыть
            </button>
          </div>
        </div>
      </div>
    </div>
  {/for}
{include 'work/modals/sendSMSModal.tpl'}

<script type="text/javascript">
    $("#theTikets").dataTable({
    language: {
      "processing": "Подождите...",
      "search": "Поиск по таблице:",
      "lengthMenu": "Показать _MENU_ записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
      "infoEmpty": "Записи с 0 до 0 из 0 записей",
      "infoFiltered": "(отфильтровано из _MAX_ записей)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "zeroRecords": "Записи отсутствуют.",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
        "first": "Первая",
        "previous": "Предыдущая",
        "next": "Сюда",
        "last": "Туда"
      },
      "aria": {
        "sortAscending": ": активировать для сортировки столбца по возрастанию",
        "sortDescending": ": активировать для сортировки столбца по убыванию"
      }
    },
    "columns": [
        {
          "orderable": false
        },
        null,
        null,
        null,
        null,
        null,
        null,
        {
          "orderable": false
        },
        {
          "orderable": false
        },
        null,
        {
          "orderable": false
        },
        {if in_array($logined.rights, array('admin'))}
        {
          "orderable": false
        },
        {/if}
        {
          "orderable": false
        }
    ],
    "aLengthMenu": [ 100, 250, 500, 1000 ],
    "order": [[ 1, "desc" ]]
  });
</script>
{literal}
<style>
    tr.odd td,
    tr.even td {
        vertical-align: middle !important;
    }

    .delItemFromTable {
        padding: 2px 10px;
        font-size: 18px;
    }

    .tiketDetails,
    .remove-control {
        cursor: pointer;
        text-align: center;
        font-size: 25px;
        padding-top: 10px !important;
    }
</style>
{/literal}