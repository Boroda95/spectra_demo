<div class="container" style="width: 99% !important;">
    <h2>Это есть архив</h2>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#toDoList" style="background-color: white !important; border-color: white !important;">
                <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                    Заметки <span class="badge">{count($toDoListArr)}</span>
                </h4>
            </div>
            <div id="toDoList" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;" >
                <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                    <div id="theTaskListTableContainer">
                    {include 'work/toDoListTable.tpl'}
                    </div>
                    <p><br></p> 
                    <form action="/workspace/" method="POST" class="addNewTask" id="addNewTask">
                    <div class="row">
                        <div class="col-md-10">
                            <textarea placeholder="Текст заметки" class="stepFormArea form-control" rows="4" name="taskText" style="width: 100%; overflow-x: hidden; resize: none;" required></textarea>
                        </div>
                        <div class="col-md-2">
                        <button type="submit" name="addNewTask" value="archive" class="btn btn-success">
                            Добавить заметку
                        </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#tasksList" style="background-color: white !important; border-color: white !important;">
                <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                    Задачи <span class="badge">{count($tasksListArr)}</span>
                </h4>
            </div>
            <div id="tasksList" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;" >
                <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                    <div id="theNewTasksListTableContainer">
                    {include 'work/tasksListTable.tpl'}
                    </div>
                    <p><br></p> 
                    <form action="/workspace/" method="POST" class="addNewTaskToTasks" id="addNewTaskToTasks">
                    <div class="row">
                        <div class="col-md-10">
                            <textarea placeholder="Текст заметки" class="stepFormArea form-control" rows="4" name="taskText" style="width: 100%; overflow-x: hidden; resize: none;" required></textarea>
                        </div>
                        <div class="col-md-2">
                        <button type="submit" name="addNewTaskToTasks" value="archive" class="btn btn-success">
                            Добавить задачу
                        </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <form action="#" id="tiketsMastersFilterForm">
        <input type="hidden" value="*" name="tiketsMastersFilter">
    </form>
  <table class="table table-bordered table-condensed" id="theTikets">
    <thead>
      <tr>
        <th>Акт</th>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Телефон</th>
        <th>Тип устройства</th>
        <th>Наименование техники</th>
        <th>Cтатус</th>
        <th>Состояние</th>
        <th>Дата создания</th>
        <th></th>
      </tr>
    </thead>
  </table>
</div>
<script type="text/javascript">

$(document).ready(function(){
    var detailRows = [];
    var helper;
    var dt = $("#theTikets").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 25,
        "aLengthMenu": [ 5, 10, 25, 50, 100, 200, 500],
        "order": [[ 0, "desc" ]],
        "ajax": {
            url: "/workspace/",
            data: function(queryData){
                queryData.dataAction = "processTikets",
                queryData.statuscode = "archive",
                queryData.managersFilterForm = $("#tiketsMastersFilterForm").serializeArray()
            }
        },
        "columns": [
            { data: 'actnum' },
            { data: 'surname' },
            { data: 'firstname' },
            { data: 'phone' },
            { data: 'id_typesofequipment' },
            { data: 'productname'},
            { data: 'id_statuses', searchable: false },
            { data: 'id_tiketState', searchable: false },
            { data: 'admissiondate' },
            { data: 'printTiket', class: "tiketPrint", orderable: false, sortable: false, searchable: false, defaultContent: "" }
        ],
        "columnDefs": [
            {
                "targets" : 0,
                "render" : function(data, type, rowData, json){
                return '<a target="_blank" href="/tiketPreview/'+data+'/">'+data+'</a>';
                }
            },//tiketPreview
            {
                "targets" : 4,
                "render" : function(data, type, rowData, json){
                    helper = json.settings.json.helper;
                    var html ='';
                    var equipments = helper.equipments;
                    for (eq in equipments) {
                        if(equipments[eq].id == data){
                            html +=equipments[eq].equipmentname;
                        }
                    }
                    return html;
                }
            },//typesofequipment
            {
                "targets" : 5,
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                   {if in_array($logined.rights, array('admin','manager'))}
                   $(renderedTD).attr('contenteditable', 'true'); 
                   $(renderedTD).attr('class', 'editableTD'); 
                   $(renderedTD).attr('dataName', 'productname'); 
                   $(renderedTD).attr('dataParent', 'tikets'); 
                   $(renderedTD).attr('dataId', rowData.tktID);
                   {/if}
                }
            },//productname
            {
                "targets" : 6,
                "render" : function(data, type, rowData, json){
                    var tktStatuses = helper.tktStatuses;
                    {if in_array($logined.rights, array('admin'))}
                        var html ='<select dataID="'+rowData.tktID+'" dataName="id_statuses" dataParent="tikets" class="form-control editableContent master">';
                        for (status in tktStatuses) {
                            if(tktStatuses[status].id == data){
                                html +='<option selected value="'+tktStatuses[status].id+'">'+tktStatuses[status].statusname+'</option>';
                            }else{
                                html +='<option value="'+tktStatuses[status].id+'">'+tktStatuses[status].statusname+'</option>';
                            }
                        }
                        html +="</select>";
                        return html;
                    {else}
                        for (status in tktStatuses) {
                            if(tktStatuses[status].id == data){
                                return tktStatuses[status].statusname;
                            }
                        }
                    {/if}
                }
            },//id_statuses
            {
                "targets" : 7,
                "render" : function(data, type, rowData, json){
                    var html ='<select dataID="'+rowData.tktID+'" dataName="id_tiketState" dataParent="tikets" class="form-control editableContent id_tiketState">';
                    var tktStates = helper.tktStates;
                    for (state in tktStates) {
                        if(tktStates[state].id == data){
                            html +='<option selected value="'+tktStates[state].id+'">'+tktStates[state].tktState+'</option>';
                        }else{
                            html +='<option value="'+tktStates[state].id+'">'+tktStates[state].tktState+'</option>';
                        }
                    }
                    html +="</select>";
                    return html;
                },
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                   $(renderedTD).parent().attr("style",getTableColColor(data));
                }
            },//tiketState
            {
                "targets" : 8,
                "render" : function(data, type, rowData, json){
                    // return data.split(" ")[0];
                    return data;
                }
            },//date
            {
                "targets" : 9,
                "render" : function(data, type, rowData, json){
                    var html = '';
                    html += '<a target="_blank" href="/tiket_print/'+rowData.tktID+'">';
                    html += '<span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;"></span></a>';
                    return html;
                }
            }//printTiket
        ]
    });
}); 
</script>

<style>
    tr.odd td, tr.even td{
        vertical-align: middle !important;
    }
    .delItemFromTable{
        padding: 2px 10px;
        font-size:18px;
    }
    .tiketDetails, .remove-control{
        cursor:pointer;
        text-align: center;
        font-size:25px;
        padding-top: 10px !important;
    }
</style>