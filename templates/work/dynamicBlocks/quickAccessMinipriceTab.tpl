<div class="btn-group-vertical miniPriceMenu inactive" role="group">
    {foreach item=groupTpls key=groupKey from=$miniPriceMenuArr}
    <div class="btn-group customVerticalMenu" role="group">
        <button id="btnGroupVerticalDrop{$groupKey}" type="button" class="btn btn-default dropdown-toggle groupItem"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {$groupTpls.groupName}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop{$groupKey}">
            {foreach item=tplData key=tplKey from=$groupTpls.tplData}
            <li>
                <a href="#" class="groupTplItem">{$tplData.tplName} -
                    <span class="tplItemData">{$tplData.tplDescription}</span>
                </a>
            </li>
            {/foreach}
        </ul>
    </div>
    {/foreach}
</div>
<p><br /></p>
<div class="panel-group" id="accordion">
    <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#groups" style="background-color: white !important; border-color: white !important;">
            <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                Группы
            </h4>
        </div>
        <div id="groups" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;">
            <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                <div id="theTplGroupsTableContainer">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Элемент</th>
                                <th>Удалить</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=groupTpls key=groupKey from=$tiketTplAllGroups}
                            <tr>
                                <td dataID="{$groupTpls.id}" dataName="groupName" dataParent="tiketTplGroups"
                                    contenteditable="true" class="editableTD">
                                    {$groupTpls.groupName}
                                </td>
                                <td>
                                    <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$groupTpls.id}"
                                        dataParent="tiketTplGroups"></span>
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
                <form action="/quickAccessItems/" method="POST" id="addNewTplGroup">
                    <div class="row">
                        <div class="col-md-10">
                            <input type="text" placeholder="Название групы" class="form-control" name="groupName"
                                required>
                            <input type="hidden" name="groupTarget" value="miniprice" required>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" name="addNewTplGroup" value="main" class="btn btn-success">
                                Добавить группу
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#groupTplsData" style="background-color: white !important; border-color: white !important;">
            <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                Шаблоны
            </h4>
        </div>
        <div id="groupTplsData" class="panel-collapse" style="background-color: white !important; border-color: white !important;">
            <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                <div id="theGroupTplsDataTableContainer">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Название шаблона</th>
                                <th>Группа</th>
                                <th>Описание шаблона</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=groupTpls key=groupKey from=$miniPriceMenuArr}
                            {foreach item=tplData key=tplKey from=$groupTpls.tplData}
                            <tr>
                                <td dataID="{$tplData.tplId}" dataName="tplName" dataParent="tiketTplData"
                                    contenteditable="true" class="editableTD">
                                    {$tplData.tplName}
                                </td>
                                <td>
                                    <select dataid="{$tplData.tplId}" dataname="tplParent" dataparent="tiketTplData"
                                        class="form-control editableContent" required>
                                        {foreach item=groupTpls key=groupKey from=$tiketTplAllGroups}
                                        {if $groupTpls.id == $tplData.tplParent}
                                        <option value="{$groupTpls.id}" selected>{$groupTpls.groupName}</option>
                                        {else}
                                        <option value="{$groupTpls.id}">{$groupTpls.groupName}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </td>
                                <td dataID="{$tplData.tplId}" dataName="tplDescription" dataParent="tiketTplData"
                                    contenteditable="true" class="editableTD fuckingMiniPrice">
                                    {str_replace(["|||",";","\r\n","\r"], ["<br>","","",""], $tplData.tplDescription)}
                                </td>
                                <td>
                                    <span class="btn glyphicon glyphicon-remove delItemFromTable" dataID="{$tplData.tplId}"
                                        dataParent="tiketTplData"></span>
                                </td>
                            </tr>
                            {/foreach}
                            {/foreach}
                        </tbody>
                    </table>
                </div>
                <form action="/quickAccessItems/" method="POST" id="addNewTiketTpl">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" name="tplName" placeholder="Название шаблона" class="form-control" required>
                            <textarea name="tplDescription" placeholder="Описание шаблона" class="form-control" rows="7" required></textarea>
                            <select name="tplParent" class="form-control" required>
                                <option value="">Не выбран</option>
                                {foreach item=groupTpls key=groupKey from=$tiketTplAllGroups}
                                <option value="{$groupTpls.id}">{$groupTpls.groupName}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" name="addNewTiketTpl" value="main" class="btn btn-success">
                                Добавить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>