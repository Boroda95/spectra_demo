<script>
    $(document).ready(function(){
        $("#clientFinder").chosen();
        $(".chosen-select").chosen();
    });
</script>
<div class="container" style="width: 99% !important;">
    <h2>Это есть рабочая зона</h2>
    <div id="tickets" class="tab-pane fade in active">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#toDoList" style="background-color: white !important; border-color: white !important;">
                    <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                        Заметки <span class="badge">{count($toDoListArr)}</span>
                    </h4>
                </div>
                <div id="toDoList" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;" >
                    <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                        <div id="theTaskListTableContainer">
                        {include 'work/toDoListTable.tpl'}
                        </div>
                        <p><br></p> 
                        <form action="/workspace/" method="POST" class="addNewTask" id="addNewTask">
                        <div class="row">
                            <div class="col-md-10">
                                <textarea placeholder="Текст заметки" class="stepFormArea form-control" rows="4" name="taskText" style="width: 100%; overflow-x: hidden; resize: none;" required></textarea>
                            </div>
                            <div class="col-md-2">
                            <button type="submit" name="addNewTask" value="main" class="btn btn-success">
                                Добавить заметку
                            </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default" style="background-color: white !important; border-color: white !important;">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#tasksList" style="background-color: white !important; border-color: white !important;">
                    <h4 class="panel-title" style="background-color: white !important; border-color: white !important; cursor:pointer;">
                        Задачи <span class="badge">{count($tasksListArr)}</span>
                    </h4>
                </div>
                <div id="tasksList" class="panel-collapse collapse" style="background-color: white !important; border-color: white !important;" >
                    <div class="panel-body" style="background-color: white !important; border-color: white !important;">
                        <div id="theNewTasksListTableContainer">
                        {include 'work/tasksListTable.tpl'}
                        </div>
                        <p><br></p> 
                        <form action="/workspace/" method="POST" class="addNewTaskToTasks" id="addNewTaskToTasks">
                        <div class="row">
                            <div class="col-md-10">
                                <textarea placeholder="Текст заметки" class="stepFormArea form-control" rows="4" name="taskText" style="width: 100%; overflow-x: hidden; resize: none;" required></textarea>
                            </div>
                            <div class="col-md-2">
                            <button type="submit" name="addNewTaskToTasks" value="main" class="btn btn-success">
                                Добавить задачу
                            </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 managerfiltermonitoring">
                <form action="#" id="tiketsMastersFilterForm">
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1">
                            <div class="input-group checkbox">
                            <label>
                                <span class="bchbox input-group-addon" style="padding:0px; font-size: 19px;">
                                    <input type="checkbox" value="*" checked="checked" name="tiketsMastersFilter" class="chbox tiketsMastersFilter_all form-control" style="height: 20px;">
                                    Все
                                </span>
                            </label>
                            </div>
                        </div>
                        {foreach from=$masters item=master }
                            <div class="col-lg-1 col-md-1 col-sm-1">
                                <div class="input-group checkbox">
                                <label>
                                    <span class="bchbox input-group-addon" for="master_{$master.id}" style="padding:0px; font-size: 19px;">
                                        <input type="checkbox" value="{$master.id}" id="master_{$master.id}" name="tiketsMastersFilter" class="chbox tiketsMastersFilters form-control" style="height: 20px;">
                                        {$master.synonym}
                                    </span>
                                </label>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </form>
            </div>
        </div>
        <div class="tiketsProcess">
            <table class="table table-bordered table-condensed" id="theTikets">
                <thead>
                <tr>
                    <th></th>
                    <th>Акт</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Телефон</th>
                    <th>Тип устройства</th>
                    <th>Наименование техники</th>
                    <th>Ремонтирует</th>
                    <th>Состояние</th>
                    <th style="width: 118px !important;">Оповещен</th>
                    <th>Дата создания</th>
                    <th>Приоритет</th>
                    <th></th>
                    {if in_array($logined.rights, array('admin','manager'))}
                    <th></th>
                    {/if}
                    {if in_array($logined.rights, array('admin'))}
                    <th></th>
                    {/if}
                </tr>
                </thead>
            </table>
        </div>
        {include 'work/modals/sendSMSModal.tpl'}
        {* {include 'work/modals/allClientInfoModal.tpl'} *}
    </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    var detailRows = [];
    var helper;
    var dt = $("#theTikets").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 50,
        "aLengthMenu": [ 5, 10, 25, 50, 100, 200, 500],
        "order": [[ 1, "desc" ]],
        "ajax": {
            url: "/workspace/",
            data: function(queryData){
                queryData.dataAction = "processTikets",
                queryData.statuscode = "during",
                queryData.managersFilterForm = $("#tiketsMastersFilterForm").serializeArray()
            }
        },
        "columns": [
            { data: null, class: "tiketDetails", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            { data: 'actnum' },
            { data: 'surname' },
            { data: 'firstname' },
            { data: 'phone' },
            { data: 'id_typesofequipment' },
            { data: 'productname'},
            { data: 'master', searchable: false },
            { data: 'id_tiketState', searchable: false },
            { data: 'is_notified', searchable: false },
            { data: 'admissiondate' },
            { data: 'priority'},
            { data: 'printTiket', class: "tiketPrint", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            {if in_array($logined.rights, array('admin','manager'))}
            { data: 'printClientData', class: "printClientData", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            {/if}
            {if in_array($logined.rights, array('admin'))}
            { data: 'delItemFromTable', class: "tiketDelete", orderable: false, sortable: false, searchable: false, defaultContent: "" },
            {/if}
        ],
        "columnDefs": [
            {
                "targets" : 0,
                "render" : function(data, type, rowData, json){
                var html = '<span class="glyphicon glyphicon-info-sign tiketDetailsModal" dataID="'+rowData.actnum+'" style="cursor: pointer; font-size: 25px;"></span>';
                return html;
                }
            },//info
            {
                "targets" : 1,
                "render" : function(data, type, rowData, json){
                return '<a target="_blank" href="/tiketPreview/'+data+'/">'+data+'</a>';
                }
            },//tiketPreview
            {
                "targets" : 5,
                "render" : function(data, type, rowData, json){
                    helper = json.settings.json.helper;
                    var html ='';
                    var equipments = helper.equipments;
                    for (eq in equipments) {
                        if(equipments[eq].id == data){
                            html +=equipments[eq].equipmentname;
                        }
                    }
                    return html;
                }
            },//typesofequipment
            {
                "targets" : 6,
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                   {if in_array($logined.rights, array('admin','manager'))}
                   $(renderedTD).attr('contenteditable', 'true'); 
                   $(renderedTD).attr('class', 'editableTD'); 
                   $(renderedTD).attr('dataName', 'productname'); 
                   $(renderedTD).attr('dataParent', 'tikets'); 
                   $(renderedTD).attr('dataId', rowData.tktID);
                   {/if}
                }
            },//productname
            {
                "targets" : 7,
                "render" : function(data, type, rowData, json){
                    var masters = helper.masters;
                    {if !in_array($logined.rights, array('remontnik'))}
                    var html ='<select dataID="'+rowData.tktID+'" dataName="master" dataParent="tikets" class="form-control editableContent master">';
                    if(data == 0){
                        html +='<option selected value="0">Не выбран</option>';
                    }else{
                        html +='<option value="0">Не выбран</option>';
                    }
                    for (master in masters) {
                        if(masters[master].id == data){
                            html +='<option selected value="'+masters[master].id+'">'+masters[master].synonym+'</option>';
                        }else{
                            html +='<option value="'+masters[master].id+'">'+masters[master].synonym+'</option>';
                        }
                    }
                    html +="</select>";
                    return html;
                    {else}
                    if(data == 0){
                        return "Не назначен";
                    }
                    for (master in masters) {
                        if(masters[master].id == data){
                            return masters[master].synonym;
                        }
                    }
                    {/if}
                },
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                    var masters = helper.masters;
                    if(data == 0){
                        $(renderedTD).attr('data-order', "A_Empty");
                    }
                    for (master in masters) {
                        if(masters[master].id == data){
                             $(renderedTD).attr('data-order', masters[master].synonym);
                        }
                    }
                }
            },//masters
            {
                "targets" : 8,
                "render" : function(data, type, rowData, json){
                    var html ='<select dataID="'+rowData.tktID+'" dataName="id_tiketState" dataParent="tikets" class="form-control editableContent id_tiketState">';
                    var tktStates = helper.tktStates;
                    for (state in tktStates) {
                        if(tktStates[state].id == data){
                            html +='<option selected value="'+tktStates[state].id+'">'+tktStates[state].tktState+'</option>';
                        }else{
                            html +='<option value="'+tktStates[state].id+'">'+tktStates[state].tktState+'</option>';
                        }
                    }
                    html +="</select>";
                    return html;
                },
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                   $(renderedTD).parent().attr("style",getTableColColor(data));
                }
            },//tiketState
            {
                "targets" : 9,
                "render" : function(data, type, rowData, json){
                    var html = '';
                    html +='<div class="row"> <div class=col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">';
                            html +='<label class="label label-danger radioLabel">';
                            html +='<input class="inLabel radioStatus" type="radio" ';
                            html += data == 0 ? 'checked="true" ' : ' ';
                            html +='value="0" name="is_notified_'+rowData.tktID+'" dataID="'+rowData.tktID+'" dataName="is_notified" dataParent="tikets"> </label>';
                    html +='</div>';
                    html +='<div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;"> <label class="label label-primary radioLabel">';
                            html +='<input class="inLabel radioStatus" type="radio" ';
                            html += data == 3 ? 'checked="true" ' : ' ';
                            html +='value="3" name="is_notified_'+rowData.tktID+'" dataID="'+rowData.tktID+'" dataName="is_notified" dataParent="tikets">';
                            html +='</label>';
                    html +='</div>';
                    html +='<div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">';
                            html +='<label class="label label-success radioLabel">';
                            html +='<input class="inLabel radioStatus" type="radio" '; 
                            html += data == 1 ? 'checked="true" ' : ' ';
                            html +='value="1" name="is_notified_'+rowData.tktID+'" dataID="'+rowData.tktID+'" dataName="is_notified" dataParent="tikets">';
                        html +='</label>';
                    html +='</div>';
                    html +='</div>';
                    return html;
                },
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                   $(renderedTD).attr('data-order', data);
                   $(renderedTD).attr('style', "padding: 8 10 0 31px !important;");
                }
            },//notified
            {
                "targets" : 10,
                "render" : function(data, type, rowData, json){
                    return data.split(" ")[0];
                }
            },//date
            {
                "targets" : 11,
                "render" : function(data, type, rowData, json){
                    return data;
                },
                "createdCell":  function (renderedTD, data, rowData, rowOrdNum, colOrdNum) {
                   {if !in_array($logined.rights, array('remontnik'))}
                   $(renderedTD).attr('contenteditable', 'true'); 
                   $(renderedTD).attr('dataName', 'priority'); 
                   $(renderedTD).attr('class', 'tiket_priority editableTD'); 
                   $(renderedTD).attr('dataParent', 'tikets'); 
                   $(renderedTD).attr('dataId', rowData.tktID);
                   {/if}
                }
            },//priority
            {
                "targets" : 12,
                "render" : function(data, type, rowData, json){
                    var html = '';
                    html += '<a target="_blank" href="/tiket_print/'+rowData.tktID+'">';
                    html += '<span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;"></span></a>';
                    return html;
                }
            },//printTiket
            {if in_array($logined.rights, array('admin','manager'))}
            {
                "targets" : 13,
                "render" : function(data, type, rowData, json){
                    var html = '';
                    html += '<a href="/printClientData/'+rowData.clntID+'">';
                    html += '<span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px; color: green;"></span></a>';
                    return html;
                }
            },//printClientData
            {/if}
            {if in_array($logined.rights, array('admin'))}
            {
                "targets" : 14,
                "render" : function(data, type, rowData, json){
                    return '<span class="btn glyphicon glyphicon-trash delItemFromTable" dataID="'+rowData.tktID+'" dataParent="tikets"></span>';
                }
            },//removeTiket
            {/if}
        ]
    });
    $('.tiketsMastersFilter_all, .tiketsMastersFilters').on('change', function(){
        if($(this).val() == '*'){
            $('.tiketsMastersFilters').prop('checked', false);
        }else{
            $('.tiketsMastersFilter_all').prop('checked', false);
        }
        if(!$('.tiketsMastersFilters:checkbox:checked').val() && !$('.tiketsMastersFilter_all:checkbox:checked').val()){
            $('.tiketsMastersFilter_all').prop('checked', true);
        }
        $('#theTikets').DataTable().ajax.reload(); 
    });
}); 
</script>

<style>
    tr.odd td, tr.even td{
        vertical-align: middle !important;
    }
    .delItemFromTable{
        padding: 2px 10px;
        font-size:18px;
    }
    .tiketDetails, .remove-control{
        cursor:pointer;
        text-align: center;
        font-size:25px;
        padding-top: 10px !important;
    }
</style>
