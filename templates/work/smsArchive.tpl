<div class="container" style="width: 99% !important;">
	<p><br></p>
  	<h2>Тут отображаются все отправленные клиентам смс сообщения.</h2>
	{include 'work/getSmsArchiveTable.tpl'}
</div>
<script>
	$(document).ready(function(){
		$("#smsArchiveDataTable").dataTable({
		    language: {
		      "processing": "Подождите...",
		      "search": "Поиск по таблице:",
		      "lengthMenu": "Показать _MENU_ записей",
		      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
		      "infoEmpty": "Записи с 0 до 0 из 0 записей",
		      "infoFiltered": "(отфильтровано из _MAX_ записей)",
		      "infoPostFix": "",
		      "loadingRecords": "Загрузка записей...",
		      "zeroRecords": "Записи отсутствуют.",
		      "emptyTable": "В таблице отсутствуют данные",
		      "paginate": {
		        "first": "Первая",
		        "previous": "Предыдущая",
		        "next": "Сюда",
		        "last": "Туда"
		      },
		      "aria": {
		        "sortAscending": ": активировать для сортировки столбца по возрастанию",
		        "sortDescending": ": активировать для сортировки столбца по убыванию"
		      }
		    },
		    "columns": [
		        null,
		        null,
		        null,
		        null,
		        null,
		        null
		    ],
		    "aLengthMenu": [ 50, 100, 500, 1000 ],
		    "order": [[ 0, "asc" ]]
		  });
	});
</script>