<html lang="ru" xml:lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{$title}</title>
  <script type='text/javascript' src="/components/jquery/jquery-3.2.1.min.js"></script>
  <script type='text/javascript' src="/components/bootstrap/js/bootstrap.min.js"></script>
  <link href="/components/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div id="print_act" style="width: 850px; padding: 0px 10px 0px 17px !important;">
  <div class="row" style="padding-top: 25px;">
    <div class="col-md-12" id="logo"><img class="logo" src="/img/logo.png" width="230px;">
      <div style="float: right; text-align: right; font-size: 21px; font-weight: bold" id="contact1">
        <p>ООО «Спектра»</p><p>+7 930 844 44 10</p><p>www.thespectra.ru</p>
      </div></div>
  </div>
  <div style="font-size: 12px; border-bottom: 2px solid black; border-top: 2px solid black; padding: 0 2px;">
    <span>249032, Калужская область, город Обнинск, улица Курчатова, д.41в, оф.15</span> <span style="float: right;">ИНН 4025441043, ОГРН 1144025003792, КПП 402501001</span>
  </div>
  <p><br></p>
  <h2 style="margin-left: 225px;">Этапы проведенной работы:</h2>
  <table border="1" width="80%" cellpadding="2" style="font-size: 20px;">
    <th>
      <td>Заголовок этапа</td><td>Описание этапа</td>
    </th>
    {foreach from=$stages item=stepItem key=stepKey}  
      <tr>
        <td class="title_td">{$stepItem.stagename}</td><td class="info_td">{$stepItem.stagedescription}</td>
      </tr>
    {/foreach}
  </table>
</body>

<style>
  @media print{
    table{
      border: 0 !important;
      border-color: transparent !important;
    }  
    table>td>tr{
      border: 0 !important;
    }
  }
  .title_td{
    width: 30%;
    padding: 4px;
  }
  .info_td{
    width: 70%;
    padding: 4px;
  }
</style>

<script type="text/javascript">
  function PrintElem(elem){
      Popup($(elem).html());
  }
  function Popup(data){
    var hei = screen.height;
    var wid = screen.width;
    var mywindow = window.open('', 'Print client data', 'height='+hei-50+',width='+wid-50);
    mywindow.document.write('<html><head><title>Print client data</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
  }
</script>

