<table class="table table-bordered table-condensed" id="theClients">
<thead>
  <tr>
    {foreach key=cnfClntKey item=cnfClntData from=$config.clients.columns}
    {if $cnfClntData.mainView == 'true'}
    <th>{$cnfClntData.colCaption}</th>
    {/if}
    {/foreach}
  </tr>
</thead>
<tbody>
  {foreach key=clientsKey item=clientsArr from=$clients.data}
  <tr>
    {foreach key=cnfClntKey item=cnfClntData from=$config.clients.columns}
    {if $cnfClntData.colDisplay == 'yes' && $cnfClntData.mainView == 'true'}
      {if $cnfClntData.colType == 'edit' || $cnfClntData.colType == 'method'}
        <td dataID="{$clientsArr.id}" dataName="{$cnfClntKey}" id="{$cnfClntKey}" dataParent="clients" contenteditable="true" class="editableTD">
          {$clientsArr.$cnfClntKey}
        </td>
      {elseif $cnfClntData.colType == 'system'}
        {if $cnfClntData.tag == 'span'}
          <td>
            <span class="{$cnfClntData.settings.class}" id="{$cnfClntData.settings.id}" activedata="{$clientsArr.id}"></span>
          </td> 
        {elseif $cnfClntData.tag == 'delete'}
          <td>
            <span class="{$cnfClntData.settings.class}" id="{$cnfClntData.settings.id}" dataID="{$clientsArr.id}" dataParent="{$cnfClntData.parent}"></span>
          </td>
        {/if}
      {elseif $cnfClntData.colType == 'select'}
      <td>
        <select class="editableContent {$cnfClntKey}_active" dataID="{$clientsArr.id}" dataName="{$cnfClntKey}" dataParent="clients">
        {foreach item=refData key=refKey from=$clients.refs.$cnfClntKey}
          {$dataKey = $cnfClntData.relatedKEYData}
        {if $refData.id == $clientsArr.$cnfClntKey}
          <option selected value="{$refData.id}">{$refData.$dataKey}</option>
        {else}
          <option value="{$refData.id}">{$refData.$dataKey}</option>
        {/if} 
        {/foreach}
        </select>
      </td>
      {else}
        <td>{$clientsArr.$cnfClntKey}</td>
      {/if}
    {/if}
    {/foreach}
  </tr>
  {/foreach}
</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
  $("#theClients").dataTable({
    language: {
      "processing": "Подождите...",
      "search": "Поиск по таблице:",
      "lengthMenu": "Показать _MENU_ записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
      "infoEmpty": "Записи с 0 до 0 из 0 записей",
      "infoFiltered": "(отфильтровано из _MAX_ записей)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "zeroRecords": "Записи отсутствуют.",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
        "first": "Первая",
        "previous": "Предыдущая",
        "next": "Сюда",
        "last": "Туда"
      },
      "aria": {
        "sortAscending": ": активировать для сортировки столбца по возрастанию",
        "sortDescending": ": активировать для сортировки столбца по убыванию"
      }

    },
    "columns": [
        {foreach key=cnfClntKey item=cnfClntData from=$config.clients.columns}
        {if $cnfClntData.mainView == "true"}
          {if $cnfClntData.sortable == 'true'}
          null,
          {else}
          {
            "orderable": false
          },
          {/if}
        {/if}
        {/foreach}
    ],
    "order": [[ 0, 'asc' ]]
  });

});
</script>