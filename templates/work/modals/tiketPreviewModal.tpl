<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="tiketPeviewModal">
    <div class="modal-dialog modal-lg megaModal" style="width: 100%;">
        <div class="modal-content">
        <div class="modal-header" style="padding-top: 35px; padding-bottom: 3px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -25px;">&times;</button>
            <div class="row" style="margin-right: 20px; margin-top: 20px; margin-left: 20px;">
                {$tiketData = $tiketInfo.tiket}
                {$steps = $tiketInfo.steps}
                {foreach item=colData key=colKey from=$config.mainCols}
                    {$colParent = $colData.parent}
                    {$curentColumn = $config.$colParent.columns.$colKey}
                    {if $colKey == 'id_statuses' || $colKey == 'id_typesofequipment' || $colKey == 'id_tiketState'}
                        {if in_array($logined.rights, array('remontnik')) && $colKey == 'id_statuses' && $tiketInfo.id_statuses == '3' || 
                            in_array($logined.rights, array('manager')) && ($colKey == 'id_statuses' || $colKey == 'id_tiketState') && $tiketInfo.id_statuses == '3' || 
                            in_array($logined.rights, array('remontnik','moderator','manager')) && $colKey == 'id_typesofequipment'}
                            {foreach item=refData key=refKey from=$tiketInfo.refs.$colKey}
                                {$dataKey = $curentColumn.relatedKEYData}
                                {if $refData.id == $tiketInfo.$colKey}
                                    <div class="col-md-2 {$colKey}" value="{$refData.id}">{$refData.$dataKey}</div>
                                {/if} 
                            {/foreach}
                        {else}
                            <div class="col-md-2" style="padding-top: 10px;">
                                <select class="editableContent {$colKey} form-control" dataID="{$tiketData.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                                {foreach item=refData key=refKey from=$tiketInfo.refs.$colKey}
                                {$dataKey = $curentColumn.relatedKEYData}
                                {if $refData.id == $tiketData.$colKey}
                                <option selected value="{$refData.id}">{$refData.$dataKey}</option>
                                {else}
                                <option value="{$refData.id}">{$refData.$dataKey}</option>
                                {/if} 
                                {/foreach}
                                </select>
                            </div>
                        {/if}
                    {elseif $colKey == 'masters'}
                        {if in_array($logined.rights, array('remontnik'))}
                            {foreach item=refData key=refKey from=$masters}
                            {if $refData.id == $tiketData.master}
                            <div class="col-md-2 {$colKey}" value="{$refData.id}">{$refData.synonym}</div>
                            {/if} 
                            {/foreach}
                        {else}
                            <div class="col-md-2" style="padding-top: 10px;">
                                <select class="editableContent {$colKey} form-control" dataID="{$tiketData.tktID}" dataName="master" dataParent="{$colParent}">
                                {if $tiketData.master == 0}
                                    <option selected value="0">Не выбран</option>
                                {/if}
                                {foreach item=refData key=refKey from=$masters}
                                {if $refData.id == $tiketData.master}
                                <option selected value="{$refData.id}">{$refData.synonym}</option>
                                {else}
                                <option value="{$refData.id}">{$refData.synonym}</option>
                                {/if} 
                                {/foreach}
                                </select>
                            </div>
                        {/if}
                    {elseif $colKey == 'is_notified'}
                        <div class="col-md-2" style="padding-top: 15px;padding-bottom: 10px; width: 115px;">
                            <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                                <label class="label label-danger radioLabel">
                                <input class="inLabel radioStatus" type="radio" {if $tiketData.$colKey == 0}checked="true"{/if} value="0" name="{$colKey}{$tiketData.tktID}_m" dataID="{$tiketData.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                                {$tiketData.$colKey_1}
                                </label>
                            </div>
                            <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                                <label class="label label-primary radioLabel">
                                <input class="inLabel radioStatus" type="radio" {if $tiketData.$colKey == 3}checked="true"{/if} value="3" name="{$colKey}{$tiketData.tktID}_m" dataID="{$tiketData.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                                {$curentColumn.tandemItems[2]}
                                </label>
                            </div>
                            <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                                <label class="label label-success radioLabel">
                                <input class="inLabel radioStatus" type="radio" {if $tiketData.$colKey == 1}checked="true"{/if} value="1" name="{$colKey}{$tiketData.tktID}_m" dataID="{$tiketData.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                                {$curentColumn.tandemItems[1]}
                                </label>
                            </div>
                        </div>
                    {/if}
                {/foreach}
                <div class="col-md-2" style="padding-top: 15px;">
                    <a target="_blank" href="/tiket_print/{$tiketData.tktID}">
                        <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;"></span>
                    </a>
                    {if in_array($logined.rights, array('admin','manager','moderator'))}
                        <a href="/printClientData/{$tiketData.clntID}">
                            <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px; color: green;"></span>
                        </a>
                        <a target="_blank" href="/receipt_print/{$tiketData.tktID}">
                            <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px; color:red;"></span>
                        </a>
                        <span class="glyphicon glyphicon-send sendSMSModal glyphiconColls" id="sendSmsModalShow" 
                            activedata="{$tiketData.clntID}"  style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;">
                        </span>
                        <a target="_blank" href="/clientPreview/{$tiketData.clntID}">
                            <span class="glyphicon glyphicon-user glyphiconColls" style="text-shadow: white 0 0 3px;"></span>
                        </a>
                    {/if}
                </div>
            </div>
            <br>
            <table class="table table-bordered table-condensed table-hover" id="ssanaya_tbl" style="margin-bottom: 0px !important; width: 60% !important; float: left;">
                <tr style="font-size: 17px; font-weight: bold;">
                    <td colspan="2">Акт приема оборудования в ремонт №:</td>
                    <td class="success" style="text-align: center;">{$tiketData.actnum}</td>
                    <td class="danger">{$tiketData.admissiondate}</td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold;">
                    <td>Заказчик (Ф.И.О.):</td>
                    <td class="danger">
                    <span>{$tiketData.surname}</span>
                    <span>{$tiketData.firstname}</span> 
                    <span>{$tiketData.middlename}</span> 
                    </td>
                    <td>Моб. Тел.:</td>
                    <td class="danger"><span id='clientLogin'>{$tiketData.phone}</span> {if $tiketData.secondphone} или {$tiketData.secondphone}{/if}</td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold;">
                    <td>Название оборудования:</td>
                    <td class="danger">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="productname" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.productname}
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.productname}
                    </div>
                    {/if}
                    </td>
                    <td>Комплектность:</td>   <!-- --------------------------------------- -->
                    <td class="danger">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-10" dataID="{$tiketData.tktID}" dataName="completeness" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.completeness}
                    </div>
                    {else}
                    <div class="col-md-10" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.completeness}
                    </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold;">
                    <td colspan="2">Неисправность оборудования со слов клиента:</td>
                    <td rowspan="2" colspan="2" class="danger">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="faultdescription" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.faultdescription}
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.faultdescription}
                    </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold;"> 
                    <td>Серийный номер:</td>
                    <td class="danger">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="productseries" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.productseries}
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.productseries}
                    </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                    <td style="vertical-align: inherit;">Внешний вид:</td>
                    <td class="danger" colspan="3" style="vertical-align: inherit;">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="exteriorview" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.exteriorview}
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.exteriorview}
                    </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                    <td style="vertical-align: inherit;">Требования клиента <br> при обращении:</td>
                    <td class="danger" colspan="3" style="vertical-align: inherit;">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="tktcomment" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.tktcomment} 
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.tktcomment} 
                    </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold;">
                    <td style="vertical-align: inherit;">Минипрайс:</td>
                    <td class="danger" colspan="3" style="vertical-align: inherit;">
                    {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="miniPrice" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px; white-space: pre;">{$tiketData.miniPrice}</div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px; white-space: pre;">{$tiketData.miniPrice} </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold;">
                    <td>Номер ili карты:</td>
                    <td class="danger">
                        <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                            {$tiketData.creditCardNum}
                        </div>
                    </td>
                    <td>Примерные рамки стоимости:</td>
                    <td class="danger">
                        <div class="col-md-10" style="word-wrap: break-word; margin-left: 10px;">
                            {$tiketData.priceRange}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                    <td class="info" style="vertical-align: inherit;">Скрытое поле:</td>
                    <td class="info" colspan="3" style="vertical-align: inherit;">
                    {if in_array($logined.rights, array('admin','manager'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiketData.tktID}" dataName="hiddenstatus" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.hiddenstatus} 
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.hiddenstatus} 
                    </div>
                    {/if}
                    </td>
                </tr>
                <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                    <td class="info" style="vertical-align: inherit;">Приоритет:</td>
                    <td class="info" colspan="3" style="vertical-align: inherit;">
                    {if !in_array($logined.rights, array('remontnik'))}
                    <div contenteditable="true" class="editableTD col-md-11 tiket_priority" dataID="{$tiketData.tktID}" dataName="priority" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.priority} 
                    </div>
                    {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiketData.priority} 
                    </div>
                    {/if}
                    </td>
                </tr>
            </table>
            <div style="width: 40% !important; float: left;">
            <h3 style="margin-top: 0px;">История заказов клиента:</h3>
            {foreach from=$tiketInfo.tiketHistory item=historyItem key=historyKey}
                {if $historyItem.statuscode == 'during'}
                    {if $historyItem.tktId == $tiketInfo.tktID}
                    <span class="label label-primary" class="tHistoryLabel">#{$historyItem.actnum}</span>
                    {else}
                    <span class="label label-primary" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                    {/if}
                {elseif $historyItem.statuscode == 'completed'}
                    <span class="label label-success" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                {elseif $historyItem.statuscode == 'archive'}
                    <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                {elseif $historyItem.statuscode == 'bolt'}
                    <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                {/if}
            {/foreach}
            </div>
        </div>
        <div class="modal-body" style="padding: 10px; padding-top: 0; padding-bottom: 0;">
            <div class="panel panel-default" style="background-color: white !important; border-color: white !important; padding: 0; margin-bottom: 0px;">
            <div class="panel-heading" style="background-color: white !important; border-color: white !important;" >
                <h4 style="margin-bottom: 0px;"><strong>Этапы:</strong></h4>
            </div>

            <table class="table table-responsive table-hover" id="theStepsTable">
                {include 'work/stepsTable.tpl'}
            </table>
            
            <p style="width: 100%; border-bottom: 2px solid gray; margin-bottom: 1px;"><br></p>
            <form action="/workspace/" method="POST" enctype="multipart/form-data" class="addNewStage" id="addNewStage" stpsTblID="theStepsTable" tiketId="{$tiketData.tktID}">
                <div class="row" style="margin-left: -5px;">
                <div class="col-md-2" style="padding-left: 5px;padding-right: 5px;">
                    <div class="theCbbxTextarea">
                    <textarea placeholder="Заголовок этапа" class="stepFormArea form-control" rows="5" cols="45" name="stagename" style="width: 100%;" required></textarea>
                    <div class="btn-group">
                        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
                        <span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
                        </button>
                        <ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
                        {foreach item=titleItemsData key=titleItemskey from=$stepTitleItemsTable}
                        <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$titleItemsData.items}</li>
                        {/foreach}
                        </ul>
                    </div>
                    </div>
                </div>
                <div class="col-md-3" style="padding-left: 5px;padding-right: 5px;">
                    <div class="theCbbxTextarea">
                    <textarea placeholder="Описание" class="stepFormArea form-control" rows="5" cols="45" name="stagedescription" style="width: 100%;" required></textarea>
                    <div class="btn-group">
                        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
                        <span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
                        </button>
                        <ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
                        {foreach item=descrItemsData key=descrItemskey from=$stepDescrItemsTable}
                        <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$descrItemsData.items}</li>
                        {/foreach}
                        </ul>
                    </div>
                    </div>
                </div>
                <div class="col-md-3" style="padding-left: 5px;padding-right: 5px;">
                    <textarea placeholder="Скрытое описание" class="stepFormArea form-control" rows="5" cols="45" name="hidstagedescription" style="width: 100%;"></textarea>
                </div>
                <div class="col-md-1" style="padding-left: 5px;padding-right: 5px;">
                    <input placeholder="Цена" type="num" class="stepFormArea form-control" name="stagePrice">
                </div>
                <div class="col-md-3" id="stepFileInputContainer">
                    <div class="row" style="padding: 0 !important;">
                    <div class="prev"></div>
                    </div>
                    <input type="file" name="stageimages[]" class="images form-control" id="stepFormFileInput" multiple>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="addNewStage" value="{$tiketData.tktID}"  class="btn btn-success" style="float: right; margin-right: 30px;">
                            Добавить
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <div class="modal-footer" style="padding: 5px;">
            <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-right: 28px;">
                Закрыть
            </button>
        </div>
        </div>
    </div>
</div>
<style>
#tiketPeviewModal{
    padding-right: 8px !important;padding-left: 8px !important;
}
</style>