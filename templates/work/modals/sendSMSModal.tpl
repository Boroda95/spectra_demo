<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="sendSMSModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<p class="leadd"><h4>SmS оповещения</h4></p>
			</div>
			<div class="modal-body" style="padding-bottom: 0px;">
			<div id="smsArchiveTable">
				
			</div>
			<form method="POST" action="/workspace/" role="form" id="sendSMSForm">
				<div class="controls">
					<div class="row">
						<div class="col-md-12 form-group">
							<div class="theCbbxTextarea">
								<label for="form_message">Сообщение *</label>
								<textarea placeholder="Текст сообщения" id="form_message" rows="3" name="smsMessage" class="smsMessageArea form-control" required></textarea>
								

								<div class="counter">Осталось символов: <span id="counter"></span></div>
								<div class="btn-group">
									<button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
										<span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
									</button>
									<ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
									{foreach item=smsItemsData key=smsItemskey from=$smsTemlates}
										<li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$smsItemsData.templateText}</li>
									{/foreach}
									</ul>
								</div>
								<div class="btn-group">
									<button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
										<span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
									</button>
									<ul class="dropdown-menu txtAreaCbbx" style="width: max-content;" id="sedSMSTemplates">

									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer" style="padding: 0; border: none;">
						<input type="submit" name="sendSMS" class="btn btn-success btn-send" value="Отправить SMS">
					</div>
				</div>
				<input type="text" name="clientID" value="" id="smsFormClntID" class="hidden">
				<input type="text" name="clientPhone" value="" id="smsFormClntPhone" class="hidden">
			</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		{if in_array($logined.rights, array('admin'))}
			var maxLetterCount = 210;
		{/if}
		{if in_array($logined.rights, array('manager'))}
			var maxLetterCount = 140;
		{/if}

		$(document).on("input focus", ".smsMessageArea", function(e){
			if (this.value.length >= maxLetterCount){
				this.value = this.value.substr(0, maxLetterCount);
			}
			$('span#counter').empty().html("Осталось символов:"+ (maxLetterCount - this.value.length) + " из "+maxLetterCount)
		});
	});
</script>
