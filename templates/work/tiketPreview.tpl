<div class="container" style="width: 99% !important; padding:0px;">
    <h2>Просмотрщик заказа. </h2>
    <div class="row settingsWrap">
        {foreach item=colData key=colKey from=$config.mainCols}
            {$colParent = $colData.parent}
            {$curentColumn = $config.$colParent.columns.$colKey}
            {if $colKey == 'id_statuses' || $colKey == 'id_typesofequipment' || $colKey == 'id_tiketState'}
                {if in_array($logined.rights, array('remontnik')) && $colKey == 'id_statuses' && $tiket.tiket.id_statuses == '3' || 
                    in_array($logined.rights, array('manager')) && ($colKey == 'id_statuses' || $colKey == 'id_tiketState') && $tiket.tiket.id_statuses == '3' || 
                    in_array($logined.rights, array('remontnik','moderator','manager')) && $colKey == 'id_typesofequipment'}
                    {foreach item=refData key=refKey from=$tiket.refs.$colKey}
                        {$dataKey = $curentColumn.relatedKEYData}
                    {if $refData.id == $tiket.tiket.$colKey}
                        <div class="col-md-2 {$colKey}" value="{$refData.id}">{$refData.$dataKey}</div>
                    {/if} 
                    {/foreach}
                {else}
                    <div class="col-md-2" style="padding-top: 14px;">
                        <select class="editableContent {$colKey} form-control" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {foreach item=refData key=refKey from=$tiket.refs.$colKey}
                        {$dataKey = $curentColumn.relatedKEYData}
                        {if $refData.id == $tiket.tiket.$colKey}
                        <option selected value="{$refData.id}">{$refData.$dataKey}</option>
                        {else}
                        <option value="{$refData.id}">{$refData.$dataKey}</option>
                        {/if}
                        {/foreach}
                        </select>
                    </div>
                {/if}
                {elseif $colKey == 'masters'}
                    {if in_array($logined.rights, array('remontnik'))}
                        {foreach item=refData key=refKey from=$masters}
                        {if $refData.id == $tiket.tiket.master}
                        <div class="col-md-2 {$colKey}" value="{$refData.id}">{$refData.synonym}</div>
                        {/if} 
                        {/foreach}
                    {else}
                        <div class="col-md-2" style="padding-top: 14px;">
                            <select class="editableContent {$colKey} form-control" dataID="{$tiket.tiket.tktID}" dataName="master" dataParent="{$colParent}">
                            {if $tiket.tiket.master == 0}
                                <option selected value="0">Не выбран</option>
                            {/if}
                            {foreach item=refData key=refKey from=$masters}
                            {if $refData.id == $tiket.tiket.master}
                            <option selected value="{$refData.id}">{$refData.synonym}</option>
                            {else}
                            <option value="{$refData.id}">{$refData.synonym}</option>
                            {/if} 
                            {/foreach}
                            </select>
                        </div>
                    {/if}
            {elseif $colKey == 'is_notified'}
                <div class="col-md-2" style="padding-top: 15px;padding-bottom: 5px; width: 125px;">
                    <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                        <label class="label label-danger radioLabel">
                        <input class="inLabel radioStatus" style="left: -5px;" type="radio" {if $tiket.tiket.$colKey == 0}checked="true"{/if} value="0" name="{$colKey}$tiket.tikettktID}_m" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {$curentColumn.tandemItems[0]}
                        </label>
                    </div>
                    <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                        <label class="label label-primary radioLabel">
                        <input class="inLabel radioStatus" style="left: -5px;" type="radio" {if $tiket.tiket.$colKey == 3}checked="true"{/if} value="3" name="{$colKey}{$tiket.tiket.tktID}_m" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {$curentColumn.tandemItems[2]}
                        </label>
                    </div>
                    <div class="col-md-4 labeledRadio" style="padding: 0 !important; width: 28px !important;">
                        <label class="label label-success radioLabel">
                        <input class="inLabel radioStatus" style="left: -5px;" type="radio" {if $tiket.tiket.$colKey == 1}checked="true"{/if} value="1" name="{$colKey}{$tiket.tiket.tktID}_m" dataID="{$tiket.tiket.tktID}" dataName="{$colKey}" dataParent="{$colParent}">
                        {$curentColumn.tandemItems[1]}
                        </label>
                    </div>
                </div>
            {/if}
        {/foreach}
        <div class="col-md-2" style="padding-top: 15px;">
            <a target="_blank" href="/tiket_print/{$tiket.tiket.tktID}">
                <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;"></span>
            </a>
            {if in_array($logined.rights, array('admin','manager','moderator'))}
                <a href="/printClientData/{$tiket.tiket.clntID}">
                    <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px; color: green;"></span>
                </a>
                <a target="_blank" href="/receipt_print/{$tiket.tiket.tktID}">
                    <span class="glyphicon glyphicon-print" style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px; color:red;"></span>
                </a>
                <span class="glyphicon glyphicon-send sendSMSModal glyphiconColls" id="sendSmsModalShow" 
                    activedata="{$tiket.tiket.clntID}"  style="cursor: pointer; font-size: 25px; text-shadow: white 0 0 3px;">
                </span>
                <span class="glyphicon glyphicon-user allClientInfo glyphiconColls" id="moreInfo" activedata="{$tiket.tiket.clntID}" style="text-shadow: white 0 0 3px;"></span>
            {/if}
        </div>
    </div>
    <p style="font-size: 10px;  margin: 0;"><br></p>
    {$steps = $tiket.steps}
    {$tiketHistory = $tiket.tiketHistory}
    {$tiket = $tiket.tiket}
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
    <div class="col=md-12">
        <table class="table table-bordered table-condensed table-hover" id="ssanaya_tbl" style="margin-bottom: 0px !important; width: 60% !important; float: left;">
            <tr style="font-size: 17px; font-weight: bold;">
                <td colspan="2">Акт приема оборудования в ремонт №:</td>
                <td class="success" style="text-align: center;">{$tiket.actnum}</td>
                <td class="danger">{$tiket.admissiondate}</td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold;">
                <td>Заказчик (Ф.И.О.):</td>
                <td class="danger">
                    <span>{$tiket.surname}</span>
                    <span>{$tiket.firstname}</span> 
                    <span>{$tiket.middlename}</span> 
                </td>
                <td>Моб. Тел.:</td>
                <td class="danger"><span id='clientLogin'>{$tiket.phone}</span> {if $tiket.secondphone} или {$tiket.secondphone}{/if}</td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold;">
                <td>Название оборудования:</td>
                <td class="danger">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="productname" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.productname}
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.productname}
                    </div>
                {/if}
                </td>
                <td>Комплектность:</td>
                <td class="danger">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-10" dataID="{$tiket.tktID}" dataName="completeness" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.completeness}
                    </div>
                {else}
                    <div class="col-md-10" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.completeness}
                    </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold;">
                <td colspan="2">Неисправность оборудования со слов клиента:</td>
                <td rowspan="2" colspan="2" class="danger">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="faultdescription" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.faultdescription}
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.faultdescription}
                    </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold;"> 
                <td>Серийный номер:</td>
                <td class="danger">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="productseries" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.productseries}
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.productseries}
                    </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td style="vertical-align: inherit;">Внешний вид:</td>
                <td class="danger" colspan="3" style="vertical-align: inherit;">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="exteriorview" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.exteriorview}
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.exteriorview}
                    </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td style="vertical-align: inherit;">Требования клиента <br> при обращении:</td>
                <td class="danger" colspan="3" style="vertical-align: inherit;">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11 tiket_priority" dataID="{$tiket.tktID}" dataName="tktcomment" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.tktcomment} 
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.tktcomment} 
                    </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold;">
                <td style="vertical-align: inherit;">Минипрайс:</td>
                <td class="danger" colspan="3" style="vertical-align: inherit;">
                {if in_array($logined.rights, array('admin'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="miniPrice" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px; white-space: pre;">{$tiket.miniPrice}</div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px; white-space: pre;">{$tiket.miniPrice} </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold;">
                <td>Номер ili карты:</td>
                <td class="danger">
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.creditCardNum}
                    </div>
                </td>
                <td>Примерные рамки стоимости:</td>
                <td class="danger">
                    <div class="col-md-10" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.priceRange}
                    </div>
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td class="info" style="vertical-align: inherit;">Скрытое поле:</td>
                <td class="info" colspan="3" style="vertical-align: inherit;">
                {if in_array($logined.rights, array('admin','manager'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="hiddenstatus" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.hiddenstatus} 
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.hiddenstatus} 
                    </div>
                {/if}
                </td>
            </tr>
            <tr style="font-size: 13px; font-weight: bold; height: 45px;">
                <td class="info" style="vertical-align: inherit;">Приоритет:</td>
                <td class="info" colspan="3" style="vertical-align: inherit;">
                {if !in_array($logined.rights, array('remontnik'))}
                    <div contenteditable="true" class="editableTD col-md-11" dataID="{$tiket.tktID}" dataName="priority" dataParent="tikets" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.priority} 
                    </div>
                {else}
                    <div class="col-md-11" style="word-wrap: break-word; margin-left: 10px;">
                        {$tiket.priority} 
                    </div>
                {/if}
                </td>
            </tr>
        </table>
        <div style="width: 40% !important; float: left; padding-left: 10px;">
        <h3 style="margin-top: 0px;">История заказов клиента:</h3>
        {$curID = $tiket.tktID}
            {foreach from=$tiketHistory.$curID item=historyItem key=historyKey}
                {if $historyItem.statuscode == 'during'}
                    {if $historyItem.tktId == $tiket.tktID}
                        <span class="label label-primary" class="tHistoryLabel">#{$historyItem.actnum}</span>
                    {else}
                        <span class="label label-primary" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                    {/if}
                {elseif $historyItem.statuscode == 'completed'}
                    {if $historyItem.tktId == $tiket.tktID}
                        <span class="label label-success" class="tHistoryLabel">#{$historyItem.actnum}</span>
                    {else}
                        <span class="label label-success" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                    {/if}
                {elseif $historyItem.statuscode == 'archive'}
                    {if $historyItem.tktId == $tiket.tktID}
                        <span class="label label-default" class="tHistoryLabel">#{$historyItem.actnum}</span>
                    {else}
                        <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                    {/if}
                {elseif $historyItem.statuscode == 'bolt'}
                    {if $historyItem.tktId == $tiket.tktID}
                        <span class="label label-danger" class="tHistoryLabel">#{$historyItem.actnum}</span>
                    {else}
                        <span class="label label-danger" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
                    {/if}
                {/if}
            {/foreach}
        </div>
    </div>
    </div>

    <p style="width: 100%; border-bottom: 2px solid gray;"><br></p>
    <div class="panel panel-default" style="background-color: white !important; border-color: white !important; padding-left: 5px; padding-right: 5px;">
        <div class="panel-heading" style="background-color: white !important; border-color: white !important; padding-bottom:0px;">
            <h4 style="margin-bottom: 0px;"><strong>Этапы:</strong></h4>
        </div>
        <table class="table table-responsive table-hover" id="theStepsTable">
            {include 'work/stepsTable.tpl'}
        </table>
        <p style="width: 100%; border-bottom: 2px solid gray;"><br></p>
        <form action="/workspace/" method="POST" enctype="multipart/form-data" class="addNewStage" id="addNewStage" stpsTblID="theStepsTable" tiketId="{$tiket.tktID}">
            <div class="row">
                <div class="col-md-2">
                    <div class="theCbbxTextarea">
                        <textarea placeholder="Заголовок этапа" class="stepFormArea form-control" rows="5" cols="45" name="stagename" style="width: 100%;" required></textarea>
                        <div class="btn-group">
                            <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
                                <span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
                            </button>
                            <ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
                                {foreach item=titleItemsData key=titleItemskey from=$stepTitleItemsTable}
                                <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$titleItemsData.items}</li>
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="theCbbxTextarea">
                        <textarea placeholder="Описание" class="stepFormArea form-control" rows="5" cols="45" name="stagedescription" style="width: 100%;" required></textarea>
                        <div class="btn-group">
                            <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 16; color: red;">
                                <span class="glyphicon glyphicon-plus"> </span> <span class="glyphicon glyphicon-chevron-down"> </span>
                            </button>
                            <ul class="dropdown-menu txtAreaCbbx" style="width: max-content;">
                                {foreach item=descrItemsData key=descrItemskey from=$stepDescrItemsTable}
                                <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$descrItemsData.items}</li>
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <textarea placeholder="Скрытое описание" class="stepFormArea form-control" rows="5" cols="45" name="hidstagedescription" style="width: 100%;"></textarea>
                </div>
                <div class="col-md-1" style="padding-left: 5px;padding-right: 5px;">
                    <input placeholder="Цена" type="num" class="stepFormArea form-control" name="stagePrice">
                </div>
                <div class="col-md-3" id="stepFileInputContainer">
                    <div class="row" style="padding: 0 !important;">
                        <div class="prev"></div>
                    </div>
                    <input type="file" name="stageimages[]" class="images form-control" id="stepFormFileInput" multiple>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="addNewStage" value="{$tiket.tktID}"  class="btn btn-success" style="float: right; margin-right: 30px;">
                        Добавить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
{include 'work/modals/sendSMSModal.tpl'}
{include 'work/modals/allClientInfoModal.tpl'}
<style>
.settingsWrap{
    margin-left: 0px !important;
    margin-right: 0px !important;
}
</style>