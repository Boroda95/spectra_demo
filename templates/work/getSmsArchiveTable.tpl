<table class="table table-bordered table-condensed table-hover" id="smsArchiveDataTable">
     <thead>
     	<tr align="center">
               <th>#</th>
     		<th>Автор</th>
     		<th>Тело сообщения</th>
     		<th>Номер</th>
     		<th>Статус</th>
     		<th>Дата</th>
     	</tr>
     </thead>
     <tbody>
     {if count($clientSms) != 0}
     {foreach key=clientSmsKey item=clientSmsData from=$clientSms}
          <tr>
               <td>{$clientSmsData['counter']}</td>
               <td>{$clientSmsData.author}</td>
               <td>{$clientSmsData.smsContext}</td>
               <td>{$clientSmsData.clientPhone}</td>
               <td>{$clientSmsData.statusDescription}</td>
               <td>{$clientSmsData.sendDate}</td>
          </tr>
     {/foreach}
     {else}
     <tr align="center">
          <td colspan="6" >Данному клиенту SMS оповещения еще не отправлялись.</td>
     </tr>
     {/if}
     </tbody>
</table>