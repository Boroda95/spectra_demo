<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-12 col-md-12">
		{if $logIn_error != ''}
			<div class="alert alert-warning">
			  {$logIn_error}
			</div>
		{/if}
		<div class="container">
			<form class="form-signin" action="/logIn/" method="POST">
				<h2 class="form-signin-heading">Введите имя пользователя и парольку</h2>
				<label for="inputEmail" class="sr-only">Email</label>
				<input type="text" id="inputEmail" name="email" class="form-control" placeholder="Email" required="" autofocus="">
				<label for="inputPassword" class="sr-only">Password</label>
				<input type="password" id="inputPassword" class="form-control" name="password"  placeholder="Password" required="">
				<button class="btn btn-lg btn-primary btn-block" name="logInSubmit" type="submit">Sign in</button>
			</form>
		</div>
	</div>
</div>