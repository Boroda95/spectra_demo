<div class="container" style="width: 99% !important; padding-left:150px;padding-right:150px; padding-top:10px;">
    <div class="tiketHistoryBlock">
        <h3 style="margin-top: 0px;">История заказов клиента:</h3>
        {$curID = $tiket.tktID}
        {foreach from=$tiketHistory item=historyItem key=historyKey}
        {if $historyItem.statuscode == 'during'}
        <span class="label label-primary" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
        {elseif $historyItem.statuscode == 'completed'}
        <span class="label label-success" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
        {elseif $historyItem.statuscode == 'archive'}
        <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
        {elseif $historyItem.statuscode == 'bolt'}
        <span class="label label-default" class="tHistoryLabel"><a href="/tiketPreview/{$historyItem.actnum}/" target="_blank">#{$historyItem.actnum}</a></span>
        {/if}
        {/foreach}
    </div>
    <table class="table table-responsive table-hover" id="theStepsTable{$tiket.tktID}">
        <thead>
            <tr>
                <th>Название</th>
                <th>Значение</th>
            </tr>
        </thead>
        <tbody>
            {foreach key=cnfClntKey item=cnfClntData from=$clientsConfig.columns}
            <tr>
                {if $cnfClntData.colType == 'edit' || $cnfClntData.colType == 'method'}
                <th>{$cnfClntData.colCaption}</th>
                <td dataID="{$client[0].id}" dataName="{$cnfClntKey}" dataParent="clients" contenteditable="true" class="editableTD">
                    {$client[0].$cnfClntKey}
                </td>
                {elseif $cnfClntData.colType == 'date'}
                <th>{$cnfClntData.colCaption}</th>
                <td><input type="date" class="editableContent {$cnfClntKey}" dataID="{$client[0].id}" dataName="{$cnfClntKey}"
                        dataParent="clients" value="{$client[0].$cnfClntKey}"></td>
                {elseif $cnfClntData.colType == 'select'}
                <th>{$cnfClntData.colCaption}</th>
                <td>
                    <select class="editableContent {$cnfClntKey}" dataID="{$client[0].id}" dataName="{$cnfClntKey}"
                        dataParent="clients">
                        {foreach item=refData key=refKey from=$client.refs.$cnfClntKey}
                        {$dataKey = $cnfClntData.relatedKEYData}
                        {if $refData.id == $client[0].$cnfClntKey}
                        <option selected value="{$refData.id}">{$refData.$dataKey}</option>
                        {else}
                        <option value="{$refData.id}">{$refData.$dataKey}</option>
                        {/if}
                        {/foreach}
                    </select>
                </td>
                {elseif $cnfClntData.colType == 'radioTandem'}
                <th>{$cnfClntData.colCaption}</th>
                <td>
                    {if $client[0].$cnfClntKey == 0}
                    <div class="row">
                        <div class="labeledRadio col-md-6">
                            <label class="label label-warning radioLabel">
                                <input class="inLabel radioStatus" type="radio" checked="true" value="0" name="{$cnfClntKey}"
                                    dataID="{$client[0].id}" dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[0]}
                            </label>
                        </div>
                        <div class="labeledRadio  col-md-6">
                            <label class="label label-success radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="1" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[1]}
                            </label>
                        </div>
                    </div>
                    {else}
                    <div class="row">
                        <div class="labeledRadio col-md-6">
                            <label class="label label-warning radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="0" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[0]}
                            </label>
                        </div>
                        <div class="labeledRadio col-md-6">
                            <label class="label label-success radioLabel">
                                <input class="inLabel radioStatus" type="radio" checked="true" value="1" name="{$cnfClntKey}"
                                    dataID="{$client[0].id}" dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[1]}
                            </label>
                        </div>
                    </div>
                    {/if}
                </td>
                {elseif $cnfClntData.colType == 'radioTrio'}
                <th>{$cnfClntData.colCaption}</th>
                <td>
                    {if $client[0].$cnfClntKey == 0}
                    <div class="row">
                        <div class="labeledRadio col-md-4">
                            <label class="label label-warning radioLabel">
                                <input class="inLabel radioStatus" type="radio" checked="true" value="0" name="{$cnfClntKey}"
                                    dataID="{$client[0].id}" dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[0]}
                            </label>
                        </div>
                        <div class="labeledRadio col-md-4">
                            <label class="label label-primary radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="2" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[2]}
                            </label>
                        </div>
                        <div class="labeledRadio  col-md-4">
                            <label class="label label-success radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="1" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[1]}
                            </label>
                        </div>
                    </div>
                    {elseif $client[0].$cnfClntKey == 1}
                    <div class="row">
                        <div class="labeledRadio col-md-4">
                            <label class="label label-warning radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="0" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[0]}
                            </label>
                        </div>
                        <div class="labeledRadio col-md-4">
                            <label class="label label-primary radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="2" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[2]}
                            </label>
                        </div>
                        <div class="labeledRadio col-md-4">
                            <label class="label label-success radioLabel">
                                <input class="inLabel radioStatus" type="radio" checked="true" value="1" name="{$cnfClntKey}"
                                    dataID="{$client[0].id}" dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[1]}
                            </label>
                        </div>
                    </div>
                    {else}
                    <div class="row">
                        <div class="labeledRadio col-md-4">
                            <label class="label label-warning radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="0" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[0]}
                            </label>
                        </div>
                        <div class="labeledRadio col-md-4">
                            <label class="label label-primary radioLabel">
                                <input class="inLabel radioStatus" type="radio" checked="true" value="2" name="{$cnfClntKey}"
                                    dataID="{$client[0].id}" dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[2]}
                            </label>
                        </div>
                        <div class="labeledRadio col-md-4">
                            <label class="label label-success radioLabel">
                                <input class="inLabel radioStatus" type="radio" value="1" name="{$cnfClntKey}" dataID="{$client[0].id}"
                                    dataName="{$cnfClntKey}" dataParent="clients">
                                {$cnfClntData.tandemItems[1]}
                            </label>
                        </div>
                    </div>
                    {/if}
                </td>
                {/if}
            </tr>
            {/foreach}
        </tbody>
    </table>
    <style>
        td{
            padding: 6px !important;
        }
    </style>