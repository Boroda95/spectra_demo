<form action="/create/" method="POST">
    <div class="row">
        <div class="col-md-2">
            <select data-placeholder="Номер" name="id_clients" id="clientFinder" tabindex="2" required>
            <option value=""></option>
            {foreach key=numsKey item=numsArr from=$clients.data}
            <option value="{$numsArr.id}">{$numsArr.phone}</option>
            {/foreach}
            </select>
        </div>
        <div class="col-md-2">
            <p><input type="text" id="firstname_new" class="form-control " placeholder="{$clntsCnfg.columns.firstname.colCaption}" readonly="readonly"></p>
        </div>
        <div class="col-md-2">
            <p><input type="text" id="surname_new" class="form-control" placeholder="{$clntsCnfg.columns.surname.colCaption}" readonly="readonly"></p>
        </div>
        <div class="col-md-2">
            <p><input type="text" id="middlename_new" class="form-control" placeholder="{$clntsCnfg.columns.middlename.colCaption}" readonly="readonly"></p>
        </div>
        <div class="col-md-2">
            <p><input type="text" id="email_new" class="form-control" placeholder="{$clntsCnfg.columns.email.colCaption}" readonly="readonly"></p>
        </div>
        <div class="col-md-1" style="padding-left: 10px !important; padding-right: 10px !important;">
            <p><input type="text" id="clientStatus_new" class="form-control" placeholder="Статус" readonly="readonly" style="padding-left: 3px;padding-right: 3px;"></p>
        </div>
        <div class="col-md-1" style="padding-left: 10px !important; padding-right: 10px !important;">
            <p><input type="text" id="haveIlliCard_new" class="form-control" placeholder="Ili card" readonly="readonly" style="padding-left: 3px;padding-right: 3px;"></p>
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <div class="row">
        <div class="col-md-2">
            <select data-placeholder="Тип техники" name="id_typesofequipment" class="chosen-select" tabindex="2" required>
            <option value=""></option>  
            {foreach key=eqpmntKey item=eqpmntArr from=$equipments}
            <option value="{$eqpmntArr.id}">{$eqpmntArr.equipmentname}</option>
            {/foreach}
            </select>
        </div>
        <input type="text" name="id_statuses" value="2" class="hidden">
        <input type="text" name="admissiondate" value="" class="hidden">
        <input type="text" name="actnum" value="" class="hidden">
        <div class="col-md-4">
            <input type="text" name="productname" class="form-control" placeholder="{$tktsCnfg.productname.colCaption}" required>
        </div>
        <div class="col-md-3">
            <div class="theCbbxTextarea input-group">
                <input type="text" name="completeness" class="form-control" type="text" maxlength="100" placeholder="{$tktsCnfg.completeness.colCaption}" required>
                <div class="input-group-btn">
                    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 15; color: red; height: 34px">  
                    <span class="glyphicon glyphicon-plus"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                    {foreach item=tktCmpltnItemsData key=tktCmpltnItemskey from=$tktCmpltnItemsTable}
                    <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$tktCmpltnItemsData.items}</li>
                    {/foreach}
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <input type="text" name="productseries" class="form-control" placeholder="{$tktsCnfg.productseries.colCaption}" required>
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <div class="row">
        <div class="col-md-6">
            <input type="text" name="faultdescription" class="form-control" placeholder="{$tktsCnfg.faultdescription.colCaption}" required>
        </div>
        <div class="col-md-6">
            <input type="text" name="hiddenstatus" class="form-control" placeholder="{$tktsCnfg.hiddenstatus.colCaption}">
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <div class="row">
        <div class="col-md-12">
            <div class="theCbbxTextarea input-group">
                <textarea name="exteriorview" class="form-control" rows="3" placeholder="{$tktsCnfg.exteriorview.colCaption}" required></textarea>
                <div class="input-group-btn">
                    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 15; color: red; height: 34px">  
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        {foreach item=tktExtViewItemsData key=tktExtViewItemskey from=$tktExtViewItemsTable}
                        <li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$tktExtViewItemsData.items}</li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <div class="row">
        <div class="col-md-12">
        <div class="theCbbxTextarea input-group">
            <textarea name="tktcomment" rows="3" class="form-control" placeholder="{$tktsCnfg.tktcomment.colCaption}" required></textarea>
            <div class="input-group-btn">
                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" style="font-size: 15; color: red; height: 34px">  
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
                <ul class="dropdown-menu pull-right">
                    {foreach item=tktCmmntItemsData key=tktCmmntItemskey from=$tktCmmntItemsTable}
                    <li class="txtAreaCbbxItem"  style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">{$tktCmmntItemsData.items}</li>
                    {/foreach}
                </ul>
            </div>
        </div>
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <div class="row">
        <div class="col-md-12">
            <div class="theCbbxTextarea input-group">
                <div class="input-group-btn">
                    <button class="btn btn-default btn-sm openMinipriceMenu" type="button" data-toggle="dropdown" style="font-size: 15; color: red; height: 34px">  
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                    <div class="btn-group-vertical miniPriceMenu inactive" role="group">
                        {foreach item=groupTpls key=groupKey from=$miniPriceMenuArr}
                        <div class="btn-group customVerticalMenu" role="group"> 
                            <button id="btnGroupVerticalDrop{$groupKey}" type="button" class="btn btn-default dropdown-toggle groupItem" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                {$groupTpls.groupName} 
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop{$groupKey}">
                                {foreach item=tplData key=tplKey from=$groupTpls.tplData}
                                <li>
                                    <a href="#" class="groupTplItem">{$tplData.tplName}
                                        <span class="tplItemData">{$tplData.tplDescription}</span>
                                    </a>
                                </li>
                                {/foreach}
                            </ul>
                        </div>
                        {/foreach}
                    </div>
                </div>
                <textarea name="miniPrice" id="miniPrice" rows="7" class="form-control" placeholder="Мини-прайс" required></textarea>
            </div>
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <div class="row">
        <div class="col-md-12">
            <input type="text" name="priceRange" id="priceRange" class="form-control" placeholder="{$tktsCnfg.priceRange.colCaption}" required>
        </div>
    </div>
    <p class="rowDelimiter"><br></p>
    <button type="submit" name="{$config.tikets.tableName}" value="add" class="btn btn-success pull-right">Добавить</button>
</form>
<script>
    $(document).ready(function () {
        $.each($(document).find(".customVerticalMenu"), function (i, btGroup) {
            $(btGroup).find('ul.dropdown-menu').css({ 'left': $(btGroup).width(), 'top': '-3px' });
        });
        $(document).on("mouseenter", "button.groupItem", function (e) {
            $.each($(document).find("button.groupItem"), function (i, groupItem) {
                $(this).attr('aria-expanded', false);
                $(this).parent().removeClass('open');
            });
            $(this).attr('aria-expanded', true);
            $(this).parent().addClass('open');
        });
        $(document).on("click", ".groupTplItem", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var thisArea = $(document).find('textarea#miniPrice');
            // thisArea.val($.trim(thisArea.val() + '\n' + $(this).find('span.tplItemData').html() + ';')).focus();
            thisArea.val($.trim($(this).find('span.tplItemData').html().replace(/\|\|\|/g, ";\n"))+";").focus();
            $(document).find('.miniPriceMenu').removeClass('active');
            $(document).find('.miniPriceMenu').addClass('inactive');
        });
        $(document).on("click", ".openMinipriceMenu", function (e) {
            if($(document).find('.miniPriceMenu').hasClass('inactive')){
                $(document).find('.miniPriceMenu').removeClass('inactive');
                $(document).find('.miniPriceMenu').addClass('active');
            }else{
                $(document).find('.miniPriceMenu').removeClass('active');
                $(document).find('.miniPriceMenu').addClass('inactive');
            }
        });
        $(document).on("focusout", ".openMinipriceMenu", function (e) {
            setTimeout(() => {
                $(document).find('.miniPriceMenu').removeClass('active');
                $(document).find('.miniPriceMenu').addClass('inactive');
            }, 200);
        });
    });
</script>
<style>
    .tplItemData {
        display: none;
    }
    .miniPriceMenu{
        position: absolute;
        z-index: 999;
    }
    .miniPriceMenu.active{
        display: block !important;
    }
    .miniPriceMenu.inactive{
        display: none !important;
    }
    .rowDelimiter{
        margin: 0;
    }
</style>