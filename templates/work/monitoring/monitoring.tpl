<div class="container" style="width: 99% !important;">
    <p><br></p>
    <h2>Это есть мониторинг)))</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#stagesMonitoring">Мониторинг этапов</a></li>
        <li><a data-toggle="tab" href="#statusMonitoring">Мониторинг статусов</a></li>
    </ul>
    <div class="tab-content">
        <div id="stagesMonitoring" class="tab-pane fade in active">
            <p><br></p>
            <div class="panel panel-default" style="background-color: white;">
                <div class="panel-heading">
                    <h3 class="panel-title" style="color: red;">Фильтры времени</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 col-sm-12 datepanelmonitoring">
                           <div class="row">
                                <form action="#" id="stageMNTRDateFilterForm">
                                <div class="col-lg-1 col-md-1 col-sm-1" style="width: 137px !important;">
                                    <div class="input-group">
                                        <input type="radio" value="week" checked="checked" name="stageMNTRDateFilter" class="rbutton stageMNTRDateFilter form-control" aria-describedby="dateFltrCheck1">
                                        <span class="rbbut input-group-addon" id="dateFltrCheck1">За неделю</span>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1" style="width: 127px !important;">
                                    <div class="input-group">
                                        <input type="radio" value="month" name="stageMNTRDateFilter" class="rbutton stageMNTRDateFilter form-control" aria-describedby="dateFltrCheck2">
                                        <span class="rbbut input-group-addon" id="dateFltrCheck2">За месяц</span>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1" style="width: 146px !important;">
                                    <div class="input-group">
                                        <input type="radio" value="threeMonth" name="stageMNTRDateFilter" class="rbutton stageMNTRDateFilter form-control" aria-describedby="dateFltrCheck3">
                                        <span class="rbbut input-group-addon" id="dateFltrCheck3">За 3 месяца</span>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3" style="width: 446px !important;">
                                    <div class="row">
                                        <div class="col-md-1"><input type="radio" value="period" name="stageMNTRDateFilter" class="rbutton stageMNTRDateFilter form-control" aria-describedby="dateFltrCheck4" style="width: 35px;"></div>
                                        <div class="input-group col-md-3">
                                            <span class="input-group-addon" id="dateFilterStart" style="background: none !important;">От</span>
                                            <input type="date" min="2017-01-01" max="3000-02-01" name="dateFilterStart" value="{date('Y-m-d')}" 
                                                   id="dateFltrStartInpt" class="stageMNTRDateFilter form-control" aria-describedby="dateFilterStart">
                                            <span class="input-group-addon" id="dateFilterEnd" style="background: none !important;">До</span>
                                            <input type="date" min="2017-01-01" max="3000-02-01" name="dateFilterEnd" value="{date('Y-m-d')}" 
                                                   id="dateFltrEndInpt" class="stageMNTRDateFilter form-control" aria-describedby="dateFilterEnd">
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div> 
                        </div>
                        <div class="col-md-5 managerfiltermonitoring">
                            <div class="row">
                                <form action="#" id="stageMNTRManagersFilterForm">
                                <div class="col-lg-2 col-md-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="*" checked="checked" name="stageMNTRManagerFilter" class="chbox stageMNTRManagerFilterAll form-control">
                                        <span class="bchbox input-group-addon">Все</span>
                                    </div>
                                </div>
                                {foreach from=$masters item=master}
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="{$master.id}" name="stageMNTRManagerFilter" class="chbox stageMNTRManagerFilters form-control">
                                        <span class="bchbox input-group-addon">{$master.synonym}</span>
                                    </div>
                                </div>
                                {/foreach}
                                </form>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            {include 'work/monitoring/stageMNTRDataTable.tpl'}
        </div>

        <div id="statusMonitoring" class="tab-pane fade">
            <p><br></p>
            <div class="panel panel-default" style="background-color: white;">
                <div class="panel-heading">
                    <h3 class="panel-title" style="color: red;">Фильтры времени</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7 datepanelmonitoring">
                           <div class="row">
                                <form action="#" id="statusDateFilterForm" name="statusDateFilterForm">
                                <div class="col-lg-1 col-md-1 col-sm-1" style="width: 137px !important;">
                                    <div class="input-group">
                                        <input type="radio" value="week" checked="checked" name="statusMNTRDateFilter" class="rbutton statusMNTRDateFilter form-control" aria-describedby="dateFltrCheck1">
                                        <span class="rbbut input-group-addon" id="dateFltrCheck1">За неделю</span>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1" style="width: 127px !important;">
                                    <div class="input-group">
                                        <input type="radio" value="month" name="statusMNTRDateFilter" class="rbutton statusMNTRDateFilter form-control" aria-describedby="dateFltrCheck2">
                                        <span class="rbbut input-group-addon" id="dateFltrCheck2">За месяц</span>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1" style="width: 146px !important;">
                                    <div class="input-group">
                                        <input type="radio" value="threeMonth" name="statusMNTRDateFilter" class="rbutton statusMNTRDateFilter form-control" aria-describedby="dateFltrCheck3">
                                        <span class="rbbut input-group-addon" id="dateFltrCheck3">За 3 месяца</span>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3" style="width: 446px !important;">
                                    <div class="row">
                                        <div class="col-md-1"><input type="radio" value="period" name="statusMNTRDateFilter" class="rbutton statusMNTRDateFilter form-control" aria-describedby="dateFltrCheck4" style="width: 35px;"></div>
                                        <div class="input-group col-md-3">
                                            <span class="input-group-addon" id="dateFilterStart" style="background: none !important;">От</span>
                                            <input type="date" min="2017-01-01" max="3000-02-01" name="dateFilterStart" value="{date('Y-m-d')}" 
                                                   id="dateFltrStartInpt" class="statusMNTRDateFilter form-control" aria-describedby="dateFilterStart">
                                            <span class="input-group-addon" id="dateFilterEnd" style="background: none !important;">До</span>
                                            <input type="date" min="2017-01-01" max="3000-02-01" name="dateFilterEnd" value="{date('Y-m-d')}" 
                                                   id="dateFltrEndInpt" class="statusMNTRDateFilter form-control" aria-describedby="dateFilterEnd">
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div> 
                        </div>
                        <div class="col-md-5 managerfiltermonitoring">
                            <div class="row">
                                <form action="#" id="statusMNGRFilterForm" name="statusMNGRFilterForm">
                                <div class="col-lg-2 col-md-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="*" checked="checked" name="managerFilter" class="chbox statusMNTRManagerFilterAll form-control" aria-describedby="managerFltrCheck">
                                        <span class="bchbox input-group-addon">Все</span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="1" name="managerFilter" class="chbox statusMNTRManagerFilters form-control" aria-describedby="managerFltrCheck1">
                                        <span class="bchbox input-group-addon">Артем</span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="3" name="managerFilter" class="chbox statusMNTRManagerFilters form-control" aria-describedby="managerFltrCheck2">
                                        <span class="bchbox input-group-addon">Pavel</span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="input-group"> 
                                        <input type="checkbox" value="5" name="managerFilter" class="chbox statusMNTRManagerFilters form-control" aria-describedby="managerFltrCheck3">
                                        <span class="bchbox input-group-addon">Mark</span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="4" name="managerFilter" class="chbox statusMNTRManagerFilters form-control" aria-describedby="managerFltrCheck4">
                                        <span class="bchbox input-group-addon">Mari</span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="input-group">
                                        <input type="checkbox" value="2" name="managerFilter" class="chbox statusMNTRManagerFilters form-control" aria-describedby="managerFltrCheck5">
                                        <span class="bchbox input-group-addon">Jony</span>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {include 'work/monitoring/statusMNTRDataTable.tpl'}
        </div>
    </div>
</div>



