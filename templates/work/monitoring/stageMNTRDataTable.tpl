<div class="panel panel-default" style="background-color: white;">
    <div class="panel-heading">
        <h3 class="panel-title" style="color: red;">Сами данные:</h3>
    </div>
	<div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12">
            	<table class="table table-bordered table-condensed" id="monitoringStagesTable" style="width: 100% !important; margin: 0px !important;">
		        	<thead>
			            <tr>
			              <th>№ акта</th>
			              <th>Ремонтник</th>
			              <th>Время этапа</th>
			              <th>Наименование техники</th>
			              <th>Описание этапа</th>
			              <th>Статус + состояние</th>
			            </tr>
		        	</thead>
		        </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#monitoringStagesTable").DataTable({
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	        	url: "/monitoring/stagesTable/",
	        	data: function(loool){
            		loool.dateFilterForm = $("#stageMNTRDateFilterForm").serializeArray(),
	        		loool.managersFilterForm = $("#stageMNTRManagersFilterForm").serializeArray()
	        	}
	        },
	        "columns": [
	            { data: 'actnum' },
	            { data: 'synonym' },
	            { data: 'stagedate' },
	            { data: 'productname'},
	            { data: 'stagedescription'},
	            { data: 'state'}
	        ],
	        "paging": true,
	        "pageLength": 100,
	        "language": {
	          "processing": "Подождите...",
	          "search": "Поиск по таблице:",
	          "lengthMenu": "Показать _MENU_ записей",
	          "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
	          "infoEmpty": "Записи с 0 до 0 из 0 записей",
	          "infoFiltered": "(отфильтровано из _MAX_ записей)",
	          "infoPostFix": "",
	          "loadingRecords": "Загрузка записей...",
	          "zeroRecords": "Записи отсутствуют.",
	          "emptyTable": "В таблице отсутствуют данные",
	          "paginate": {
	            "first": "Первая",
	            "previous": "Предыдущая",
	            "next": "Сюда",
	            "last": "Туда"
	          },
	          "aria": {
	            "sortAscending": ": активировать для сортировки столбца по возрастанию",
	            "sortDescending": ": активировать для сортировки столбца по убыванию"
	          }
	        },
	        "aLengthMenu": [ 5, 10, 25, 50, 100, 200, 500],
	        "columnDefs": [ 
			    {
			        "targets" : 0,
			        // "searchable": false,
			        "render" : function (data, type, row, json){
			        	var html = '';
			        	html += '<a target="_blank" href="/tiketPreview/'+row.actnum+'/">'+row.actnum+'</a>';
			          return html;
			        }
			    },
			    {
			    	"targets" : 2,
			    	"render" : function(data,type, row, json){
			    		return data.substr(0, data.length - 3);
			    	}
			    },
			    {
			    	"targets" : 4,
			    	"render" : function(data,type, row, json){
			    		var html = '';
			    		html += '<div contenteditable="true" class="editableTD"'; 
			    			html += ' dataID="'+row.stageID+'" dataName="stagedescription"'; 
			    			html +=  'dataParent="stages" style="word-wrap: break-word;">'+data;
    					html += '</div>';
			    		return html;
			    	}
			    }
		    ],
		    "order": [[ 2, "desc" ]]
	    });

		$('.stageMNTRDateFilter').on('change', function(){
			$('#monitoringStagesTable').DataTable().ajax.reload(); 
		});

		$('.stageMNTRManagerFilterAll, .stageMNTRManagerFilters').on('change', function(){
			if($(this).val() == '*'){
				$('.stageMNTRManagerFilters').prop('checked', false);
			}else{
				$('.stageMNTRManagerFilterAll').prop('checked', false);
			}
			if(!$('.stageMNTRManagerFilters:checkbox:checked').val() && !$('.stageMNTRManagerFilterAll:checkbox:checked').val()){
				$('.stageMNTRManagerFilterAll').prop('checked', true);
			}
		    $('#monitoringStagesTable').DataTable().ajax.reload(); 
		});
	});
</script>