<div class="panel panel-default" style="background-color: white;">
    <div class="panel-heading">
        <h3 class="panel-title" style="color: red;">Сами данные:</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <table class="table table-bordered table-condensed" id="monitoringStatusTable" style="width: 100% !important; margin: 0px !important;">
                    <thead>
                        <tr>
                          <th>№ акта</th>
                          <th>Ремонтник</th>
                          <th>Наименование техники</th>
                          <th>Время смены статуса</th>
                          <th>Был статус</th>
                          <th>Стал статус</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#monitoringStatusTable").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/monitoring/statusTable/",
                data: function(loool){
                    loool.dateFilterForm = $("#statusDateFilterForm").serializeArray(),
                    loool.managersFilterForm = $("#statusMNGRFilterForm").serializeArray()
                }
            },
            "columns": [
                { data: 'actnum' },
                { data: 'synonym' },
                { data: 'productname'},
                { data: 'eventDate' },
                { data: 'oldStatus'},
                { data: 'newStatus'}
            ],
            "paging": true,
            "pageLength": 100,
            "language": {
              "processing": "Подождите...",
              "search": "Поиск по таблице:",
              "lengthMenu": "Показать _MENU_ записей",
              "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
              "infoEmpty": "Записи с 0 до 0 из 0 записей",
              "infoFiltered": "(отфильтровано из _MAX_ записей)",
              "infoPostFix": "",
              "loadingRecords": "Загрузка записей...",
              "zeroRecords": "Записи отсутствуют.",
              "emptyTable": "В таблице отсутствуют данные",
              "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Сюда",
                "last": "Туда"
              },
              "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
              }
            },
            "aLengthMenu": [ 5, 10, 25, 50, 100, 200, 500],
            "columnDefs": [ 
                {
                    "targets" : 0,
                    "render" : function (data, type, row, json){
                        var html = '';
                        html += '<a target="_blank" href="/tiketPreview/'+row.actnum+'/">'+row.actnum+'</a>';
                      return html;
                    }
                },
                {
                    "targets" : 3,
                    "render" : function(data,type, row, json){
                        return data.substr(0, data.length - 3);
                    }
                }
            ],
            "order": [[ 3, "desc" ]]
        });

        $('.statusMNTRDateFilter').on('change', function(){
            $('#monitoringStatusTable').DataTable().ajax.reload(); 
        });

        $('.statusMNTRManagerFilterAll, .statusMNTRManagerFilters').on('change', function(){
            if($(this).val() == '*'){
                $('.statusMNTRManagerFilters').prop('checked', false);
            }else{
                $('.statusMNTRManagerFilterAll').prop('checked', false);
            }
            if(!$('.statusMNTRManagerFilters:checkbox:checked').val() && !$('.statusMNTRManagerFilterAll:checkbox:checked').val()){
                $('.statusMNTRManagerFilterAll').prop('checked', true);
            }
            $('#monitoringStatusTable').DataTable().ajax.reload(); 
        });
    });
</script>