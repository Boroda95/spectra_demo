<html lang="ru" xml:lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{$title}</title>
  <script type='text/javascript' src="/components/jquery/jquery-3.2.1.min.js"></script>
  <script type='text/javascript' src="/components/bootstrap/js/bootstrap.min.js"></script>
  <link href="/components/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div id="loader"><span></span></div>

<table style="width: 656px; margin: 50px;" id="tbl">
  <tr>
    <div class="row" style="padding-top: 25px; max-width: 770px;">
          <div class="col-md-12" id="logo"><img class="logo" style="margin-left: 30px;" src="../img/logo.png" width="230px;">
          <!-- <div class="col-md-6" id="contact" style="font-size: 20px; font-weight: bold; width: 438px; margin-bottom: 20px;"> -->
            <div style="float: right; text-align: right; font-size: 21px; font-weight: bold; margin-top: 18px;" id="contact1">
              <p>ООО «Спектра»</p><p>+7 930 844 44 10</p><p>www.thespectra.ru</p>
            </div></div>
          <!-- </div> -->
        </div>
  </tr>
  <tr>
    <div style="font-size: 11.7px; border-bottom: 2px solid black; border-top: 2px solid black; padding: 0 2px; max-width: 757px; height: 20px;">
      <span>249032, Калужская область, город Обнинск, улица Курчатова, д.41в, оф.15</span> <span style="float: right;">ИНН 4025441043, ОГРН 1144025003792, КПП 402501001</span>
    </div>
  </tr>
  <tr style="text-align: center;"><td colspan="3">Расписка по акту № <span class="actnum"></span></td></tr>
  <tr>
    <td>
      <p><br></p>
    </td>
  </tr>
  <tr style="text-indent: 20px;"><td colspan="3">Я, <span class="surname"> </span> <span class="firstname"> </span>, забрал(a) <span class="equipmentname"></span> <span class="productname"></span> (S\N: <span class="productseries"> </span>) в надлежащем виде, претензий не имею.</td></tr>
  <tr><td><p><br></p></td></tr>
  <tr>
    <td style="text-align: center;">___________________</td>
    <td style="text-align: center;">___________________</td>
    <td style="text-align: center;">___________________</td>
  </tr>
  <tr>
    <td style="text-align: center;">Ф.И.О.</td>
    <td style="text-align: center;">Дата.</td>
    <td style="text-align: center;">Подпись.</td>
  </tr>
</table>

<!-- <iframe src="/price.pdf" width="800px" height="600px" > -->

<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url: '/receipt_print/',
      type: 'POST',
      data: {
        getTiketInfo: 'Get tiket info',
        tiketID: {$thisID}
      },
      beforeSend: function(){
        $('#loader').fadeIn();
      },
      success: function(data){
        res = $.parseJSON(data);

        $.each(res[0],function(key,value){
            $('.'+key).empty().html(value);
        });
      },
      error: function(data){
        res = $.parseJSON(data);
        console.log(res['message']);
      },
      complete: function(){
        $('#loader').delay(500).fadeOut();
      }
    });
  });
</script>
<style>
  @media print{
    #tbl{
      font-family:"Arial";
    }
  }
</style>
