<html lang="ru" xml:lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{$title}</title>
	<script type='text/javascript' src="/components/jquery/jquery-3.2.1.min.js"></script>
	<script type='text/javascript' src="/components/bootstrap/js/bootstrap.min.js"></script>
	<script src="/components/fancybox/fancybox.js"></script>
	<script src="/components/bootstrap/js/offcanvas.js"></script>
	<script src="/components/chosen/chosen.jquery.js"></script>
	<script src="/components/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/components/maskedinput/jquery.maskedinput.min.js"></script>
	<script src="/js/action.js"></script>

	<link href="/components/fancybox/fancybox.css" rel="stylesheet">
	<link href="/components/font_awesome/css/font_awesome.min.css" rel="stylesheet">
	<link href="/components/chosen/chosen.bootstrap.css" rel="stylesheet">
	<link href="/components/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="/CSS/mainTheme.css" rel="stylesheet"/>
	<link href="/components/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
