<div class="container" style="background-color: #f7f7f7; box-shadow: 0px 0px 7px 2px #b3b3b3; min-height: 950px !important;">
<!-- <div class="container waiting" style="width: 540px !important;"><br>
	<p style="font-size: 24px;">Ожидает в Spectra на Курчатова 41в</p><br>
	<p style="font-size: 19px;">Пн-Пт: 10:00-19:00 <br> Сб-Вс: 11:00-18:00</p><br>
	<a href="tel:+79308444410" style="font-size: 19px;" >+7 (930) 844-44-10</a>
	<p style="font-size: 19px;">Для получения техники с ремонта, требуется предоставление акта</p>
</div> -->
<!-- ------------------------------------------------------- -->
<div class="container" style="width: 540px !important; color: #5d5d5d;"><center><h3 class="MainTitle">Готовы к выдаче</h3></center></div>
{if count($arrayOfTikets.completed.tikets) != 0}
{foreach key=key item=fieldArr from=$arrayOfTikets.completed.tikets}
<div class="container" style="padding: 10px 0px 10px 0px; width: 500px !important; background-color: white; overflow-y: hidden; box-shadow: 0px 0px 3px 2px #e8e7e7; border-radius: 2px;">
	<div class="complete tiketCaption" style="cursor: pointer; position: relative;">
		<div class="complete_title">{$fieldArr.productname} <p>{$fieldArr.clientTktState}</p></div>
		<div class="n_act">{$fieldArr.actnum}</div>
	</div>
	<div class="statusBar" statusID="{$fieldArr.id_tiketState}"></div>
	{$currentTktID = $fieldArr.id}
	{if $arrayOfTikets.completed.steps.$currentTktID == 'empty'}
	<div class="steps" style="position: relative; overflow-x: hidden;">
		<div class="step">
			<p class="steps_title" style="color: #5d5d5d;">На текущий момент нет информации о этапах ремонта.</p>	
		</div>
	</div>
	{else}
	{$tktStagesArr = $arrayOfTikets.completed.steps.$currentTktID}
	<div class="container waiting" style="width: 500px !important;"><br>
		<p style="font-size: 24px;">Ожидает в Spectra на Курчатова 41в</p><br>
		<p style="font-size: 19px;">Пн-Пт: 10:00-19:00 <br> Сб-Вс: 11:00-18:00</p><br>
		<a href="tel:+79308444410" style="font-size: 19px;" >+7 (930) 844-44-10</a>
		<p style="font-size: 19px;">Для получения техники с ремонта, требуется предоставление акта</p>
	</div>
	<div class="steps" style="position: relative; overflow-x: hidden;"><br>
		{foreach key=stageKey item=stage from=$tktStagesArr}
		{$dateArr = explode(" ", $stage.stagedate)}
		<div class="step">
			<p class="steps_title">{$stage.stagename}</p>
			<p class="steps_description">{$stage.stagedescription}</p>
			<p class="steps_date">{$dateArr[0]}</p>	
			{if $stage.stageimages != 'empty'}
			{$tktImagesArr = unserialize($stage.stageimages)}
			<div class='list-group gallery'>
				<div class="row">
					{foreach key=imgKey item=imgItem from=$tktImagesArr}
					<div class='col-sm-4 hidden-xs col-md-4 col-lg-4'>
				        <a class="thumbnail fancybox" rel="ligthbox" href="/img/{$imgItem}" style="width: 140; height: 85px;">
				          <img class="img-responsive" src="/img/{$imgItem}" style="height: -webkit-fill-available;">
				        </a>
				    </div>	
					{/foreach}
				</div>
			</div>
			{/if}
		</div>
		{/foreach}
	</div>
	{/if}
</div>	
{/foreach}
{else}
<div class="container" style="width: 540px !important; color: #5d5d5d;"><center><h3>У вас еще нет выполненных заказов.</h3></center></div>

{/if}


	<!-- <div><a href="#" style="font-size: 19px; padding: 0px 20px 0px 20px;">Настроить уведомления</a></div> -->
<!-- ------------------------------------------------------------ -->
<div class="container" style="width: 540px !important; color: #5d5d5d;"><center><h3 class="MainTitle">В процессе ремонта</h3></center></div>
{foreach key=key item=fieldArr from=$arrayOfTikets.during.tikets}
<div class="container" style="padding: 10px 0px 10px 0px; width: 500px !important; background-color: white; overflow-y: hidden; box-shadow: 0px 0px 3px 2px #e8e7e7; border-radius: 2px; margin-bottom: 15px;">
	<div class="complete tiketCaption" style="cursor: pointer; position: relative;">
		<div class="complete_title">{$fieldArr.productname} <p>{$fieldArr.clientTktState}</p></div>
		<div class="n_act">{$fieldArr.actnum}</div>
	</div>
	<div class="statusBar" statusID="{$fieldArr.id_tiketState}"></div>
	{$currentTktID = $fieldArr.id}
	{if $arrayOfTikets.during.steps.$currentTktID == 'empty'}
	<div class="steps" style="position: relative; overflow-x: hidden;">
		<div class="step">
			<p class="steps_title" style="color: #5d5d5d;">На текущий момент нет информации о этапах ремонта.</p>	
		</div>
	</div>
	{else}
	{$tktStagesArr = $arrayOfTikets.during.steps.$currentTktID}
	<div class="steps" style="position: relative; overflow-x: hidden;"><br>
		{foreach key=stageKey item=stage from=$tktStagesArr}
		{$dateArr = explode(" ", $stage.stagedate)}
		<div class="step">
			<p class="steps_title">{$stage.stagename}</p>
			<p class="steps_description">{$stage.stagedescription}</p>
			<p class="steps_date">{$dateArr[0]}</p>	
			{if $stage.stageimages != 'empty'}
			{$tktImagesArr = unserialize($stage.stageimages)}
			<div class='list-group gallery'>
				<div class="row">
					{foreach key=imgKey item=imgItem from=$tktImagesArr}
					<div class='col-sm-4 hidden-xs col-md-4 col-lg-4'>
				        <a class="thumbnail fancybox" rel="ligthbox" href="/img/{$imgItem}" style="width: 140; height: 85px;">
				          <img class="img-responsive" src="/img/{$imgItem}" style="height: -webkit-fill-available;">
				        </a>
				    </div>	
					{/foreach}
				</div>
			</div>
			{/if}
		</div>
		{/foreach}
	</div>
	{/if}
</div>	
{/foreach}
{foreach key=key item=fieldArr from=$arrayOfTikets.bolt.tikets}
<div class="container" style="padding: 10px 0px 10px 0px; width: 500px !important; background-color: white; overflow-y: hidden; box-shadow: 0px 0px 3px 2px #e8e7e7; border-radius: 2px; margin-bottom: 15px;">
	<div class="complete tiketCaption" style="cursor: pointer; position: relative;">
		<div class="complete_title">{$fieldArr.productname} <p>{$fieldArr.clientTktState}</p></div>
		<div class="n_act">{$fieldArr.actnum}</div>
	</div>
	<div class="statusBar" statusID="{$fieldArr.id_tiketState}"></div>
	{$currentTktID = $fieldArr.id}
	{if $arrayOfTikets.bolt.steps.$currentTktID == 'empty'}
	<div class="steps" style="position: relative; overflow-x: hidden;">
		<div class="step">
			<p class="steps_title" style="color: #5d5d5d;">На текущий момент нет информации о этапах ремонта.</p>	
		</div>
	</div>
	{else}
	{$tktStagesArr = $arrayOfTikets.bolt.steps.$currentTktID}
	<div class="steps" style="position: relative; overflow-x: hidden;"><br>
		{foreach key=stageKey item=stage from=$tktStagesArr}
		{$dateArr = explode(" ", $stage.stagedate)}
		<div class="step">
			<p class="steps_title">{$stage.stagename}</p>
			<p class="steps_description">{$stage.stagedescription}</p>
			<p class="steps_date">{$dateArr[0]}</p>	
			{if $stage.stageimages != 'empty'}
			{$tktImagesArr = unserialize($stage.stageimages)}
			<div class='list-group gallery'>
				<div class="row">
					{foreach key=imgKey item=imgItem from=$tktImagesArr}
					<div class='col-sm-4 hidden-xs col-md-4 col-lg-4'>
				        <a class="thumbnail fancybox" rel="ligthbox" href="/img/{$imgItem}" style="width: 140; height: 85px;">
				          <img class="img-responsive" src="/img/{$imgItem}" style="height: -webkit-fill-available;">
				        </a>
				    </div>	
					{/foreach}
				</div>
			</div>
			{/if}
		</div>
		{/foreach}
	</div>
	{/if}
</div>	
{/foreach}
{foreach key=key item=fieldArr from=$arrayOfTikets.waiting.tikets}
<div class="container" style="padding: 10px 0px 10px 0px; width: 500px !important; background-color: white; overflow-y: hidden; box-shadow: 0px 0px 3px 2px #e8e7e7; border-radius: 2px; margin-bottom: 15px;">
    <div class="complete tiketCaption" style="cursor: pointer; position: relative;">
        <div class="complete_title">{$fieldArr.productname} <p>{$fieldArr.clientTktState}</p>
        </div>
        <div class="n_act">{$fieldArr.actnum}</div>
    </div>
    <div class="statusBar" statusID="{$fieldArr.id_tiketState}"></div>
    {$currentTktID = $fieldArr.id}
    {if $arrayOfTikets.waiting.steps.$currentTktID == 'empty'}
    <div class="steps" style="position: relative; overflow-x: hidden;">
        <div class="step">
            <p class="steps_title" style="color: #5d5d5d;">На текущий момент нет информации о этапах ремонта.</p>
        </div>
    </div>
    {else}
    {$tktStagesArr = $arrayOfTikets.waiting.steps.$currentTktID}
    <div class="steps" style="position: relative; overflow-x: hidden;"><br>
        {foreach key=stageKey item=stage from=$tktStagesArr}
        {$dateArr = explode(" ", $stage.stagedate)}
        <div class="step">
            <p class="steps_title">{$stage.stagename}</p>
            <p class="steps_description">{$stage.stagedescription}</p>
            <p class="steps_date">{$dateArr[0]}</p>
            {if $stage.stageimages != 'empty'}
            {$tktImagesArr = unserialize($stage.stageimages)}
            <div class='list-group gallery'>
                <div class="row">
                    {foreach key=imgKey item=imgItem from=$tktImagesArr}
                    <div class='col-sm-4 hidden-xs col-md-4 col-lg-4'>
                        <a class="thumbnail fancybox" rel="ligthbox" href="/img/{$imgItem}" style="width: 140; height: 85px;">
                            <img class="img-responsive" src="/img/{$imgItem}" style="height: -webkit-fill-available;">
                        </a>
                    </div>
                    {/foreach}
                </div>
            </div>
            {/if}
        </div>
        {/foreach}
    </div>
    {/if}
</div>
{/foreach}

<!-- --------------------------------------------------------------------------- -->
<div class="container" style="width: 540px !important; color: #5d5d5d;"><center><h3 class="MainTitle" style="margin-top: 10px !important;">Архив</h3></center></div>  
{if count($arrayOfTikets.archive.tikets) != 0}
{foreach key=key item=fieldArr from=$arrayOfTikets.archive.tikets}
<div class="container" style="width: 500px !important; background-color: white; overflow-y: hidden; box-shadow: 0px 0px 3px 2px #e8e7e7; border-radius: 2px; padding: 0 !important;">
	<div class="complete tiketCaption" style="cursor: pointer; position: relative;">
		<div class="complete_title">{$fieldArr.productname} <p style="height: 10px;" ></p></div>
		<div class="n_act">{$fieldArr.actnum}</div>
	</div>
	<div class="statusBar" statusID="{$fieldArr.id_tiketState}"></div>
	{$currentTktID = $fieldArr.id}
	{if $arrayOfTikets.archive.steps.$currentTktID == 'empty'}
	<div class="steps" style="position: relative; overflow-x: hidden;">
		<div class="step">
			<p class="steps_title" style="color: #5d5d5d;">На текущий момент нет информации о этапах ремонта.</p>	
		</div>
	</div>
	{else}
	{$tktStagesArr = $arrayOfTikets.archive.steps.$currentTktID}
	<div class="steps" style="position: relative; overflow-x: hidden;"><br>
		{foreach key=stageKey item=stage from=$tktStagesArr}
		{$dateArr = explode(" ", $stage.stagedate)}
		<div class="step">
			<p class="steps_title">{$stage.stagename}</p>
			<p class="steps_description">{$stage.stagedescription}</p>
			<p class="steps_date">{$dateArr[0]}</p>	
			{if $stage.stageimages != 'empty'}
			{$tktImagesArr = unserialize($stage.stageimages)}
			<div class='list-group gallery'>
				<div class="row">
					{foreach key=imgKey item=imgItem from=$tktImagesArr}
					<div class='col-sm-4 hidden-xs col-md-4 col-lg-4'>
				        <a class="thumbnail fancybox" rel="ligthbox" href="/img/{$imgItem}" style="width: 140; height: 85px;">
				          <img class="img-responsive" src="/img/{$imgItem}" style="height: -webkit-fill-available;">
				        </a>
				    </div>	
					{/foreach}
				</div>
			</div>
			{/if}
		</div>
		{/foreach}
	</div>
	{/if}
</div>	
{/foreach}
{else}
<div class="container" style="width: 540px !important; color: #5d5d5d;"><center><h3>Архив пуст.</h3></center></div>
{/if}
<p><br></p>
</div>

<script>
function getTableColColor(valData){
	switch(valData) {
	  case '1':
	    return 'background-color: #20c600 !important';
	  break;
	  case '3':
	    return 'background-color: #38761d !important';
	  break;
	  case '4':
	    return 'background-color: #0075c6 !important';
	  break;
	  case '6':
	    return 'background-color: #939393 !important';
	  break;
	  case '8':
	    return 'background-color: #ef0000 !important';
	  break;
	  case '9':
	    return 'background-color: #90c9d6 !important';
	  break;
	  case '10':
	    return 'background-color: #ffb900 !important';
	  break;
	  case '11':
	    return 'background-color: #f7630c !important';
	  break;
	  case '14':
	    return 'background-color: #e2008c !important';
	  break;
	  case '15':
	    return 'background-color: #c22000 !important';
	  break;
    case '16':
      return 'background-color: #f7630c !important';
    break;
    case '17':
      return 'background-color: #ef0000 !important';
    break;
	}
}
	$(document).ready(function(){
		$(".steps").hide();
		$(".tiketCaption").click(function(){
			activeStep = $(this).parent().find('.steps');
			if($(activeStep).css("display") == "none"){
				$(activeStep).show("slow");
			}else{
				$(activeStep).hide(1000);
			}
		});
		jQuery("a.fancybox").fancybox();
		$(".statusBar").each(function(i){
			$(this).attr('style', getTableColColor($(this).attr('statusID')));

		//
		});

	});
</script>

<style>

@media screen and (max-width: 1367px){
	.MainTitle{
		font-size: 22px !important;
	}
	.complete_title{
		font-size: 21px !important;
	}
}
.statusBar{
	height: 14px;
	padding: 0 !important;
}
.complete_title{
	padding: 6px 20px 5px 20px;
	font-size: 24px; 
	color: #0055A6; 
	line-height: 30px; 
	font-family: Roboto, sans-serif; 
	letter-spacing: 1px;
	/*border-bottom: 14px solid #F5B302;*/
}
.complete_title>p{
	color: #737373;
	font-size: 16px;
}
.n_act{
	position: absolute; 
	font-size: 24px; 
	right: 5%;
	top: 34%; 
	color: #737373;
}
.waiting{
	padding: 0px 20px 5px 20px;
	color: #737373;
}
.steps{
	padding: 0px 20px 0px 20px
}
.steps>.step>.steps_title{
	font-size: 20px;
}
.steps>.step>.steps_description{
	font-size: 15px;
	color: #737373;
}
.steps>.step>.steps_date{
	font-size: 17px;
	color: #737373;
}
.during_title{
	padding: 15px 20px 5px 20px;
	font-size: 24px; 
	color: #0055A6; 
	line-height: 30px; 
	font-family: Roboto, sans-serif; 
	letter-spacing: 1px;
	border-bottom: 14px solid;
}
.during_title>p{
	color: #737373;
	font-size: 16px;
}

.archiv_title{
	padding: 15px 20px 5px 20px;
	font-size: 24px; 
	color: #0055A6; 
	line-height: 30px; 
	font-family: Roboto, sans-serif; 
	letter-spacing: 1px;
	border-bottom: 14px solid;
}
.archiv_title>p{
	color: #737373;
	font-size: 16px;
}
/*
/STATUS
*/
</style>