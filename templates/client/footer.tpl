<footer>
    <div class="container hidden-xs" id="footer-container">
      <div class="row" id="footer-row">
        <div class="col-md-3 col-sm-6 footer_block1 ">
          <h6 class="heading7">Ремонт</h6>
          <ul class="footer-ul">
            <li><a href="http://thespectra.ru/repair_notebooks/">Ремонт ноутбуков</a></li>
            <li><a href="http://thespectra.ru/repair_coputers/">Ремонт компьютеров</a></li>
            <li><a href="http://thespectra.ru/repair_coputers#monik/">Ремонт мониторов</a></li>
            <li><a href="http://thespectra.ru/repair_apple/">Ремонт техники Apple</a></li>
            <li><a href="http://thespectra.ru/repair_tablet_smartphone/">Ремонт смартфонов и планшетов</a></li><br>
            <li><a href="http://thespectra.ru/repair_of_home/">Выезд специалиста на дом</a></li>
            <li><a href="http://thespectra.ru/network/">Настройка интернета</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 footer_block2" id="service">
          <h6 class="heading7">Обслуживание организаций</h6>
          <ul class="footer-ul">
            <li><a href="http://thespectra.ru/outsoursing/">Обслуживание компьютеров</a></li>
            <li><a href="http://thespectra.ru/outsoursing#lan_y/">Монтаж Локальных сетей</a></li>
            <li><a href="http://thespectra.ru/outsoursing#video_y/">Установка видеонаблюдения</a></li>
            <li><a href="http://thespectra.ru/outsoursing#print_y/">Обслуживание принтеров и МФУ</a></li>      
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 footer_block3" id="help">
          <h6 class="heading7">Помощь</h6>
            <ul class="footer-ul">
              <li><a href="http://login.thespectra.ru">Личный кабинет</a></li>
              <li><a href="http://thespectra.ru/contact/">Наши контакты</a></li>
              <li><a href="http://thespectra.ru/price/">Прайс лист на услуги</a></li>
              <li><a href="http://thespectra.ru/sale/">Акции</a></li>
              <li><a href="http://thespectra.ru/vacancy/">Вакансии</a></li> 
              <li><a href="http://thespectra.ru/blog/">Наш блог</a></li> 
              <li><a href="http://thespectra.ru/thanks/">Оставить отзыв</a></li>  
              <li><a href="http://thespectra.ru/partner/">Наши партнеры</a></li>  
            </ul>
        </div>
        <div class="col-md-3 col-sm-6 footer_block4">
          <img class="logo_footer" src="/img/bkgw.png">
          <br>
          <a href="http://vk.com/thespectra" target="_blank"><div class="vk" id="contactS"></div></a>
          <a href="http://www.odnoklassniki.ru/group/54218802003968/" target="_blank" ><div class="odnoklassniki" id="contactS"></div></a>
          <a href="http://www.facebook.com/groups/spectra.service" target="_blank" ><div class="facebook" id="contactS"></div></a>
          <a href="http://instagram.com/thespectra" target="_blank" ><div class="instagram" id="contactS"></div></a>
          <a href="http://obninskbiz.ru/view.php?REQ=view&id=4082" target="_blank" ><div class="biz" id="contactS"></div></a>
        </div>
      </div>
    </div>

      <div class="container visible-xs">
        <div class="panel-group" id="faqAccordion">
          <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle question-toggle collapsed" id="f_xs_foot_rem" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
              <h4 class="panel-title"><p href="#" class="ing">Ремонт</p></h4>
            </div>
            <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                    <ul class="footer-ul">
                      <li><a href="http://thespectra.ru/repair_notebooks/" class="a_footer_xs">Ремонт ноутбуков</a></li>
                      <li><a href="http://thespectra.ru/repair_coputers/" class="a_footer_xs">Ремонт компьютеров</a></li>
                      <li><a href="http://thespectra.ru/repair_coputers/#monik/" class="a_footer_xs">Ремонт мониторов</a></li>
                      <li><a href="http://thespectra.ru/repair_apple/" class="a_footer_xs">Ремонт техники Apple</a></li>
                      <li><a href="http://thespectra.ru/repair_tablet_smartphone/" class="a_footer_xs">Ремонт смартфонов и планшетов</a></li>
                      <li><a href="http://thespectra.ru/repair_of_home/" class="a_footer_xs">Выезд специалиста на дом</a></li>
                      <li><a href="http://thespectra.ru/network/" class="a_footer_xs">Настройка интернета</a></li>
                    </ul>
                </div>
            </div>
          </div>
          <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                 <h4 class="panel-title">
                    <p href="http://thespectra.ru/outsoursing/" class="ing">Обслуживание организаций</p>
              </h4>
            </div>
            <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <ul class="footer-ul">
                    <li><a href="#" class="a_footer_xs">Обслуживание компьютеров</a></li>
                    <li><a href="http://thespectra.ru/outsoursing#lan_y/" class="a_footer_xs">Монтаж Локальных сетей</a></li>
                    <li><a href="http://thespectra.ru/outsoursing#video_y/" class="a_footer_xs">Установка видеонаблюдения</a></li>
                    <li><a href="http://thespectra.ru/outsoursing#print_y/" class="a_footer_xs">Обслуживание принтеров и МФУ</a></li>       
                  </ul>
                </div>
            </div>
          </div>
          <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
              <h4 class="panel-title"><p href="#" class="ing">Помощь</p></h4>
            </div>
            <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <ul class="footer-ul">
                    <li><a href="http://login.thespectra.ru" class="a_footer_xs">Личный кабинет</a></li>
                    <li><a href="http://thespectra.ru/contact" class="a_footer_xs">Наши контакты</a></li>
                    <li><a href="http://thespectra.ru/price" class="a_footer_xs">Прайс лист на услуги</a></li>
                    <li><a href="http://thespectra.ru/sale" class="a_footer_xs">Акции</a></li>
                    <li><a href="http://thespectra.ru/vacancy" class="a_footer_xs">Вакансии</a></li> 
                    <li><a href="http://thespectra.ru/blog" class="a_footer_xs">Наш блог</a></li>
                    <li><a href="http://thespectra.ru/thanks" class="a_footer_xs">Оставить отзыв</a></li>   
                    <li><a href="http://thespectra.ru/partner">Наши партнеры</a></li>        
                  </ul>
                </div>
            </div>
          </div>    
        </div>
        <!--/panel-group-->
        <div class="container footer_block_xs">
          <center><img class="logo_footer_xs" src="/img/bkgw.png"></center>
          <br>
          <div class="footer_blok_xs2">
            <a href="http://vk.com/thespectra" target="_blank"><div class="vk" id="contactS"></div></a>
            <a href="http://www.odnoklassniki.ru/group/54218802003968/" target="_blank" ><div class="odnoklassniki" id="contactS"></div></a>
            <a href="http://www.facebook.com/groups/spectra.service" target="_blank" ><div class="facebook" id="contactS"></div></a>
            <a href="http://instagram.com/thespectra" target="_blank" ><div class="instagram" id="contactS"></div></a>
            <noindex><a href="http://obninskbiz.ru/view.php?REQ=view&id=4082" target="_blank" ><div class="biz" id="contactS"></div></a></noindex>
          </div>
        </div>
      </div>
      <div class="container container_btn_group" id="line_foot">
        <div class="col-md-10"><p id="ooo">© 2013-2018 ООО "Спектра"</p></div>
        <!-- <div class="col-md-2"><img src="/img/soc/biz-hover.png" id="biz" alt=""></div> -->
      </div>
      
  </footer>