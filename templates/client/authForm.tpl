<div class="row row-offcanvas row-offcanvas-right" style="margin-right: 1px !important;">
	<div class="col-md-12">
		{if $logIn_error != ''}
			<div class="alert alert-warning">
			  {$logIn_error}
			</div>
		{/if}
		<div class="container" >
			<form class="form-signin" action="/authorization/" method="POST">
				<h2 class="form-signin-heading">Введите Ваш номер телефона без 8 и пароль</h2>
				<label for="inputEmail" class="sr-only">Телефон</label>
				<input type="phone" id="inputEmail" name="phone" class="form-control" placeholder="Вводите номер без цифры 8 вначале" size="10" minlength="10" maxlength="10" required="" autofocus="">
				<label for="inputPassword" class="sr-only">Пароль</label>
				<input type="password" id="inputPassword" class="form-control" name="password"  placeholder="Пароль" required="">
				<button class="btn btn-lg btn-primary btn-block" name="logInSubmit" type="submit">Войти</button>
				<br>
				<p>Регистрация в личном кабине и выдача пароля производится только при обращении в наш сервис</p>
			</form>
		</div>
	</div>
</div>

<div style="width: 100px; height: 100px;">
	
</div>