<!-- <style>
	@media screen and (max-width: 1145px){
		
	}
</style>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="col-md-5 col-sm-4"><a class="navbar-brand" href="#">Кабинет клиента</a></div>
	<div class="col-md-4 col-sm-4">
		<a class="navbar-brand" href="http://thespectra.ru/">Главная</a>
		<a class="navbar-brand" href="http://thespectra.ru/price/">Прайс</a>
		<a class="navbar-brand" href="http://thespectra.ru/contact/">Контакты</a>
	</div>
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
	</button>
	<div class="navbar-collapse collapse col-md-3" id="theNavbar">
		{if isset($loginedClient) && $loginedClient != ''}
		<ul class="nav navbar-nav navbar-right" style="margin-right: 50px;">
			<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$loginedClient.fName} {$loginedClient.sName}<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="/authorization/exit/">Выйти</a></li>
              </ul>
            </li>
        </ul>
		{/if}
	</div>
</nav> -->
{* 
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter20195464 = new Ya.Metrika2({ id:20195464, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/20195464" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter --> *}

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container" style="font-size: 13px !important;">
    <ul class="nav navbar-nav">
      <div class="row">

        <div class="col-md-2 col-sm-2 visible-md-block visible-sm-block visible-lg-block toun1" style="width: 200px; font-size: 14px !important; padding-top: 11px;">
          <a  class="adress" href="http://maps.yandex.ru/?ol=biz&oid=1262079565"><i class="fa fa-map-marker" aria-hidden="true"></i> Обнинск</a>
        </div>
        <div class="col-md-2 col-sm-2 visible-md-block visible-sm-block visible-lg-block adress1" style="width: 200px; padding-top: 12px;">
          <a  class="adress" href="http://maps.yandex.ru/?ol=biz&oid=1262079565"><i class="fa fa-map" aria-hidden="true"></i> ул. Курчатова 41В</a>
        </div>
        <div class="col-md-2 col-sm-2 visible-md-block visible-sm-block visible-lg-block email1" style="width: 210px; padding-top: 12px;">
          <a  class="email" href="mailto:info@thespectra.ru" ><i class="fa fa-envelope-o" aria-hidden="true"></i> info@thespectra.ru</a></li>
        </div>
        <div class="col-md-3 col-sm-3 visible-md-block visible-sm-block visible-lg-block phone1" style="width: 200px; padding-top: 12px;">
          <a  class="phone" href="tel:+79308444410"><i class="fa fa-phone" aria-hidden="true"></i>  +7 (930) 844-44-10</a></li>
        </div>
        <div class="col-md-2 col-sm-2 visible-md-block visible-sm-block visible-lg-block client1" style="padding-top: 12px; color: white;">
	      	{if isset($loginedClient) && $loginedClient != ''}
			<button type="button" class=" dropdown-toggle" data-toggle="dropdown" id="toun"><span>{$loginedClient.fName} {$loginedClient.sName} </span><i class="fa fa-sign-out" aria-hidden="true"></i></button>
			<ul class="dropdown-menu" role="menu">
			<li><a href="/authorization/exit/">Выйти</a></li>
			</ul>
			{/if}
        </div>

      </div>
    </ul>
  </div><!-- /.container -->

    <!-- xs -->
  <div class="container_nav_xs">
    <div class="col-xs-3 nav_ico_xs1 visible-xs " id="nav_ico_xs"><a class="adress" href="http://maps.yandex.ru/?ol=biz&oid=1262079565"><i class="fa fa-map-marker" aria-hidden="true"></i></a></div>
    <div class="col-xs-3 nav_ico_xs2 visible-xs " id="nav_ico_xs"><a class="email" href="mailto:info@thespectra.ru" style="float: right;"><i class="fa fa-envelope-o" aria-hidden="true"></i> </a></div>
    <div class="col-xs-3 nav_ico_xs3 visible-xs " id="nav_ico_xs"><a class="phone" href="tel:+79308444410"><i class="fa fa-phone" aria-hidden="true"></i></a></div>
    <div class="col-xs-3 nav_ico_xs3 visible-xs " id="nav_ico_xs"><a class="phone" href="/authorization/exit/" style="float: right;"><i class="fa fa-sign-out" aria-hidden="true"></i></a></div>
  </div> 

</div><!-- /.navbar -->

<div class="row" style="margin-right: 1px !important;">
  <div class="col-md-12" id="logo_mid">
    <center><a href="http://thespectra.ru"><img class="logo" src="/img/logo.png"></a></center>
  </div>
</div>

<div class="container-fluid visible-lg-block visible-md-block visible-sm-block" id="div-toolbar-container">
  <div class="container container_btn_group">
    <div class="btn-group btn-group-lg" id="btn_group_id">
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/repair_coputers/" style="border-left: 0 !important; border-bottom-left-radius: 0 !important;" >
          <img src="/img/button/pc.png" class="btn-blok-img"><br>Ремонт <br>компьютеров
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/repair_notebooks/">
          <img src="/img/button/nb.png" class="btn-blok-img"><br>Ремонт <br>ноутбуков
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/repair_tablet_smartphone/">
          <img src="/img/button/tb.png" class="btn-blok-img"><br>Ремонт <br>планшетов
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/repair_apple/">
          <img src="/img/button/apple.png" class="btn-blok-img"><br>Ремонт <br>Apple
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/build_new_pc/">
          <img src="/img/button/bpc.png" class="btn-blok-img"><br>Сборка <br>компьютеров
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/network/">
          <img src="/img/button/net.png" class="btn-blok-img"><br>Настройка <br>интернета
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/outsoursing/">
          <img src="/img/button/corp.png" class="btn-blok-img"><br>Обслуживание <br>Организаций
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/repair_of_home/">
          <img src="/img/button/home.png" class="btn-blok-img"><br>Выезд <br>на дом
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/price/">
          <img src="/img/button/price.png" class="btn-blok-img"><br>Прайс лист <br>на услуги
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://store.thespectra.ru">
          <img src="/img/button/store.png" class="btn-blok-img"><br>Магазин
        </a>
        <a type="button" class="btn btn-default" id="btn-blok" href="http://thespectra.ru/contact/" style="border-right: 0 !important; border-bottom-right-radius: 0 !important;">
          <img src="/img/button/info.png" class="btn-blok-img"><br>Информация
        </a>
    </div>
  </div>
</div>
