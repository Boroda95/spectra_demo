function fixUserByPhone(id){
	$.ajax({
        url: '/create/',
        type: 'POST',
        data: {
            userDataFinder: 'Get user info',
            id: id
        },
        beforeSend: function(){
            $('#loader').fadeIn();
        },
        success: function(data){
            res = $.parseJSON(data);
            $.each(res,function(key,value){
            if(key == 'clientStatus'){
                $('#' + key + '_new').attr('value', (value == "0") ? "Не проверен" : (value == "1") ? "Молодец" : "Нe молодец");
                $('#' + key + '_new').attr("style", (value == "0") ? "background-color:#fff !important; padding-left: 5px;padding-right: 5px;" : (value == "1") ? "background-color:rgb(13,255,0) !important; padding-left: 5px;padding-right: 5px;" : "background-color:rgb(216, 0, 40) !important; color: #fff; padding-left: 5px;padding-right: 5px;");
            }else if(key == 'creditCardNum'){
                $('#haveIlliCard_new').attr('value', (value == "") ? "Нет карты" : value);
                $('#haveIlliCard_new').attr("style", (value == "") ? "background-color:rgb(216, 0, 40) !important; color: #fff; padding-left: 5px;padding-right: 5px;" : "background-color:rgb(13,255,0) !important;padding-left: 5px;padding-right: 5px;");
            }else if(key!='id'){
                $('#'+key+'_new').attr('value',value);
            }else{
                $("select#clientFinder").val(value);
                $("#clientFinder").trigger("liszt:updated");
                $("#clientFinder").trigger("chosen:updated");
                $("select#clientFinder").val(value);
            }
            });
        },
        error: function(data){
            res = $.parseJSON(data);
        },
        complete: function(){
            $('#loader').delay(500).fadeOut();
        }
	});
}

function dateTimeToTiketDate(theDate){
	var timestamp = new Date(Date.parse(theDate));
	var date = new Date();
	var restDate = '';
	var monthNames = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
	                  "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"
	];

	date.setTime(timestamp);
	restDate += date.getDay();
	restDate += ' '+monthNames[date.getMonth()];
	restDate += ' '+date.getFullYear()+'г.';
	return restDate;
}

function getTableColColor(valData){
	switch(valData) {
    case '1':
	    return 'background-color: #20c600 !important';
	case '3':
	    return 'background-color: #38761d !important';
	case '4':
	    return 'background-color: #0075c6 !important';
	case '6':
	    return 'background-color: #939393 !important';
	case '8':
	    return 'background-color: #ef0000 !important';
	case '9':
	    return 'background-color: #90c9d6 !important';
	case '10':
	    return 'background-color: #ffb900 !important';
	case '11':
	    return 'background-color: #f7630c !important';
	case '14':
	    return 'background-color: #e2008c !important';
	case '15':
	    return 'background-color: #c22000 !important';
    case '16':
        return 'background-color: #f7630c !important';
    case '17':
        return 'background-color: #ef0000 !important';
    case '18':
        return 'background-color: #20c600 !important';
    case '19':
        return 'background-color: #38761d !important';
	}
}
function getClientTableRowColor(valData){
    switch(valData) {
    case '0':
        return 'background-color:rgb(255, 255, 255) !important';
    case '1':
        return 'background-color:rgb(13,255,0) !important';
    case '2':
        return 'background-color:rgb(216, 0, 40) !important';
  }
}

$(document).ready(function(){
    $(document).on("submit", ".addNewStage", function (e){
        e.preventDefault();
        e.stopPropagation();
        var data = new FormData();
        var thisForm = $(this);
        var formData = thisForm.serializeArray();
        var formButton = $(document).find(thisForm.find("button[type='submit']")[0]);
        formData.push({'name': formButton.attr('name'), 'value': formButton.val()});
        $.each(formData, function(i, datas){
            data.append(datas.name, datas.value);
        });

        var formFileInput = $($(this).find("input[type='file']")[0].files);
        var ins = formFileInput.length;
        for (var x = 0; x < ins; x++) {
            data.append("files[]", formFileInput[x]);
        }
        
        $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data        : data,
            cache       : false,
            dataType    : 'html',
            processData : false,
            contentType : false,
            beforeSend: function(){
                $('#loader').fadeIn();
            },
            success: function(data){
                    if(data != 'Не получилосъ'){
                    $("#"+$(document).find(thisForm).attr('stpsTblID')).empty();
                    $("#"+$(document).find(thisForm).attr('stpsTblID')).append(data);
                    $(document).find('.prev').empty();
                    $(document).find(thisForm)[0].reset();
                }else{
                    console.log('Хз, что могло пойти не так. Но что-то пошло не так. ');
                }
            },
            error: function(data){
                    console.log(data);
            },
            complete: function(){
                    $('#loader').delay(500).fadeOut();
            }
        });
        return false;
    });

    $(".addNewTask").submit(function(e){
        e.preventDefault();
        var data = new FormData();
        var thisForm = $(this);
        var formData = thisForm.serializeArray();
        var formButton = $(thisForm.find("button[type='submit']")[0]);
        formData.push({'name': formButton.attr('name'), 'value': formButton.val()});
        $.each(formData, function(i, datas){
            data.append(datas.name, datas.value);
        });
        $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data        : data,
            cache       : false,
            dataType    : 'html',
            processData : false,
            contentType : false,
            beforeSend: function(){
                // $('#loader').fadeIn();
            },
            success: function(data){
                if(data != 'Не получилосъ'){
                    $("#theTaskListTable").remove();
                    $("#theTaskListTableContainer").append(data);
                    
                    $(thisForm)[0].reset();
                }else{
                    console.log('Хз, что могло пойти не так. Но что-то пошло не так. ');
                }
            },
            error: function(data){
                console.log(data);
            }
        });
        return false;
    });

    $(".addNewTaskToTasks").submit(function (e) {
        e.preventDefault();
        var data = new FormData();
        var thisForm = $(this);
        var formData = thisForm.serializeArray();
        var formButton = $(thisForm.find("button[type='submit']")[0]);
        formData.push({ 'name': formButton.attr('name'), 'value': formButton.val() });
        $.each(formData, function (i, datas) {
            data.append(datas.name, datas.value);
        });
        $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'html',
            processData: false,
            contentType: false,
            success: function (data) {
                if (data != 'Не получилосъ') {
                    $("#theNewTaskListTable").remove();
                    $("#theNewTasksListTableContainer").append(data);
                    $(thisForm)[0].reset();
                } else {
                    console.log('Хз, что могло пойти не так. Но что-то пошло не так. ');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
        return false;
    });

    $(document).on("click", ".txtAreaCbbxItem", function(e){
        var block = $(this).parent().parent().parent();
        if($(block).find('textarea')){
            var curTxt = $(block).find('textarea').val();
            $(block).find('textarea').val(curTxt + $(this).html());
            $(block).find('textarea').focus();
        }
        if($(block).find('input')){
            var curTxt = $(block).find('input').val();
            $(block).find('input').val(curTxt + $(this).html());
            $(block).find('input').focus();
        }
    });

    $('.extTxtAreaCbbxItem').click(function(){
        var block = $(this).parent().parent().parent();
        if($(block).find('textarea')){
            var curTxt = $(block).find('textarea').val();
            temp = curTxt +'\n'+ $(this).html()+';'
            $(block).find('textarea').val($.trim(temp));
            $(block).find('textarea').focus();
        }
    });

    $(".id_tiketState").each(function(i){
        console.log($(this).get(0).tagName);
        if($(this).get(0).tagName == 'TD' || $(this).get(0).tagName == 'DIV'){
            $(this).parent().attr('style', getTableColColor($(this).attr('value')));
        }else{
            $(this).parent().parent().attr('style', getTableColColor($(this).val()));
        }

        //
    });

    $(".clientStatus_active").each(function(i){
        $(this).parent().parent().attr('style', getClientTableRowColor($(this).val()));

        //
    });
    
    $('.getTiketPrepare').click(function(){
        fixUserByPhone($(this).attr('activedata').trim());

        //
    });

    $('#clientFinder').change(function(){
        fixUserByPhone($('#clientFinder option:selected').val());

        //
    });

    $(document).on("change", ".images", function(e){
        var thisInput = $(this);
        files = e.target.files;
        $(thisInput.parent().find('div.row').find('div.prev')).empty();
        $.each(files, function(i, file){
            $(thisInput.parent().find('div.row').find('div.prev')).append(''+
                '<div class="col-md-4">'+
                    '<img class="img-responsive" src="'+URL.createObjectURL(file)+'" style="width: 70px; height: 60px;">'+
                '</div>'+
            '');
        });
    });

    $(document).on("click", ".paginate_button", function(e){
        $(".id_tiketState").each(function(i){
            $(this).parent().parent().attr('style', getTableColColor($(this).val()));
        });
        $(".clientStatus_active").each(function(i){
            $(this).parent().parent().attr('style', getClientTableRowColor($(this).val()));
        });
    });

    $(document).on("click", ".allClientInfo", function(e){
        var clientID = $(this).attr('activedata');
        $('#loader').fadeIn();
        $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data: {
                getClientContentById: 'Get that',
                clientID: clientID
            },
            dataType    : 'html',
            success: function(data){
                if(data != 'Не получилосъ'){
                $(".clientDataContainer").empty().append(data);
                }else{
                $(".clientDataContainer").empty().append('Хз, что могло пойти не так. Но что-то пошло не так.');
                }
            },
            error: function(data){
                console.log(data);
            }
        });
        $("#allClientInfoModal").modal('show'); // открываем модальное окно
        $("#printLink").attr('href','/printClientData/'+clientID+'/');
        $('#loader').delay(2000).fadeOut();
    });

    $(document).on("click", ".delItemFromTable", function(e){
        if(!confirm('Вы действительно хотите удалить данную запись?')){
        return false;
        }
        var editableElement = $(this);
        $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data: {
                expressDeleter: 'Delete that', 
                id: $(this).attr('dataID'),
                parent: $(this).attr('dataParent')
            },
            beforeSend: function(){
                $('#loader').fadeIn();
            },
            success: function(data){
                res = $.parseJSON(data);
            $(editableElement).parent().parent().remove();
            },
            error: function(data){
                res = $.parseJSON(data);
            },
            complete: function(){
                $('#loader').delay(500).fadeOut();
            }
        });
    });
    
    $(document).on("focus", ".editableContent, .editableTD", function(e){
        if($(this).hasClass('editableContent')){
        oldVal = $(this).val();
        }
        if($(this).hasClass('editableTD')){
        oldVal = $(this).html().trim();
        }
    });

    $(document).on("blur", ".editableContent, .editableTD", function(e){
        var editableElement = $(this);
        if($(this).hasClass('editableContent')){
        newVal = $(this).val();
        }
        if($(this).hasClass('editableTD')){
            newVal = $(this).html().trim();
        }
        if ($(this).hasClass('fuckingMiniPrice')){
            console.log($(this).html().trim());
            newVal = $(this).html().trim();
            newVal = newVal.replace(/<br>/g, "|||");
            console.log(newVal);
        }
        if(oldVal != newVal){
            if($(editableElement).hasClass('id_statuses') && !confirm('Вы действительно хотите сменить этот статус?')){
                return false;
        }
        $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data: {
            expressEditor: 'Edit that',
            new_val: newVal, 
            old_val: oldVal, 
            id: $(this).attr('dataID'),
            name: $(this).attr('dataName'),
            parent: $(this).attr('dataParent')
            },
            beforeSend: function(){
            $('#loader').fadeIn();
            },
            success: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
            if($(editableElement).hasClass('id_tiketState')){
                $(editableElement).parent().parent().attr('style', getTableColColor($(editableElement).val()));
            }
            if($(editableElement).hasClass('clientStatus_active')){
                $(editableElement).parent().parent().attr('style', getClientTableRowColor($(editableElement).val()));
            }
            },
            error: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
            },
            complete: function(){
            $('#loader').delay(500).fadeOut();
            }
        });
        }
    });

    $(document).on("change", ".radioStatus", function(e){
        $.ajax({
        url: '/contentEditor/',
        type: 'POST',
        data: {
            expressEditor: 'Edit that',
            new_val: $(this).val(), 
            id: $(this).attr('dataID'),
            name: $(this).attr('dataName'),
            parent: $(this).attr('dataParent')
        },
        beforeSend: function(){
            $('#loader').fadeIn();
        },
        success: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
        },
        error: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
        },
        complete: function(){
            $('#loader').delay(500).fadeOut();
        }
        });
    });

    $(document).on("click", ".delThisImg", function(e){
        var container = $(this).parent().parent();
        $(this).parent().remove();
        var arrOfImg = [];

        $.each($(container).children(), function(i, item){
        var src = $(item).find('img').attr('src').split('/');
        arrOfImg.push(src[src.length-1]);
        });
        
        $.ajax({
        url: '/workspace/',
        type: 'POST',
        data: {
            ImgExpressEditor: 'Edit that',
            new_val: arrOfImg, 
            id: $(container).attr('dataID'),
            name: $(container).attr('dataName'),
            parent: $(container).attr('dataParent')
        },
        beforeSend: function(){
            $('#loader').fadeIn();
        },
        success: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
        },
        error: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
        },
        complete: function(){
            $('#loader').delay(500).fadeOut();
        }
        });
    });

    $(document).on("click", ".sendSMSModal", function(e){
        var clientID = $(this).attr('activedata');
        var thePhone = $.trim($(this).parent().parent().find('td#phone').html());
        if(!thePhone){
        thePhone = $.trim($('#clientLogin').html());
        }
        $('#smsFormClntPhone').val(thePhone);
        $('#smsFormClntID').val(clientID);
        if(thePhone.length == 10 || thePhone[0] == 9){
        $('#loader').fadeIn();
            $.ajax({
            url: '/contentEditor/',
            type: 'POST',
            data: {
                getSmsArchive: 'true',
                clientID: clientID
            },
            success: function(data){
                res = $.parseJSON(data);

                $('#smsArchiveTable').empty().html(res['archive']);

                $('#sedSMSTemplates').empty().prepend(''+
                '<li class="txtAreaCbbxItem" style="cursor: pointer; border-bottom: 1px solid black; padding: 2px;">'+
                    'login.thespectra.ru \n'+
                    'Логин: '+res['login'] + '\n'+
                    'Пароль: '+res['passw'] + '\n'+
                '</li>');
                $("#sendSMSModal").modal('show');
            },
            error: function(data){
                console.log(data);
            },
            complete: function(){
                $('#loader').delay(100).fadeOut();
            }
            });
        }else{
        alert("Номер '"+thePhone+"'' не соответствует обговоренному формату. Приведите номер телефона к формату: 9004003020. 10 цифр. Первая цифра 9.");
        }
    });

    $(document).on("click", ".tiketDetailsModal", function(e){
        var tiketID = $(this).attr("dataID");
        $.ajax({
        url: '/contentEditor/',
        type: 'POST',
        data: {
            "action": "getTiketDetailModal",
            "tiketID": tiketID
        }, 
        beforeSend: function () {
            $('#loader').fadeIn();
        },
        success: function(data){
            $(document).find("#tiketPeviewModal").remove();
            $('body').append(data);
            var tState = $(document).find("div.col-md-2 select.id_tiketState");
            $(tState).parent().parent().attr('style', getTableColColor($(tState)[0].value));
            $('#loader').delay(100).fadeOut();
            $(document).find("#tiketPeviewModal").modal('show');
            $(document).find("#tiketPeviewModal").attr("style","padding-right: 5px !important;padding-left: 5px !important;");
        },
        error: function(data){
            console.log(data);
            $('#loader').delay(100).fadeOut();
            alert("Извините произошла ошибка! Воспользуйтесь просмотрщиком заказов в отдельном окне и сообщите, пожалуйста, о проблеме  разработчикам.");
        }
        });
    });
    $(document).on("keypress", ".tiket_priority", function(e){
        e = e || event;
        if (e.ctrlKey || e.altKey || e.metaKey) return false;
        var chr = String.fromCharCode(e.keyCode || e.charCode);
        if (chr == null) return false;
        if (chr < '0' || chr > '9') {
            return false;
        }
        if ($(this).html() + chr > 9999){
            return false;
        }
    });
    $("#sendSMSForm").submit(function(e){
        e.preventDefault();
        var thisForm = $(this);
        var formData = thisForm.serializeArray();
        var smsText = formData[0]['value'];
        var clientID = formData[1]['value'];
        var clientPhone = formData[2]['value'];
        $.ajax({
        url: '/contentEditor/',
        type: 'POST',
        data: {
            sendSms: 'true',
            clientID: clientID,
            smsText: smsText,
            clientPhone: clientPhone
        },
        success: function(data){
            res = $.parseJSON(data);
        },
        error: function(data){
            res = $.parseJSON(data);
            console.log(res['message']);
        }
        });
        $("#sendSMSModal").modal('hide');
    });
});