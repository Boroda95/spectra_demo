<?php
	/**
	 * [cодержит основыные(фундаментальные) функции и процедуры в проекте]
	 */
	class core extends DomDocument{

		public function __construct(){
			if(!isset($this->con)){
				$this->con = mysqli_connect(HOST,USER,PASSWORD,DB);
				mysqli_set_charset($this->con, "utf8");
				mysqli_query($this->con,"SET NAMES utf8");
				mysqli_query($this->con,"SET lc_time_names = 'ru_RU'");
			}elseif (!$this->con) {
				$this->logMe("Ошибка подключения к БД. Host - ".HOST." | User - ".USER." | Psw - ".PASSWORD." | DB - ".DB, "error");
			}
			require_once SmartyClass.'Smarty.class.php';
			$this->smarty = new Smarty();
			
			$this->smarty->caching = FASE;
			$this->smarty->force_compile = TRUE;
			$this->smarty->compile_check = TRUE;

			$this->smarty->template_dir = SmartyTemplatesDir;
			$this->smarty->compile_dir = SmartyCompileDir;
			$this->smarty->config_dir = SmartyConfigDir;
			$this->smarty->cache_dir = SmartyCacheDir;
			// $this->smarty->debugging = TRUE;
		}

		protected function getStrFromBD($query){
			$result = mysqli_query($this->con, $query);
			if(!$result){
				$this->logMe("&#10;&#9;&#9;Ошибка в: '".__FUNCTION__."'.&#10;&#9;&#9;Инициатор:".$query.".&#10;&#9;&#9;Текст: ".mysqli_error($this->con)."&#10;  ","error" ,'log');
				return FALSE;
			}
			if(mysqli_num_rows($result)==1){
				return $result->fetch_array(MYSQLI_ASSOC);
			}else{
				return FALSE;
			}
		}
		protected function checkNumBeforeAdd($query){
			$result = mysqli_query($this->con, $query);
			if(!$result){
				$this->logMe("&#10;&#9;&#9;Ошибка в: '".__FUNCTION__."'.&#10;&#9;&#9;Инициатор:".$query.".&#10;&#9;&#9;Текст: ".mysqli_error($this->con)."&#10;  ","error" ,'log');
				return FALSE;
			}
			if(mysqli_num_rows($result) > 0){
				return FALSE;
			}else{
				return TRUE;
			}
		}

		protected function editItemValue($parent,$name,$newVal,$id){
			if($this->executeQuery("UPDATE ".$this->con->real_escape_string($parent)." SET ".$this->con->real_escape_string($name)."='".$this->con->real_escape_string($newVal)."' WHERE id = ".$id)){
				return 'Значение успешно изменено.';
			}else{
				return 'Хозяин, у нас пробемы. Изменения не вступили в силу.';
			}
		}
		
		public function getArrFromTableBYQuery($query){
			$result = mysqli_query($this->con,$query);
			if(!$result){
				$this->logMe("&#10;&#9;&#9;Ошибка в: '".__FUNCTION__."'.&#10;&#9;&#9;Инициатор:".$query.".&#10;&#9;&#9;Текст: ".mysqli_error($this->con)."&#10;  ","error" ,'log');
				return FALSE;
			}
			$dataRow = array();
			for($i = 0; $i < mysqli_num_rows($result); $i++){
				$dataRow[] = $result->fetch_array(MYSQLI_ASSOC);
			}
			return $dataRow;
		}

		public function logMe($logText, $logType, $tag, $traseArr=FALSE){
			$logFileName = date("d.m.Y").'log.xml';
			$rootTag = 'logs';
			if($traseArr AND $tag == 'traser'){
				$trs = array();
				$trs['line'] = $traseArr[0]['line'];
				$trs['func'] = $traseArr[0]['function'];
				$trs['class'] = $traseArr[0]['class'];
				$logFileName = date("d.m.Y").'trs.xml';
				$rootTag = 'trasers';
				$logText = 'Из класса: '.$trs['class'].', из функции: '.$trs['func'].', линия: '.$trs['line'];
			}
			$this->domObj = new DOMDocument("1.0","UTF-8");
			$this->domObj->preserveWhiteSpace = false;
			$this->domObj->formatOutput=true;

			if(!file_exists(pathToLogs.$logFileName)){
				$this->domObj->appendChild($this->domObj->createElement($rootTag));
				$this->domObj->save(pathToLogs.$logFileName);
			}
			$this->domObj->load(pathToLogs.$logFileName);

			$elLog = $this->domObj->createElement($tag,$logText);
			$attrList = array("hash" => $_SESSION['logHash'], "time" => date( "H:i:s" ), "type" => $logType);
			foreach ($attrList as $attrName => $attrVal){
				if($attrVal != ''){
					$newAttr = $this->domObj->createAttribute($attrName);
					$newAttr->value = $attrVal;
					$elLog->appendChild($newAttr);
				}
			}
			$this->domObj->documentElement->appendChild($elLog);
			$this->domObj->save(pathToLogs.$logFileName);
		}

		public function getPassword($length=6){
			 $arr = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','r','s','t','u','v','x','y','z',
						  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','X','Y','Z',
						  '1','2','3','4','5','6','7','8','9','0');
			$pass = "";
			for($i = 0; $i < $length; $i++){
			$index = rand(0, count($arr) - 1);
				$pass .= $arr[$index];
			}
			return $pass;
		}

		public function cyrillic_translit( $title ){
		    $iso9_table = array(
		        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Ѓ' => 'G',
		        'Ґ' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Є' => 'YE',
		        'Ж' => 'ZH', 'З' => 'Z', 'Ѕ' => 'Z', 'И' => 'I', 'Й' => 'J',
		        'Ј' => 'J', 'І' => 'I', 'Ї' => 'YI', 'К' => 'K', 'Ќ' => 'K',
		        'Л' => 'L', 'Љ' => 'L', 'М' => 'M', 'Н' => 'N', 'Њ' => 'N',
		        'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
		        'У' => 'U', 'Ў' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS',
		        'Ч' => 'CH', 'Џ' => 'DH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '',
		        'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',
		        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ѓ' => 'g',
		        'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'є' => 'ye',
		        'ж' => 'zh', 'з' => 'z', 'ѕ' => 'z', 'и' => 'i', 'й' => 'j',
		        'ј' => 'j', 'і' => 'i', 'ї' => 'yi', 'к' => 'k', 'ќ' => 'k',
		        'л' => 'l', 'љ' => 'l', 'м' => 'm', 'н' => 'n', 'њ' => 'n',
		        'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
		        'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
		        'ч' => 'ch', 'џ' => 'dh', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '',
		        'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
		    );

		    $name = strtr( $title, $iso9_table );
		    $name = preg_replace('~[^A-Za-z0-9\'_\-\.]~', '-', $name );
		    $name = preg_replace('~\-+~', '-', $name ); // --- на -
		    $name = preg_replace('~^-+|-+$~', '', $name ); // кил - на концах

		    return $name;
		}
		
		protected function executeQuery($query){
			$result = mysqli_query($this->con, $query);
			if(!$result){
				$this->logMe("&#10;&#9;&#9;Ошибка в: '".__FUNCTION__."'.&#10;&#9;&#9;Инициатор:".$query.".&#10;&#9;&#9;Текст: ".mysqli_error($this->con)."&#10;  ","error" ,'log');
				return FALSE;
			}
			return $result;
		}

		protected function print_arrr($arr){
			print("<br><br><br><pre>");
			print_r($arr);
			print("</pre><br><br><br>");
		}
	}
?>
