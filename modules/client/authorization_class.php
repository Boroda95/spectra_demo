<?php
	/**
	 * контроллер будет отвечать за авторизацию клиента клиента
	 */
	class authorization extends core{
		public function form_obr(){
			if(isset($_POST['logInSubmit'])){
				$psw = mysqli_real_escape_string($this->con, $_POST['password']);
				$phone = mysqli_real_escape_string($this->con, $_POST['phone']);
				$this->logMe('Попытка авторизироваться'.$phone,'info','log');
				$qResult = $this->getStrFromBD("SELECT id,surname,firstname FROM clients WHERE phone='".$phone."' and password='".$psw."'");
				if($qResult){
					$this->logMe('Правильный пароль.', 'info', 'log');
					$_SESSION['loginedClient'] = array(
						'status'       => TRUE,
						'fName'        => $qResult['firstname'],
						'sName'        => $qResult['surname'],
						'id'           => $qResult['id']
					);
					$this->logMe('Успешная авторизация: '.$phone, 'info', 'log');
					header("Location: http://".$_SERVER['HTTP_HOST'].'/status/');
				}else{
					$this->logMe('Неудачная авторизация: '.$phone, 'info', 'log');
					$_SESSION['logIn_error'] = "Вы ввели неверные авторизационные данные. Попробуйте еще разочек.";
					header("Location: http://".$_SERVER['HTTP_HOST'].'/authorization/');
				}
			}
		}

		public function get_page(){
			if($_GET['param_1'] == 'exit'){
				unset($_SESSION['loginedClient']);
				header("Location: http://".$_SERVER['HTTP_HOST'].'/authorization/');
			}
			if($_SESSION['loginedClient']['status'] == TRUE){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/status/');
			}

			$this->smarty->assign('title', "Авторизация");
			$this->smarty->display('header.tpl');
			$this->smarty->display('client/navbar.tpl');
			if(isset($_SESSION['logIn_error'])){
				$this->smarty->assign('logIn_error', $_SESSION['logIn_error']);
				unset($_SESSION['logIn_error']);
			}	
			$this->smarty->display('client/authForm.tpl');
			$this->smarty->display('client/footer.tpl');
		}
	}
?>