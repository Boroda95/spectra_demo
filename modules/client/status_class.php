<?php
	/**
	 * контроллер будет отвечать за вывод информации о заказе клиента
	 * если он удачно авторизировался
	 */
	class status extends core{
		public function form_obr(){
			exit;
		}

		protected function getCompletedTikets(){

		}

		public function get_page(){
			$arrayOfTikets = array(
				"completed"=> array(
					"tikets" => array(),
					"steps" => array()
				),
				"during" => array(
					"tikets" => array(),
					"steps" => array()
				),
				"bolt" => array(
					"tikets" => array(),
					"steps" => array()
				),
				"waiting" => array(
					"tikets" => array(),
					"steps" => array()
				),
				"archive" => array(
					"tikets" => array(),
					"steps" => array()
				)
			);
			if($_SESSION['loginedClient']['status'] == FALSE){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/authorization/');
			}
			$this->smarty->assign('title', "Кабинет клиента");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('loginedClient', $_SESSION['loginedClient']);
			$this->smarty->display('client/navbar.tpl');
			foreach ($arrayOfTikets as $arrKey => $arrValue){
				$query = "SELECT tk.id,tk.id_statuses, tSt.clientTktState, tk.actnum, tk.productname, tk.productseries, tk.admissiondate, tk.id_tiketState FROM tikets as tk
						  join tiketsStates as tSt on tk.id_tiketState = tSt.id 
						  WHERE tk.id_clients = '".$_SESSION['loginedClient']['id']."' AND tk.id_statuses = (SELECT id FROM statuses WHERE statuscode = '".$arrKey."')";
				$arrayOfTikets[$arrKey]['tikets'] = $this->getArrFromTableBYQuery($query);
				foreach ($arrayOfTikets[$arrKey]['tikets'] as $tktKey => $tktVal){
					$query = "SELECT id,stageimages, stagename, stagedescription, stagedate FROM stages WHERE id_tikets = '".$tktVal['id']."'";
					$temp = $this->getArrFromTableBYQuery($query);
					if(count($temp) != 0){
						$arrayOfTikets[$arrKey]['steps'][$tktVal['id']] = $temp;
					}else{
						$arrayOfTikets[$arrKey]['steps'][$tktVal['id']] = 'empty';
					}
				}
			}

			$this->smarty->assign('arrayOfTikets', $arrayOfTikets);
			$this->smarty->display('client/status.tpl');
			$this->smarty->display('client/footer.tpl');
		}
	}
?>