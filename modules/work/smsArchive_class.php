<?php
class smsArchive extends core{
	public function form_obr(){

	}
	protected function getallSmsArchive(){

		exit(json_encode($clientSms));
	}

	public function get_page(){
		if($_SESSION['logined']['status'] == FALSE){
			header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
		}
		if(in_array($_SESSION['logined']['rights'], array('admin','manager'))){
			if(isset($_GET['param_1'])){
				$this->getallSmsArchive();
			}else{
				$this->smarty->assign('title', "Архив sms сообщений");
				$this->smarty->display('header.tpl');
				$this->smarty->assign('logined', $_SESSION['logined']);
				$this->smarty->display('work/navbar.tpl');
				$clientSms =  $this->getArrFromTableBYQuery("
								SELECT id,clientPhone,smsID,smsContext,author,sendDate,statusDescription 
								FROM smsArchive");
				$json = json_decode(file_get_contents('config.json'), true);
				$statusJson = $json['smsStatuses'];
				unset($json);
				$counter = 1;
				foreach ($clientSms as $clientSmsKey => $clientSmsArr){
					if($clientSmsArr['statusDescription'] != '103'){
						$temp = file_get_contents('https://sms.ru/sms/status?api_id=7850d916-ead5-a464-49c0-7568c8845c0b&sms_id='.$clientSmsArr["smsID"].'&json=1');
						$json = json_decode($temp, true);
						if($json['status'] == "OK"){
							$this->executeQuery("UPDATE smsArchive SET statusDescription ='".$json['sms'][$clientSmsArr["smsID"]]['status_code']."' WHERE id = ".$clientSmsArr['id']);
							$clientSms[$clientSmsKey]['statusDescription'] = $statusJson[$json['sms'][$clientSmsArr["smsID"]]['status_code']];
							unset($json);
						}
					}else{
						$clientSms[$clientSmsKey]['statusDescription'] = $statusJson[$clientSmsArr['statusDescription']];
					}
					unset($clientSms[$clientSmsKey]['smsID']);
					$clientSms[$clientSmsKey]['counter'] = $counter;
					$counter++;
				}
				$this->smarty->assign('clientSms', $clientSms);
				$this->smarty->display('work/smsArchive.tpl');
				$this->smarty->display('work/footer.tpl');
			}
		}else{
			header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
		}
	}
}