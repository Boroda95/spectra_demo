<?php 
  class backupDB extends core{
    public function form_obr(){
    }
    public function get_page(){

      $tables = array();
      $result = mysqli_query($this->con,'SHOW TABLES');
      while($row = mysqli_fetch_row($result)){
        $tables[] = $row[0];
      }
      //cycle through each table and format the data
      foreach($tables as $table){
        $result = mysqli_query($this->con,'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);
        $return.= 'DROP TABLE '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($this->con,'SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";
        for ($i = 0; $i < $num_fields; $i++){
                while($row = mysqli_fetch_row($result)){
                        $return.= 'INSERT INTO '.$table.' VALUES(';
                        for($j=0; $j<$num_fields; $j++){
                                $row[$j] = addslashes($row[$j]);
                                $row[$j] = str_replace("\n","\\n",$row[$j]);
                                if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                if ($j<($num_fields-1)) { $return.= ','; }
                        }
                        $return.= ");\n";
                }
        }
        $return.="\n\n\n";
      }
      $handle = fopen('db_backups/db-backup-'.date('Y-m-d_H-i').'-'.(md5(implode(',',$tables))).'.sql','w+');
      fwrite($handle,$return);
      fclose($handle);
      $files = scandir('db_backups');
      if(count($files) > 62) {
        unlink('db_backups/'.$files[2]);
      }
      PRINT("<h1>Бэкап базы успешно создан. <a href='/workspace/'> На главную</a></h1>");
    }
  }
 ?>