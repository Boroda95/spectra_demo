<?php
class noAccess extends core{
    public function get_page(){
        if($_SESSION['logined']['status'] == FALSE){
            header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
        }

        if(in_array($_SESSION['logined']['rights'], array('noAccess'))){
            $this->smarty->assign('title', "Транзитная зона.");
            $this->smarty->display('header.tpl');
            $this->smarty->assign('logined', $_SESSION['logined']);
            $this->smarty->display('work/navbar.tpl');
            $this->smarty->display('work/noAccess.tpl');
            $this->smarty->display('work/footer.tpl');
        }else{
            header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
        }
    }
}
?>


