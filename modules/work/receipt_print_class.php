<?php
	class receipt_print extends core{
		public function form_obr(){
			if($_POST['getTiketInfo']){
				$query = "
					SELECT cl.firstname,cl.middlename,cl.surname,cl.phone,tk.actnum,tk.productname,tk.productseries,tk.completeness,
					       tk.admissiondate, tq.equipmentname
					FROM clients as cl join tikets as tk on tk.id_clients = cl.id
									   join typesofequipment as tq on tk.id_typesofequipment = tq.id 
					WHERE tk.id = '".$_POST['tiketID']."'
				";
				exit(json_encode($this->getArrFromTableBYQuery($query)));
			}
		}

		public function get_page(){
			if($_SESSION['logined']['status'] == FALSE){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
			}
			if(!isset($_GET['param_1'])){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
			}
			$this->smarty->assign('thisID', $_GET['param_1']);
			$this->smarty->display('work/receipt_print.tpl');
		}
	}
?>