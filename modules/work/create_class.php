<?php
class create extends core{
	public $json = false;
	public function form_obr(){
		if($_POST['clients'] == 'add' || $_POST['tikets'] == 'add'){
			$keys = '';
			$vals = '';
			foreach ($_POST as $key => $value){
				if($key == 'actnum'){
					$ACTNUM = $this->getStrFromBD("SELECT MAX(actnum) AS maxVal FROM tikets");
					$ACTNUM = (int)$ACTNUM['maxVal']+1;
					$keys = $keys.$key.',';
					$vals = $vals."'".$ACTNUM."',";
				}elseif($key == 'admissiondate'){
					$keys = $keys.$key.',';
					$vals = $vals."'".date('Y-m-d H:i:s')."',";
				}elseif($key == 'birthday'){ 
					$keys = $keys.$key.',';
					list($Y, $M, $D) = explode("-", $value);
					$vals = $vals."'".$D.".".$M.".".$Y."',";
				}elseif($key == 'crdtCrdIssuanceDate'){ 
					$keys = $keys.$key.',';
					list($Y, $M, $D) = explode("-", $value);
					$vals = $vals."'".$D.".".$M.".".$Y."',";
				}elseif($value != 'add'){
					$keys = $keys.$key.',';
					$vals = $vals."'".$this->con->real_escape_string($value)."',";
				}else{
					$table = $key;
				}
			}
			$vals = trim($vals, ",");
            $keys = trim($keys, ",");
			if($this->executeQuery("INSERT INTO ".$table." (".$keys.") VALUES (".$vals.")")){
                if($_POST['tikets'] == "add"){
                    header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
                }else{
                    header('Refresh: 1; URL='.$_SERVER['HTTP_REFERER']);
                }
			}else{
				print(mysqli_error($this->con));
			}
		}
		if($_POST['userDataFinder']){
			$id = mysqli_real_escape_string($this->con, $_POST['id']);
			$info = $this->getArrFromTableBYQuery("SELECT id,firstname,surname,middlename,email,clientStatus,creditCardNum FROM clients WHERE id='".$id."'");
			print(json_encode($info[0]));
			exit;
		}
	}
	public function getSortedTiketTpls($defaultList){
		$arr_tpls = array();
		foreach ($defaultList as $key => $value) {
			if(empty($arr_tpls[$value['tplParent']])) {
				$arr_tpls[$value['tplParent']] = array();
			}
			$arr_tpls[$value['tplParent']]['groupName'] = $value['groupName'];
			$arr_tpls[$value['tplParent']]['groupId'] = $value['grpId'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplId'] = $value['tplId'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplName'] = $value['tplName'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplDescription'] = $value['tplDescription'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplParent'] = $value['tplParent'];
		}
		return $arr_tpls;
	}
	public function get_page(){
		if($_SESSION['logined']['status'] == FALSE){
			header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
		}
		if(in_array($_SESSION['logined']['rights'], array('admin','manager','moderator'))){
			$this->smarty->assign('title', "Создание клиентов и заказов");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('logined', $_SESSION['logined']);
			$this->smarty->display('work/navbar.tpl');
			$this->json = json_decode(file_get_contents('config.json'), true);
			
			$this->smarty->assign('config', $this->json);
			$this->smarty->assign('newUserPassword', $this->getPassword());
			$clients['data'] = $this->getArrFromTableBYQuery("
				SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.password,cls.id as clientStatus
				FROM clients as cl 
				join clientStatuses as cls on cl.clientStatus = cls.id");
			foreach ($this->json['clients']['columns'] as $jsnKey => $jsnVal){
				if(!isset($clients['refs'][$jsnKey]) && $jsnVal['colType'] == 'select'){
					$clients['refs'][$jsnKey] = $this->getArrFromTableBYQuery("SELECT ".$jsnVal['selectColumns']." FROM ".$jsnVal['relatedTable']);
				}
			}
			$this->smarty->assign('clients', $clients);
			unset($clients);
			$this->smarty->assign('equipments', $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment"));

			$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
			$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
			$this->smarty->assign('tktCmpltnItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmpltn'"));
			$this->smarty->assign('tktExtViewItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktExtView'"));
			$this->smarty->assign('tktCmmntItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmmnt'"));
			$this->smarty->assign('smsTemlates', $this->getArrFromTableBYQuery("SELECT id,templateText FROM smsTemplates"));
			$miniPriceList = $this->getArrFromTableBYQuery(
                                        "SELECT grp.id AS grpId, grp.groupName, tpl.id AS tplId, tpl.tplName, tpl.tplDescription, tpl.tplParent 
                                        FROM tiketTplGroups AS grp JOIN tiketTplData AS tpl ON grp.id = tplParent
                                        WHERE grp.target = 'miniprice'");
            // print_r($this->getSortedTiketTpls($miniPriceList));
            // exit();
			$this->smarty->assign('miniPriceMenuArr', $this->getSortedTiketTpls($miniPriceList));

			$this->smarty->display('work/create.tpl');
			$this->smarty->display('work/footer.tpl');
		}
	}
}
?>