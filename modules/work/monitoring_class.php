<?php
class monitoring  extends core{
    public function get_page(){
        if($_SESSION['logined']['status'] == FALSE){
            header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
        }
        if($_GET['param_1'] == 'stagesTable'){
            $query = "
            SELECT DISTINCT tkt.actnum, mng.synonym, stg.stagedate, tkt.productname, stg.stagedescription, 
                   CONCAT_WS(' + ', tkSt.tktState,statuses.statusname) AS state, tkt.id AS tiketID, stg.id AS stageID
            FROM tktStagesMonitoring AS tsm 
            JOIN stages AS stg ON tsm.stageId = stg.id 
            JOIN tikets AS tkt ON tkt.id = stg.id_tikets
            JOIN managers AS mng ON stg.stageauthor = mng.id
            JOIN statuses ON tkt.id_statuses = statuses.id
            JOIN tiketsStates AS tkSt ON tkSt.id = tkt.id_tiketState ";
            
            $search = false;
            $mngFilter = false;
            if($_GET['search']['value']){
                $sDT = $_GET['search']['value'];
                $query .= "WHERE (";
                foreach ($_GET['columns'] as $clmnsKey => $clmnsVal) {
                    if($clmnsVal['data'] != 'stagedate'){
                        if($clmnsVal['data'] == 'state'){
                            $clmnsVal['data'] = 'tktState';
                        }
                        if($clmnsKey != count($_GET['columns'])-1){
                            $query .= $clmnsVal['data']." LIKE '%".$sDT."%' OR ";
                        }else{
                            $query .= $clmnsVal['data']." LIKE '%".$sDT."%') ";
                        }
                    }
                }
                $search = true;
            }
            if($_GET['managersFilterForm'][0]['value'] != '*'){
                if($search){
                    $query .= " AND (";
                }else{
                    $query .= "WHERE (";
                }
                foreach ($_GET['managersFilterForm'] as $clmnsKey => $clmnsVal) {
                    if($clmnsKey != count($_GET['managersFilterForm'])-1){
                        $query .= " stg.stageauthor = '".$clmnsVal['value']."' OR ";
                    }else{
                        $query .= " stg.stageauthor = '".$clmnsVal['value']."') ";
                    }
                }
                $mngFilter = true;
            }

            if($_GET['dateFilterForm']){
                if($_GET['dateFilterForm'][0]['value'] == 'period'){
                    $startDate = $_GET['dateFilterForm'][1]['value'].' 00:00:00';
                    $endDate = $_GET['dateFilterForm'][2]['value'].' 23:59:59';
                }elseif($_GET['dateFilterForm'][0]['value'] == 'week'){
                    $startDate = date("Y-m-d", time()-60*60*24*7).' 00:00:00';
                    $endDate = date('Y-m-d').' 23:59:59';
                }elseif($_GET['dateFilterForm'][0]['value'] == 'month'){
                    $startDate = date("Y-m-d", time()-60*60*24*31).' 00:00:00';
                    $endDate = date('Y-m-d').' 23:59:59';
                }elseif($_GET['dateFilterForm'][0]['value'] == 'threeMonth'){
                    $startDate = date("Y-m-d", time()-60*60*24*93).' 00:00:00';
                    $endDate = date('Y-m-d').' 23:59:59';
                }
                if($mngFilter || $search){
                    $query .= " AND (stg.stagedate BETWEEN '".$startDate."' AND '".$endDate."')";
                }else{
                    $query .= "WHERE (stg.stagedate BETWEEN '".$startDate."' AND '".$endDate."')";
                }
            }
            $fullDataQuery = $query;
            $query .= " ORDER BY ";
            foreach ($_GET['order'] as $clmnsKey => $clmnsVal) {
                $colName = $_GET['columns'][$clmnsVal['column']]['data'];
                if($clmnsKey != count($_GET['order'])-1){
                    $query .= $colName." ".$clmnsVal['dir'].", ";
                }else{
                    $query .= $colName." ".$clmnsVal['dir']." ";
                }
            }
            $query .= "LIMIT ".$_GET['start'].",".$_GET['length'];
            $dataArr['data'] = $this->getArrFromTableBYQuery($query);
            $fullCount = count($this->getArrFromTableBYQuery($fullDataQuery));
            $dataArr["recordsFiltered"] = $fullCount;
            $dataArr["recordsTotal"] = $fullCount;
            $resArr["draw"] = intval($_GET['draw']);
            print(json_encode($dataArr));
            exit();
        }

        if($_GET['param_1'] == 'statusTable'){
            $query = "
            SELECT DISTINCT tkt.actnum, mng.synonym, tkt.productname, tsm.eventDate, sts1.statusname AS oldStatus, sts2.statusname AS newStatus, tkt.id AS tiketID
            FROM tktStatusMonitoring AS tsm 
            JOIN tikets AS tkt ON tkt.id = tsm.tiketId 
            JOIN managers AS mng ON tsm.authorId = mng.id 
            JOIN statuses AS sts1 ON tsm.oldStatus = sts1.id 
            JOIN statuses AS sts2 ON tsm.newStatus = sts2.id ";
            
            $search = false;
            $mngFilter = false;
            if($_GET['search']['value']){
                $sDT = $_GET['search']['value'];
                $query .= "WHERE (";
                foreach ($_GET['columns'] as $clmnsKey => $clmnsVal) {
                    if($clmnsVal['data'] != 'eventDate'){
                        if($clmnsKey != count($_GET['columns'])-1){
                            $query .= $clmnsVal['data']." LIKE '%".$sDT."%' OR ";
                        }else{
                            $query .= $clmnsVal['data']." LIKE '%".$sDT."%') ";
                        }
                    }
                }
                $search = true;
            }
            if($_GET['managersFilterForm'][0]['value'] != '*'){
                if($search){
                    $query .= " AND (";
                }else{
                    $query .= "WHERE (";
                }
                foreach ($_GET['managersFilterForm'] as $clmnsKey => $clmnsVal) {
                    if($clmnsKey != count($_GET['managersFilterForm'])-1){
                        $query .= " tsm.authorId = '".$clmnsVal['value']."' OR ";
                    }else{
                        $query .= " tsm.authorId = '".$clmnsVal['value']."') ";
                    }
                }
                $mngFilter = true;
            }

            if($_GET['dateFilterForm']){
                if($_GET['dateFilterForm'][0]['value'] == 'period'){
                    $startDate = $_GET['dateFilterForm'][1]['value'].' 00:00:00';
                    $endDate = $_GET['dateFilterForm'][2]['value'].' 23:59:59';
                }elseif($_GET['dateFilterForm'][0]['value'] == 'week'){
                    $startDate = date("Y-m-d", time()-60*60*24*7).' 00:00:00';
                    $endDate = date('Y-m-d').' 23:59:59';
                }elseif($_GET['dateFilterForm'][0]['value'] == 'month'){
                    $startDate = date("Y-m-d", time()-60*60*24*31).' 00:00:00';
                    $endDate = date('Y-m-d').' 23:59:59';
                }elseif($_GET['dateFilterForm'][0]['value'] == 'threeMonth'){
                    $startDate = date("Y-m-d", time()-60*60*24*93).' 00:00:00';
                    $endDate = date('Y-m-d').' 23:59:59';
                }
                if($mngFilter || $search){
                    $query .= " AND (tsm.eventDate BETWEEN '".$startDate."' AND '".$endDate."')";
                }else{
                    $query .= "WHERE (tsm.eventDate BETWEEN '".$startDate."' AND '".$endDate."')";
                }
            }
            $fullDataQuery = $query;
            $query .= " ORDER BY ";
            foreach ($_GET['order'] as $clmnsKey => $clmnsVal) {
                $colName = $_GET['columns'][$clmnsVal['column']]['data'];
                if($clmnsKey != count($_GET['order'])-1){
                    $query .= $colName." ".$clmnsVal['dir'].", ";
                }else{
                    $query .= $colName." ".$clmnsVal['dir']." ";
                }
            }
            $query .= "LIMIT ".$_GET['start'].",".$_GET['length'];
            $dataArr['data'] = $this->getArrFromTableBYQuery($query);
            $fullCount = count($this->getArrFromTableBYQuery($fullDataQuery));
            $dataArr["recordsFiltered"] = $fullCount;
            $dataArr["recordsTotal"] = $fullCount;
            $resArr["draw"] = intval($_GET['draw']);
            print(json_encode($dataArr));
            exit();
        }
        if(in_array($_SESSION['logined']['rights'], array('admin'))){
            $this->smarty->assign('title', "Мониторинг движухи в заказах.");
            $this->smarty->display('header.tpl');
            $this->smarty->assign('logined', $_SESSION['logined']);
            $this->smarty->assign('masters', $this->getArrFromTableBYQuery("SELECT mng.id, mng.synonym, mng.rights FROM managers as mng WHERE mng.rights <> 'noAccess'"));
            $this->smarty->display('work/navbar.tpl');
            $this->smarty->display('work/monitoring/monitoring.tpl');
            $this->smarty->display('work/footer.tpl');
        }else{
            header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');    
        }
    }
}
?>

