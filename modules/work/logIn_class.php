<?php
	class logIn extends core{
		public function form_obr(){
			if(isset($_POST['logInSubmit'])){
				$psw = md5(mysqli_real_escape_string($this->con, $_POST['password']));
				$mail = mysqli_real_escape_string($this->con, $_POST['email']);
				$this->logMe('Попытка авторизироваться'.$mail,'info','log');
				$qResult = $this->getStrFromBD("SELECT id,synonym,email,fName,sName,rights FROM managers WHERE email='".$mail."' and password='".$psw."'");
				if($qResult){
					$this->logMe('Правильный пароль.', 'info', 'log');
					$_SESSION['logined'] = array(
						'status'       => TRUE, 
						'rights'       => $qResult['rights'], 
						'id'           => $qResult['id'], 
						'login'        => $qResult['synonym'],
						'fName'        => $qResult['fName'],
						'sName'        => $qResult['sName'],
					);
					$this->logMe('Успешная авторизация: '.$mail, 'info', 'log');
					header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
				}else{
					$this->logMe('Неудачная авторизация: '.$mail, 'info', 'log');
					$_SESSION['logIn_error'] = "Вы ввели неверные авторизационные данные. Попробуйте еще разочек.";
					header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
				}
			}
		}

		public function get_page(){
			if($_GET['param_1'] == 'exit'){
				unset($_SESSION['logined']);
				header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
			}
			if($_SESSION['logined']['status'] == TRUE){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
			}
			
			$this->smarty->assign('title', "Авторизация");
			$this->smarty->display('header.tpl');
			$this->smarty->display('work/navbar.tpl');
			if(isset($_SESSION['logIn_error'])){
				$this->smarty->assign('logIn_error', $_SESSION['logIn_error']);
				unset($_SESSION['logIn_error']);
			}	
			$this->smarty->display('work/authForm.tpl');
			$this->smarty->display('work/footer.tpl');
		}
	}
?>