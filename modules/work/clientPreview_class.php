<?php
    class clientPreview extends core{
        protected function getClientContentById($id){
            $client = $this->getArrFromTableBYQuery("
                                SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.secondphone,
                                cl.password,cl.creditCardNum,cl.creditCardCode,cl.crdtCrdIssuanceDate,cl.birthday,
                                cl.gender,cl.haveChilds_14old,cl.haveCar,cl.havePets,cl.adress,cl.hiddenInfo, cls.id as clientStatus
                                FROM clients as cl 
                                join clientStatuses as cls on cl.clientStatus = cls.id 
                                WHERE cl.id=" . $id);
            $json = json_decode(file_get_contents('config.json'), true);
            $clJson = $json['clients'];
            unset($json);
            foreach ($clJson['columns'] as $jsnKey => $jsnVal) {
                if ($jsnVal['colType'] == 'select') {
                    $client['refs'][$jsnKey] = $this->getArrFromTableBYQuery("SELECT " . $jsnVal['selectColumns'] . " FROM " . $jsnVal['relatedTable']);
                }
            }
            $this->smarty->assign('client', $client);
            $this->smarty->assign('clientsConfig', $clJson);
            $query = "SELECT tk.id as tktId, st.statuscode,tk.actnum FROM tikets AS tk JOIN statuses AS st ON tk.id_statuses = st.id WHERE id_clients = '" . $client[0]['id'] . "'";
            $tiketHistory = $this->getArrFromTableBYQuery($query);

            $this->smarty->assign('tiketHistory', $tiketHistory);
            $this->smarty->assign('logined', $_SESSION['logined']);
            // exit($this->smarty->display('work/getUserFullInfo.tpl'));
            return $client;
        }

        public function get_page(){
            if($_SESSION['logined']['status'] == FALSE){
                header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
            }
            if(!isset($_GET['param_1'])){
                header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
            }

            $thisClientID = $_GET['param_1'];

            $client = $this->getClientContentById($thisClientID);
            $this->smarty->assign('title', "Клиент: ".$client[0]['surname']." ".$client[0]['firstname']);
            
            $this->smarty->display('header.tpl');
            $this->smarty->assign('logined', $_SESSION['logined']);
            $this->smarty->display('work/navbar.tpl');

            $this->smarty->display('work/clientPreview.tpl');
            $this->smarty->display('work/footer.tpl');
        }
    }
?>