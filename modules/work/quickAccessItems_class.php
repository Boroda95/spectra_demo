<?php
class quickAccessItems extends core{
	public $json = false;
	public function form_obr(){
		if($_POST['addNewItemTo']){
			$items = $this->con->real_escape_string($_POST['items']);
			$target = $this->con->real_escape_string($_POST['target']);
			$this->executeQuery("INSERT INTO ".$_POST['addNewItemTo']." (items,target) VALUES ('".$items."','".$target."')");
			header('Refresh: 1; URL='.$_SERVER['HTTP_REFERER']);
		}
		if($_POST['addNewStatus']){
			$statusname = $this->con->real_escape_string($_POST['statusname']);
			$this->executeQuery("INSERT INTO ".$_POST['addNewStatus']." (statusname) VALUES ('".$statusname."')");
			header('Refresh: 1; URL='.$_SERVER['HTTP_REFERER']);
		}
		if($_POST['addNewTemplate']){
			$templatetext = $this->con->real_escape_string($_POST['templatetext']);
			$this->executeQuery("INSERT INTO ".$_POST['addNewTemplate']." (templatetext) VALUES ('".$templatetext."')");
			header('Refresh: 1; URL='.$_SERVER['HTTP_REFERER']);
		}
		if($_POST['addNewPriceItem']){
			$templatetext = $this->con->real_escape_string($_POST['item']);
			$this->executeQuery("INSERT INTO miniPrice (items) VALUES ('".$templatetext."')");
			header('Refresh: 1; URL='.$_SERVER['HTTP_REFERER']);
		}
		if($_POST['addNewTplGroup']){
            $groupName = $this->con->real_escape_string($_POST['groupName']);
            $groupTarget = $this->con->real_escape_string($_POST['groupTarget']);
            $this->executeQuery("INSERT INTO tiketTplGroups (groupName,target) VALUES ('" . $groupName . "','" . $groupTarget . "')");

            $miniPriceList = $this->getArrFromTableBYQuery(
                "SELECT grp.id AS grpId, grp.groupName, tpl.id AS tplId, tpl.tplName, tpl.tplDescription, tpl.tplParent 
                                        FROM tiketTplGroups AS grp JOIN tiketTplData AS tpl ON grp.id = tplParent
                                        WHERE grp.target = 'miniprice'"
            );
            $this->smarty->assign('miniPriceMenuArr', $this->getSortedTiketTpls($miniPriceList));
            $this->smarty->assign('tiketTplAllGroups', $this->getArrFromTableBYQuery("SELECT id, groupName FROM tiketTplGroups"));
            exit($this->smarty->display('work/dynamicBlocks/quickAccessMinipriceTab.tpl'));
		}
		if($_POST['addNewTiketTpl']){
            $tplName = $this->con->real_escape_string($_POST['tplName']);
            $tplDescription = $this->con->real_escape_string($_POST['tplDescription']);
            $tplParent = $this->con->real_escape_string($_POST['tplParent']);
            $this->executeQuery("INSERT INTO tiketTplData (tplName,tplDescription,tplParent) 
                                        VALUES ('".$tplName."','".$tplDescription."','".$tplParent."')");
            $miniPriceList = $this->getArrFromTableBYQuery(
                "SELECT grp.id AS grpId, grp.groupName, tpl.id AS tplId, tpl.tplName, tpl.tplDescription, tpl.tplParent 
                                        FROM tiketTplGroups AS grp JOIN tiketTplData AS tpl ON grp.id = tplParent
                                        WHERE grp.target = 'miniprice'"
            );
            $this->smarty->assign('miniPriceMenuArr', $this->getSortedTiketTpls($miniPriceList));
            $this->smarty->assign('tiketTplAllGroups', $this->getArrFromTableBYQuery("SELECT id, groupName FROM tiketTplGroups"));
            exit($this->smarty->display('work/dynamicBlocks/quickAccessMinipriceTab.tpl'));
		}
    }
    public function getSortedTiketTpls($defaultList){
		$arr_tpls = array();
		foreach ($defaultList as $key => $value) {
			if(empty($arr_tpls[$value['tplParent']])) {
				$arr_tpls[$value['tplParent']] = array();
			}
			$arr_tpls[$value['tplParent']]['groupName'] = $value['groupName'];
			$arr_tpls[$value['tplParent']]['groupId'] = $value['grpId'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplId'] = $value['tplId'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplName'] = $value['tplName'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplDescription'] = $value['tplDescription'];
			$arr_tpls[$value['tplParent']]['tplData'][$value['tplId']]['tplParent'] = $value['tplParent'];
		}
		return $arr_tpls;
	}
	public function get_page(){
		if($_SESSION['logined']['status'] == FALSE){
			header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
		}

		if(in_array($_SESSION['logined']['rights'], array('admin','manager','remontnik','moderator'))){
			$this->smarty->assign('title', "Quick Access");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('logined', $_SESSION['logined']);
			$this->smarty->display('work/navbar.tpl');
			$this->json = json_decode(file_get_contents('config.json'), true);
			$this->smarty->assign('config', $this->json);

			$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
			$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
			$this->smarty->assign('tktCmpltnItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmpltn'"));
			$this->smarty->assign('tktExtViewItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktExtView'"));
			$this->smarty->assign('tktCmmntItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmmnt'"));
            $miniPriceList = $this->getArrFromTableBYQuery(
                "SELECT grp.id AS grpId, grp.groupName, tpl.id AS tplId, tpl.tplName, tpl.tplDescription, tpl.tplParent 
                                        FROM tiketTplGroups AS grp JOIN tiketTplData AS tpl ON grp.id = tplParent
                                        WHERE grp.target = 'miniprice'"
            );
            $this->smarty->assign('miniPriceMenuArr', $this->getSortedTiketTpls($miniPriceList));
            $this->smarty->assign('tiketTplAllGroups', $this->getArrFromTableBYQuery("SELECT id, groupName FROM tiketTplGroups"));
            $this->smarty->assign('defaultMiniPriceTable', $miniPriceList);
			$this->smarty->assign('clientStatuses', $this->getArrFromTableBYQuery("SELECT id,statusname FROM clientStatuses"));
			$this->smarty->assign('smsTemplates', $this->getArrFromTableBYQuery("SELECT id,templatetext FROM smsTemplates"));

			$this->smarty->display('work/quickAccessItems.tpl');
			$this->smarty->display('work/footer.tpl');
		}else{
			header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
		}
	}
}
?>