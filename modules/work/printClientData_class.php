<?php
	class printClientData extends core{
		public function get_page(){
			if($_SESSION['logined']['status'] == FALSE){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
			}
			if(!isset($_GET['param_1'])){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
			}
			if(isset($_GET['param_1'])){
				$client =  $this->getArrFromTableBYQuery("
							SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.secondphone,
							   cl.password,cl.creditCardNum,cl.creditCardCode,cl.crdtCrdIssuanceDate,cl.birthday,
							   cl.gender,cl.haveChilds_14old,cl.haveCar,cl.havePets,cl.adress
							FROM clients as cl
							WHERE cl.id=".$_GET['param_1']);
			$json = json_decode(file_get_contents('config.json'), true);
			$clJson = $json['clients'];
			foreach ($clJson['columns'] as $jsnKey => $jsnVal){
				if($jsnVal['colType'] == 'radioTandem'){
					$client[0][$jsnKey] = $jsnVal['tandemItems'][$client[0][$jsnKey]];
				}
			}
			// $this->print_arrr($client);
			$this->smarty->assign('client', $client[0]);
			$this->smarty->display('work/printClientData.tpl');
			}else{
				header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
			}
		}
	}
?>