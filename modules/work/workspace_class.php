<?php
class workspace extends core{
	public $json = false;
	public function form_obr(){
		if($_POST['ImgExpressEditor']){
			exit(json_encode(array('message' => $this->editItemValue($_POST['parent'],$_POST['name'],serialize($_POST['new_val']),$_POST['id']))));
		}
	}

	protected function getTikets($tktsTypes='default'){
		$query = "
			SELECT cl.id as clntID,cl.firstname,cl.middlename,cl.surname,cl.phone,cl.secondphone,cl.email,cl.creditCardNum,st.id as id_statuses, tSt.id as id_tiketState, is_notified,
                   tk.id as tktID,tk.id_typesofequipment,tk.actnum,tk.hiddenstatus,tk.productname,tk.productseries,tk.completeness,tk.faultdescription,tk.exteriorview,
                   tk.tktcomment,tk.admissiondate,tk.miniPrice,tk.master,tk.priority,tk.priceRange
			FROM clients as cl join tikets as tk on tk.id_clients = cl.id
							   join typesofequipment as tq on tk.id_typesofequipment = tq.id 
							   join statuses as st on tk.id_statuses = st.id 
							   join tiketsStates as tSt on tk.id_tiketState = tSt.id 
		";
		if($tktsTypes == 'archive'){
			$query .= "	WHERE st.statuscode = 'archive'";
		}elseif($tktsTypes == 'completed'){
			$query .= "	WHERE st.statuscode = 'completed'";
		}elseif($tktsTypes == 'bolt'){
			$query .= "	WHERE st.statuscode = 'bolt'";
		}else{
			$query .= "	WHERE st.statuscode = 'during'";
		}
		$result = mysqli_query($this->con,$query);
		if(!$result){
			$this->logMe("&#10;&#9;&#9;Ошибка в: '".__FUNCTION__."'.&#10;&#9;&#9;Инициатор:".$query.".&#10;&#9;&#9;Текст: ".mysqli_error($this->con)."&#10;  ","error" ,'log');
			return FALSE;
		}
		$dataRow = array();
		$dataRow['refs'] = array();
		for($i = 0; $i < mysqli_num_rows($result); $i++){
			$buf = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$dataRow['data'][$i]['tiket'] = $buf;
			foreach ($this->json as $confKey => $confArr) {
				foreach ($buf as $bufKey => $bufVal) {
					if(!in_array($bufKey, $dataRow['refs']) && $confArr['columns'][$bufKey]['colType'] == 'select' && !isset($dataRow['refs'][$bufKey])){
						$dataRow['refs'][$bufKey] = $this->getArrFromTableBYQuery("SELECT ".$confArr['columns'][$bufKey]['selectColumns']." FROM ".$confArr['columns'][$bufKey]['relatedTable']);
					}
				}
			}
			$query = "SELECT tk.id as tktId, st.statuscode,tk.actnum FROM tikets AS tk JOIN statuses AS st ON tk.id_statuses = st.id WHERE id_clients = '".$buf['clntID']."'";
			$dataRow['tiketHistory'][$buf['tktID']] = $this->getArrFromTableBYQuery($query);
			if($tktsTypes != 'archive'){
				$query = "SELECT st.id,st.stageimages,st.stagename,st.stagedescription,st.hidstagedescription,st.stagedate,st.stagePrice, mg.synonym as stageauthor 
						  FROM stages as st join managers as mg on st.stageauthor = mg.id 
						  WHERE id_tikets = '".$buf['tktID']."' ORDER BY  st.stagedate ASC";
				$dataRow['data'][$i]['steps'] = $this->getArrFromTableBYQuery($query);
			}
		}
		return $dataRow;
	}

    protected function tiketsTableAction(){
        $fullDataQuery = "SELECT count(cl.id) as fullData FROM clients as cl join tikets as tk on tk.id_clients = cl.id
							   join typesofequipment as tq on tk.id_typesofequipment = tq.id 
							   join statuses as st on tk.id_statuses = st.id 
                               join tiketsStates as tSt on tk.id_tiketState = tSt.id WHERE st.statuscode = '".$_GET["statuscode"]."'";
        $query = "
			SELECT cl.id as clntID,cl.firstname,cl.middlename,cl.surname,cl.phone,cl.secondphone,cl.email,st.id as id_statuses, tSt.id as id_tiketState, is_notified,
                   tk.id as tktID,tk.id_typesofequipment,tk.actnum,tk.hiddenstatus,tk.productname,tk.productseries,tk.completeness,tk.faultdescription,tk.exteriorview,
                   tk.tktcomment,tk.admissiondate,tk.miniPrice,tk.master,tk.priority
			FROM clients as cl join tikets as tk on tk.id_clients = cl.id
							   join typesofequipment as tq on tk.id_typesofequipment = tq.id 
							   join statuses as st on tk.id_statuses = st.id 
							   join tiketsStates as tSt on tk.id_tiketState = tSt.id ";
       
        $query .= "WHERE st.statuscode = '".$_GET["statuscode"]."' AND ";
        if($_GET['managersFilterForm'][0]['value'] != '*'){
            $managersFilter = "";
            foreach ($_GET['managersFilterForm'] as $clmnsKey => $clmnsVal) {
                $managersFilter .= ($clmnsKey != count($_GET['managersFilterForm'])-1) ? " tk.master = '".$clmnsVal['value']."' OR " : " tk.master = '".$clmnsVal['value']."'";
            }
            $query .= " (".$managersFilter.") AND ";
        }
        if(count($_GET['columns'])!=0){
             $query .= " (";
            foreach ($_GET['columns'] as $clmnsKey => $clmnsVal) {
                if($clmnsVal['searchable'] == "true"){
                    $query .= ($clmnsKey != count($_GET['columns'])-1) ? $clmnsVal['data']." LIKE '%".$_GET['search']['value']."%' OR " : $clmnsVal['data']." LIKE '%".$_GET['search']['value']."%' ";
                }
            }
            if(trim(substr($query, -3)) == "OR"){
                $query = substr_replace($query, '', -3);
            }
            $query .= ") ";
        }
        $query .= " ORDER BY ";
        foreach ($_GET['order'] as $clmnsKey => $clmnsVal) {
            $colName = $_GET['columns'][$clmnsVal['column']]['data'];
            if($clmnsKey != count($_GET['order'])-1){
                $query .= $colName." ".$clmnsVal['dir'].", ";
            }else{
                $query .= $colName." ".$clmnsVal['dir']." ";
            }
        }
        $query .= " LIMIT ".$_GET['start'].",".$_GET['length'];
        $resArr['data'] = array();
        $resArr['helper']['equipments'] = $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment");
        $resArr['helper']['masters'] = $this->getArrFromTableBYQuery("SELECT id, synonym FROM managers WHERE rights <> 'noAccess'");
        $resArr['helper']['tktStates'] = $this->getArrFromTableBYQuery("SELECT id, tktState FROM tiketsStates");
        $resArr['helper']['tktStatuses'] = $this->getArrFromTableBYQuery("SELECT id, statusname FROM statuses");
        foreach (mysqli_query($this->con,$query) as $key => $value) {
            $resArr['data'][] = $value;
        }
        $fulData = mysqli_query($this->con,$fullDataQuery);
        $buf = mysqli_fetch_array($fulData,MYSQLI_ASSOC);
        $resArr["recordsFiltered"] = $buf['fullData'];
        $resArr["recordsTotal"] = $buf['fullData'];
        $resArr["draw"] = intval($_GET['draw']);
        return $resArr;
    }

	public function get_page(){
		if($_SESSION['logined']['status'] == FALSE){
			header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
		}
        if($_GET['dataAction'] == 'processTikets'){
            print_r(json_encode($this->tiketsTableAction()));
            exit();
        }
		if((isset($_GET['param_1']) && ($_GET['param_1'] == 'archive')) && in_array($_SESSION['logined']['rights'], array('admin','manager'))){
			$this->smarty->assign('title', "Архив - рабочая зона");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('logined', $_SESSION['logined']);
			$this->smarty->display('work/navbar.tpl');
			$this->json = json_decode(file_get_contents('config.json'), true);
			$this->smarty->assign('config', $this->json);
			$clients = $this->getArrFromTableBYQuery("
				SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.password,cls.id as clientStatus
				FROM clients as cl 
				join clientStatuses as cls on cl.clientStatus = cls.id");
			$this->smarty->assign('clients', $clients);
			$this->smarty->assign('equipments', $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment"));
			
			$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
			$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
            $this->smarty->assign('toDoListArr', $this->getArrFromTableBYQuery("
					SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
					FROM toDoList as tdl join managers as mg on tdl.authorID = mg.id 
                    WHERE tdl.target = 'archive' ORDER BY  tdl.taskDate ASC"));
            $this->smarty->assign('tasksListArr', $this->getArrFromTableBYQuery("
				SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
				FROM tasksList as tdl join managers as mg on tdl.authorID = mg.id 
				WHERE tdl.target = 'archive' ORDER BY  tdl.taskDate ASC"));
			$this->smarty->assign('theTikets', $this->getTikets('archive'));
			$this->smarty->display('work/workspace_archive.tpl');
			$this->smarty->display('work/footer.tpl');
		}elseif(isset($_GET['param_1']) && $_GET['param_1'] == 'completed'){
			$typeFlag = $_GET['param_1'];
			$this->smarty->assign('title', "Готовые к выдаче заказы - рабочая зона");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('logined', $_SESSION['logined']);
			$this->smarty->assign('typeFlag', $typeFlag);
			$this->smarty->display('work/navbar.tpl');
			$this->json = json_decode(file_get_contents('config.json'), true);
			$this->smarty->assign('config', $this->json);
			$this->smarty->assign('clients', $this->getArrFromTableBYQuery("SELECT id,surname,firstname,middlename,email,phone,secondphone,password FROM clients"));
			$this->smarty->assign('equipments', $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment"));
			
			$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
			$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
			$this->smarty->assign('toDoListArr', $this->getArrFromTableBYQuery("
					SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
					FROM toDoList as tdl join managers as mg on tdl.authorID = mg.id 
                    WHERE tdl.target = 'completed' ORDER BY  tdl.taskDate ASC"));
            $this->smarty->assign('tasksListArr', $this->getArrFromTableBYQuery("
				SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
				FROM tasksList as tdl join managers as mg on tdl.authorID = mg.id 
				WHERE tdl.target = 'completed' ORDER BY  tdl.taskDate ASC"));
			if($typeFlag == 'completed'){
				$this->smarty->assign('theTikets', $this->getTikets('completed'));
			}else{
				$this->smarty->assign('theTikets', $this->getTikets('bolt'));
			}
			$this->smarty->display('work/workspace_completed.tpl');
			$this->smarty->display('work/footer.tpl');
        }elseif(isset($_GET['param_1']) && $_GET['param_1'] == 'bolt'){
            $this->smarty->assign('title', "Болты и ждуны - рабочая зона");
            $this->smarty->display('header.tpl');
            $this->smarty->assign('logined', $_SESSION['logined']);
            $this->smarty->assign('typeFlag', $typeFlag);
            $this->smarty->display('work/navbar.tpl');
            $this->json = json_decode(file_get_contents('config.json'), true);
            $this->smarty->assign('config', $this->json);
            
            $this->smarty->display('work/workspace_waiting+bolt.tpl');
            $this->smarty->display('work/footer.tpl');
		}else{
			$this->smarty->assign('title', "Рабочая зона");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('logined', $_SESSION['logined']);
			$this->smarty->display('work/navbar.tpl');
			$this->json = json_decode(file_get_contents('config.json'), true);
            $this->smarty->assign('config', $this->json);
            $this->smarty->assign('masters', $this->getArrFromTableBYQuery("SELECT mng.id, mng.synonym, mng.rights FROM managers as mng WHERE mng.rights <> 'noAccess'"));
			if(in_array($_SESSION['logined']['rights'], array('admin','manager'))){
				$this->smarty->assign('newUserPassword', $this->getPassword());
				$clients['data'] = $this->getArrFromTableBYQuery("
					SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.password,cls.id as clientStatus
					FROM clients as cl 
					join clientStatuses as cls on cl.clientStatus = cls.id");

				foreach ($this->json['clients']['columns'] as $jsnKey => $jsnVal){
					if(!isset($clients['refs'][$jsnKey]) && $jsnVal['colType'] == 'select'){
						$clients['refs'][$jsnKey] = $this->getArrFromTableBYQuery("SELECT ".$jsnVal['selectColumns']." FROM ".$jsnVal['relatedTable']);
					}
				}
				$this->smarty->assign('clients', $clients);
				unset($clients);
				$this->smarty->assign('equipments', $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment"));
				$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
				$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
				$this->smarty->assign('tktCmpltnItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmpltn'"));
				$this->smarty->assign('tktExtViewItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktExtView'"));
				$this->smarty->assign('tktCmmntItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmmnt'"));
				$this->smarty->assign('smsTemlates', $this->getArrFromTableBYQuery("SELECT id,templateText FROM smsTemplates"));
				$this->smarty->assign('miniPrice', $this->getArrFromTableBYQuery("SELECT id,items FROM miniPrice"));
			}elseif(in_array($_SESSION['logined']['rights'], array('remontnik'))){
				$this->smarty->assign('clients', $this->getArrFromTableBYQuery("SELECT id,surname,firstname,middlename,email,phone,secondphone,password FROM clients"));
				$this->smarty->assign('equipments', $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment"));
				$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
				$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
			}elseif(in_array($_SESSION['logined']['rights'], array('moderator'))){
				$this->smarty->assign('newUserPassword', $this->getPassword());
				$clients['data'] = $this->getArrFromTableBYQuery("
					SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.password,cls.id as clientStatus
					FROM clients as cl 
					join clientStatuses as cls on cl.clientStatus = cls.id");

				foreach ($this->json['clients']['columns'] as $jsnKey => $jsnVal){
					if(!isset($clients['refs'][$jsnKey]) && $jsnVal['colType'] == 'select'){
						$clients['refs'][$jsnKey] = $this->getArrFromTableBYQuery("SELECT ".$jsnVal['selectColumns']." FROM ".$jsnVal['relatedTable']);
					}
				}
				$this->smarty->assign('clients', $clients);
				unset($clients);
				$this->smarty->assign('equipments', $this->getArrFromTableBYQuery("SELECT id,equipmentname FROM typesofequipment"));
				$this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
				$this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
				$this->smarty->assign('tktCmpltnItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmpltn'"));
				$this->smarty->assign('tktExtViewItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktExtView'"));
				$this->smarty->assign('tktCmmntItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'tktCmmnt'"));
				$this->smarty->assign('miniPrice', $this->getArrFromTableBYQuery("SELECT id,items FROM miniPrice"));
			}
			$this->smarty->assign('toDoListArr', $this->getArrFromTableBYQuery("
				SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
				FROM toDoList as tdl join managers as mg on tdl.authorID = mg.id 
				WHERE tdl.target = 'main' ORDER BY  tdl.taskDate ASC"));
			$this->smarty->assign('tasksListArr', $this->getArrFromTableBYQuery("
				SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
				FROM tasksList as tdl join managers as mg on tdl.authorID = mg.id 
				WHERE tdl.target = 'main' ORDER BY  tdl.taskDate ASC"));
			$this->smarty->assign('theTikets', $this->getTikets('default'));
			$this->smarty->display('work/workspace.tpl');
			$this->smarty->display('work/footer.tpl');
		}
	}
}
?>