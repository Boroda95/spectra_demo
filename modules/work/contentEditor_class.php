<?php
	class contentEditor extends core{
		protected function editItemValue($parent,$name,$newVal,$id){
			if($name == 'password'){
				$newVal = md5($newVal);
			}
			if($this->executeQuery("UPDATE ".$parent." SET ".$name."='".$newVal."' WHERE id = ".$id)){
				return 'Значение успешно изменено.';
			}else{
				return 'Хозяин, у нас пробемы. Изменения не вступили в силу.';
			}
		}

		protected function deleteItem($parent,$id){
			if($this->executeQuery("DELETE FROM ".$parent." WHERE id = ".$id)){
				return 'Значение успешно удалено.';
			}else{
				return 'Хозяин, у нас пробемы. Изменения не вступили в силу.';
			}
		}

		protected function addNewStage(){
			$done_files = array();
			for ($i=0; $i < count($_FILES['files']['name']); $i++) { 
				$file_name = $this->cyrillic_translit($_FILES['files']['name'][$i]);
				if(move_uploaded_file($_FILES['files']['tmp_name'][$i], "./img/$file_name")){
					$done_files[] = $file_name;
				}
			}
			$query = "
			INSERT INTO stages(id_tikets, stageimages, stagename, stagedescription, hidstagedescription, stagedate, stageauthor, stagePrice) 
			VALUES ('".$this->con->real_escape_string($_POST['addNewStage'])."','".serialize($done_files)."','".$this->con->real_escape_string(strip_tags($_POST['stagename']))."','".$this->con->real_escape_string(strip_tags($_POST['stagedescription']))."','".$this->con->real_escape_string(strip_tags($_POST['hidstagedescription']))."','".date("Y-m-d H:i:s")."','".$_SESSION['logined']['id']."','".$_POST['stagePrice']."')";
			if($this->executeQuery($query)){
				$this->executeQuery("INSERT INTO tktStagesMonitoring(stageId) VALUES (".mysqli_insert_id($this->con).")");
				$query = "SELECT st.id,st.stageimages,st.stagename,st.stagedescription,st.hidstagedescription,st.stagedate, mg.synonym as stageauthor, stagePrice 
				  FROM stages as st join managers as mg on st.stageauthor = mg.id 
				  WHERE id_tikets = '".$_POST['addNewStage']."' ORDER BY  st.stagedate ASC";
				$dataRow['steps'] = $this->getArrFromTableBYQuery($query);
				$this->smarty->assign('steps', $dataRow['steps']);
				$this->smarty->assign('logined', $_SESSION['logined']);
				exit($this->smarty->display('work/stepsTable.tpl'));
			}else{
				exit('Не получилосъ');
			}
		}
		
		protected function addNewTask(){
			$taskDate = date('Y-m-d H:i:s');
			$query = "
			INSERT INTO toDoList(taskText, authorId, taskDate, target) 
			VALUES ('".$this->con->real_escape_string($_POST['taskText'])."','".$_SESSION['logined']['id']."','".$taskDate."','".$this->con->real_escape_string($_POST['addNewTask'])."')";
			if($this->executeQuery($query)){
				$query = "SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
				  FROM toDoList as tdl join managers as mg on tdl.authorID = mg.id 
				  WHERE tdl.target = '".$_POST['addNewTask']."' ORDER BY  tdl.taskDate ASC";
				$dataRow['steps'] = $this->getArrFromTableBYQuery($query);
				$this->smarty->assign('toDoListArr', $this->getArrFromTableBYQuery($query));
				$this->smarty->assign('logined', $_SESSION['logined']);
				exit($this->smarty->display('work/toDoListTable.tpl'));
			}else{
				exit('Не получилосъ');
			}
        }

		protected function addNewTaskToTasks(){
			$taskDate = date('Y-m-d H:i:s');
			$query = "
			INSERT INTO tasksList(taskText, authorId, taskDate, target) 
			VALUES ('".$this->con->real_escape_string($_POST['taskText'])."','".$_SESSION['logined']['id']."','".$taskDate."','".$this->con->real_escape_string($_POST['addNewTaskToTasks'])."')";
			if($this->executeQuery($query)){
				$query = "SELECT tdl.id,tdl.taskText,tdl.authorId,tdl.taskDate,tdl.target,mg.synonym as taskAuthor
				  FROM tasksList as tdl join managers as mg on tdl.authorID = mg.id 
				  WHERE tdl.target = '".$_POST['addNewTaskToTasks']."' ORDER BY  tdl.taskDate ASC";
				$dataRow['steps'] = $this->getArrFromTableBYQuery($query);
				$this->smarty->assign('tasksListArr', $this->getArrFromTableBYQuery($query));
				$this->smarty->assign('logined', $_SESSION['logined']);
                exit($this->smarty->display('work/tasksListTable.tpl'));
			}else{
				exit('Не получилосъ');
			}
		}

		protected function getClientContentById($id){
			$client =  $this->getArrFromTableBYQuery("
							SELECT cl.id,cl.surname,cl.firstname,cl.middlename,cl.email,cl.phone,cl.secondphone,
							   cl.password,cl.creditCardNum,cl.creditCardCode,cl.crdtCrdIssuanceDate,cl.birthday,
							   cl.gender,cl.haveChilds_14old,cl.haveCar,cl.havePets,cl.adress,cl.hiddenInfo, cls.id as clientStatus
							FROM clients as cl 
							join clientStatuses as cls on cl.clientStatus = cls.id 
							WHERE cl.id=".$id);
			$json = json_decode(file_get_contents('config.json'), true);
			$clJson = $json['clients'];
			unset($json);
			foreach ($clJson['columns'] as $jsnKey => $jsnVal){
				if($jsnVal['colType'] == 'select'){
					$client['refs'][$jsnKey] = $this->getArrFromTableBYQuery("SELECT ".$jsnVal['selectColumns']." FROM ".$jsnVal['relatedTable']);
				}
			}
			$this->smarty->assign('client', $client);
			$this->smarty->assign('clientsConfig', $clJson);
			$query = "SELECT tk.id as tktId, st.statuscode,tk.actnum FROM tikets AS tk JOIN statuses AS st ON tk.id_statuses = st.id WHERE id_clients = '".$client[0]['id']."'";
            $tiketHistory = $this->getArrFromTableBYQuery($query);

			$this->smarty->assign('tiketHistory', $tiketHistory);
			$this->smarty->assign('logined', $_SESSION['logined']);
            exit($this->smarty->display('work/getUserFullInfo.tpl'));
		}

		protected function getClienSmsArchive($id){
			$clientSms =  $this->getArrFromTableBYQuery("
							SELECT arch.id,arch.clientPhone,arch.smsID,arch.smsContext,arch.author,arch.sendDate,arch.statusDescription
							FROM smsArchive AS arch
							WHERE arch.clientId = ".$id);
			$account = $this->getArrFromTableBYQuery("
							SELECT clnt.password,clnt.phone
							FROM clients AS clnt
							WHERE clnt.id = ".$id);
			$json = json_decode(file_get_contents('config.json'), true);
			$statusJson = $json['smsStatuses'];
			unset($json);
			foreach ($clientSms as $clientSmsKey => $clientSmsArr){
				if($clientSmsArr['statusDescription'] != '103'){
					$temp = file_get_contents('https://sms.ru/sms/status?api_id=demo&sms_id='.$clientSmsArr["smsID"].'&json=1');
					$json = json_decode($temp, true);
					if($json['status'] == "OK"){
						$this->executeQuery("UPDATE smsArchive SET statusDescription ='".$json['sms'][$clientSmsArr["smsID"]]['status_code']."' WHERE id = ".$clientSmsArr['id']);
						$clientSms[$clientSmsKey]['statusDescription'] = $statusJson[$json['sms'][$clientSmsArr["smsID"]]['status_code']];
						unset($json);
					}
				}else{
					$clientSms[$clientSmsKey]['statusDescription'] = $statusJson[$clientSmsArr['statusDescription']];
				}
			}
			$this->smarty->assign('clientSms', $clientSms);
			$resultData['login'] = $account[0]['phone'];
			$resultData['passw'] = $account[0]['password'];
			$resultData['archive'] = $this->smarty->fetch('work/getSmsArchiveTable.tpl');
			exit(json_encode($resultData));
		}

		protected function sendSms($clientId,$clientPhone,$smsText){
			require_once 'sms.ru/sms.ru.php';
			$smsru = new SMSRU('demo');
			$data = new stdClass();
			$data->to = '7'.$clientPhone;
			$data->text = $smsText;
			$sms = $smsru->send_one($data);
			if ($sms->status == "OK") {
				$this->executeQuery("INSERT INTO smsArchive (clientId,clientPhone,smsID,smsContext,author,sendDate,statusDescription) 
					                    VALUES ('".$clientId."','".$clientPhone."','".$sms->sms_id."','".$smsText."','".$_SESSION['logined']['login']."','".date('Y-m-d H:i:s')."','".$sms->status_code."')");
				return array('status' => 'OK', 'message' => 'Смска успешно улетела)');
			}else{
			    return array('status' => 'ERROR', 'message' => "Текст ошибки: $sms->status_text.");
			}
		}

		protected function getTiketDetailsModal($tiketID){
            $tiketData = $this->getArrFromTableBYQuery("
                SELECT cl.id as clntID,cl.firstname,cl.middlename,cl.surname,cl.phone,cl.secondphone,cl.email,cl.creditCardNum,st.id as id_statuses, tSt.id as id_tiketState, is_notified,
                       tk.id as tktID,tk.id_typesofequipment,tk.actnum,tk.hiddenstatus,tk.productname,tk.productseries,tk.completeness,tk.faultdescription,tk.exteriorview,
                       tk.tktcomment,tk.admissiondate,tk.miniPrice,tk.master,tk.priority,tk.priceRange
                FROM clients as cl join tikets as tk on tk.id_clients = cl.id
                                   join typesofequipment as tq on tk.id_typesofequipment = tq.id 
                                   join statuses as st on tk.id_statuses = st.id 
                                   join tiketsStates as tSt on tk.id_tiketState = tSt.id 
                WHERE tk.actnum = '".$tiketID."'
            ")[0];
            $dataRow = array();
            $dataRow['refs'] = array();
            $dataRow['tiket'] = $tiketData;
            $json = json_decode(file_get_contents('config.json'), true);
            foreach ($json as $confKey => $confArr) {
                foreach ($tiketData as $bufKey => $bufVal) {
                    if(!in_array($bufKey, $dataRow['refs']) && $confArr['columns'][$bufKey]['colType'] == 'select' && !isset($dataRow['refs'][$bufKey])){
                        $dataRow['refs'][$bufKey] = $this->getArrFromTableBYQuery("SELECT ".$confArr['columns'][$bufKey]['selectColumns']." FROM ".$confArr['columns'][$bufKey]['relatedTable']);
                    }
                }
            }
            $query = "SELECT tk.id as tktId, st.statuscode,tk.actnum FROM tikets AS tk JOIN statuses AS st ON tk.id_statuses = st.id WHERE id_clients = '". $tiketData['clntID']."'";
            $dataRow['tiketHistory'] = $this->getArrFromTableBYQuery($query);
            
            $query = "SELECT st.id,st.stageimages,st.stagename,st.stagedescription,st.hidstagedescription,st.stagedate,st.stagePrice, mg.synonym as stageauthor 
                        FROM stages as st join managers as mg on st.stageauthor = mg.id 
                        WHERE id_tikets = '". $tiketData['tktID']."' ORDER BY  st.stagedate ASC";
            $dataRow['steps'] = $this->getArrFromTableBYQuery($query);
            
			$this->smarty->assign('tiketInfo', $dataRow);
            $this->smarty->assign('config', $json);
            $this->smarty->assign('logined', $_SESSION['logined']);
            $this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
            $this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
            $this->smarty->assign('masters', $this->getArrFromTableBYQuery("SELECT id, synonym FROM managers WHERE rights <> 'noAccess'"));
			$this->smarty->display('work/modals/tiketPreviewModal.tpl');
			exit();
        }

		public function form_obr(){
			if($_SESSION['logined']['status'] == FALSE){
				header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
			}
			if($_POST['expressEditor']){
				if($_POST['name'] == 'id_statuses'){
					$this->executeQuery("INSERT INTO tktStatusMonitoring(tiketId,eventDate,oldStatus,newStatus,authorId) 
										 VALUES('".$this->con->real_escape_string($_POST['id'])."','".date('Y-m-d H:i:s')."','".$this->con->real_escape_string($_POST['old_val'])."','".$this->con->real_escape_string($_POST['new_val'])."','".$_SESSION['logined']['id']."')");
				}
				exit(json_encode(array('message' => $this->editItemValue($this->con->real_escape_string($_POST['parent']),$this->con->real_escape_string($_POST['name']),$this->con->real_escape_string(strip_tags($_POST['new_val'])),$this->con->real_escape_string($_POST['id'])))));
			}
			if($_POST['expressDeleter']){
				exit(json_encode(array('message' => $this->deleteItem($_POST['parent'],$_POST['id']))));
			}
			if($_POST['stagename']){
				$this->addNewStage();
			}
			if($_POST['addNewTask']){
				$this->addNewTask();
			}
			if($_POST['addNewTaskToTasks']){
				$this->addNewTaskToTasks();
			}
			if($_POST['getClientContentById']){
				$this->getClientContentById($_POST['clientID']);
			}
			if($_POST['getSmsArchive']){
				$this->getClienSmsArchive($_POST['clientID']);
			}
			if($_POST['sendSms']){
				exit(json_encode($this->sendSms($_POST['clientID'],$_POST['clientPhone'],$this->con->real_escape_string($_POST['smsText']))));
			}
			if($_POST['checkPhone']){
				if($this->checkNumBeforeAdd("SELECT id FROM clients WHERE phone = '".$this->con->real_escape_string($_POST['clientPhone'])."'") == true){
					$result = array('checkResult' => "ok");
					$this->logMe('Клиента с номером '.$_POST['clientPhone']." нет в базе, идем добавлять.",'debug','log');
				}else{
					$this->logMe('Клиент с номером '.$_POST['clientPhone']." уже есть в базе, наша остановка.",'debug','log');
					$result = array('checkResult' => "This number is already in the database.");
				}
				exit(json_encode($result));
			}
			if($_POST['action'] == "getTiketDetailModal"){
				$this->getTiketDetailsModal($_POST["tiketID"]);
			}
		}
		
		public function get_page(){
		}
	}
?>