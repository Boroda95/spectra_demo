<?php
class settings extends core{
    public function form_obr()
    {
        if ($_POST['action'] == "addNewMaster") {
            $this->addNewMaster();
        }
    }
    public function addNewMaster()
    {
        // print_r($_POST);
        $query = "INSERT INTO managers(synonym, fName, sName, rights, password) 
                               VALUES ('".$_POST['masterSynonym']."','".$_POST['masterName']."','".$_POST['masterSecName']."','".$_POST['masterRights']."','".md5($_POST['masterPassword'])."')";
        // print($query);
        // exit;
        $this->executeQuery($query);
        header("Location: http://" . $_SERVER['HTTP_HOST'] . '/settings/');
    }

	public function get_page(){
		if($_SESSION['logined']['status'] == FALSE){
			header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
        }
		if(in_array($_SESSION['logined']['rights'], array('admin'))){
			$this->smarty->assign('title', "Редактирование прав пользователей.");
			$this->smarty->display('header.tpl');
			$this->smarty->assign('logined', $_SESSION['logined']);
			$this->smarty->display('work/navbar.tpl');
			$managers =  $this->getArrFromTableBYQuery("SELECT id,fName,sName,synonym,rights,password FROM managers");
			$this->smarty->assign('managers', $managers);
			$this->smarty->display('work/settings.tpl');
			$this->smarty->display('work/footer.tpl');
		}else{
			header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');	
		}
	}
}
?>