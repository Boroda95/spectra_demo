<?php
    class tiketPreview extends core{
        protected function getTiketByID($thisActNum){
            $query = "
                SELECT cl.id as clntID,cl.firstname,cl.middlename,cl.surname,cl.phone,cl.secondphone,cl.email,cl.creditCardNum,st.id as id_statuses, tSt.id as id_tiketState, is_notified,
                       tk.id as tktID,tk.id_typesofequipment,tk.actnum,tk.hiddenstatus,tk.productname,tk.productseries,tk.completeness,tk.faultdescription,tk.exteriorview,
                       tk.tktcomment,tk.admissiondate,tk.miniPrice,tk.master,tk.priority,tk.priceRange
                FROM clients as cl join tikets as tk on tk.id_clients = cl.id
                                   join typesofequipment as tq on tk.id_typesofequipment = tq.id 
                                   join statuses as st on tk.id_statuses = st.id 
                                   join tiketsStates as tSt on tk.id_tiketState = tSt.id 
                WHERE tk.actnum = '".$thisActNum."'
            ";
            $result = mysqli_query($this->con,$query);
            if(!$result){
                $this->logMe("&#10;&#9;&#9;Ошибка в: '".__FUNCTION__."'.&#10;&#9;&#9;Инициатор:".$query.".&#10;&#9;&#9;Текст: ".mysqli_error($this->con)."&#10;  ","error" ,'log');
                return FALSE;
            }
            $dataRow = array();
            $dataRow['refs'] = array();
            for($i = 0; $i < mysqli_num_rows($result); $i++){
                $buf = mysqli_fetch_array($result,MYSQLI_ASSOC);
                $dataRow['tiket'] = $buf;
                foreach ($this->json as $confKey => $confArr) {
                    foreach ($buf as $bufKey => $bufVal) {
                        if(!in_array($bufKey, $dataRow['refs']) && $confArr['columns'][$bufKey]['colType'] == 'select' && !isset($dataRow['refs'][$bufKey])){
                            $dataRow['refs'][$bufKey] = $this->getArrFromTableBYQuery("SELECT ".$confArr['columns'][$bufKey]['selectColumns']." FROM ".$confArr['columns'][$bufKey]['relatedTable']);
                        }
                    }
                }
                $query = "SELECT tk.id as tktId, st.statuscode,tk.actnum FROM tikets AS tk JOIN statuses AS st ON tk.id_statuses = st.id WHERE id_clients = '".$buf['clntID']."'";
                $dataRow['tiketHistory'][$buf['tktID']] = $this->getArrFromTableBYQuery($query);
                
                $query = "SELECT st.id,st.stageimages,st.stagename,st.stagedescription,st.hidstagedescription,st.stagedate,st.stagePrice, mg.synonym as stageauthor 
                          FROM stages as st join managers as mg on st.stageauthor = mg.id 
                          WHERE id_tikets = '".$buf['tktID']."' ORDER BY  st.stagedate ASC";
                $dataRow['steps'] = $this->getArrFromTableBYQuery($query);
            }
            return $dataRow;
        }
        public function get_page(){
            if($_SESSION['logined']['status'] == FALSE){
                header("Location: http://".$_SERVER['HTTP_HOST'].'/logIn/');
            }
            if(!isset($_GET['param_1'])){
                header("Location: http://".$_SERVER['HTTP_HOST'].'/workspace/');
            }

            $thisActNum = $_GET['param_1'];

            $this->smarty->assign('title', $thisActNum);
            $this->smarty->display('header.tpl');
            $this->smarty->assign('logined', $_SESSION['logined']);
            $this->smarty->display('work/navbar.tpl');
            $this->json = json_decode(file_get_contents('config.json'), true);
            $this->smarty->assign('config', $this->json);

            
            $this->smarty->assign('stepTitleItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepTitle'"));
            $this->smarty->assign('stepDescrItemsTable', $this->getArrFromTableBYQuery("SELECT id,items FROM quickAccessItems where target = 'stepDescr'"));
            $this->smarty->assign('masters', $this->getArrFromTableBYQuery("SELECT id, synonym FROM managers WHERE rights <> 'noAccess'"));
            $this->smarty->assign('tiket', $this->getTiketByID($thisActNum));
            $this->smarty->display('work/tiketPreview.tpl');
            $this->smarty->display('work/footer.tpl');
        }
    }
?>