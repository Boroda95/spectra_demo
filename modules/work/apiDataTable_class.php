<?php
class apiDataTable extends core{
    public function getPageContent(){
        $jsonConfig = json_decode(file_get_contents('configs/config.json'));
        if(!in_array($_GET['param_1'], $jsonConfig->availableTblList)) {
            $this->logMe('Кто-то ломится в незарегистрированную таблицу: '.$_GET['param_1'], 'error');
            return "НиКоГо НеТ дОмА.";
        }
        $query = "SELECT ";
        foreach ($_GET['columns'] as $clmnsKey => $clmnsVal) {
            if($clmnsKey != count($_GET['columns'])-1){
                $query .= $clmnsVal['data'].", ";
            }else{
                $query .= $clmnsVal['data']." ";
            }
        }
        $query .= "FROM ".$_GET['param_1']." ";
        $fullDataQuery = $query;
        if($_GET['search']['value']){
            $sDT = $_GET['search']['value'];
            $query .= "WHERE ";
            foreach ($_GET['columns'] as $clmnsKey => $clmnsVal) {
                if($clmnsKey != count($_GET['columns'])-1){
                    $query .= $clmnsVal['data']." LIKE '%".$sDT."%' OR ";
                }else{
                    $query .= $clmnsVal['data']." LIKE '%".$sDT."%' ";
                }
            }
        }
        $query .= "ORDER BY ";
        foreach ($_GET['order'] as $clmnsKey => $clmnsVal) {
            $colName = $_GET['columns'][$clmnsVal['column']]['data'];
            if($clmnsKey != count($_GET['order'])-1){
                $query .= $colName." ".$clmnsVal['dir'].", ";
            }else{
                $query .= $colName." ".$clmnsVal['dir']." ";
            }
        }
        $query .= "LIMIT ".$_GET['start'].",".$_GET['length'];
        $resArr['data'] = array();
        foreach ($this->con->query($query) as $key => $value) {
            $resArr['data'][] = $value;
        }
        $fulData = $this->con->query($fullDataQuery);
        $rows = $fulData->fetchAll();
        $resArr["recordsFiltered"] = count($rows);
        $resArr["recordsTotal"] = count($rows);
        $resArr["draw"] = intval($_GET['draw']);
        print(json_encode($resArr));
        exit();
    }
}
?>